# coding:utf-8
'''
jieba.cut 方法接受三个输入参数: 需要分词的字符串；cut_all 参数用来控制是否采用全模式；HMM 参数用来控制是否使用 HMM 模型
jieba.cut_for_search 方法接受两个参数：需要分词的字符串；是否使用 HMM 模型。该方法适合用于搜索引擎构建倒排索引的分词，粒度比较细
待分词的字符串可以是 unicode 或 UTF-8 字符串、GBK 字符串。注意：不建议直接输入 GBK 字符串，可能无法预料地错误解码成 UTF-8
jieba.cut 以及 jieba.cut_for_search 返回的结构都是一个可迭代的 generator，可以使用 for 循环来获得分词后得到的每一个词语(unicode)，或者用
jieba.lcut 以及 jieba.lcut_for_search 直接返回 list
jieba.Tokenizer(dictionary=DEFAULT_DICT) 新建自定义分词器，可用于同时使用不同词典。jieba.dt 为默认分词器，所有全局分词相关函数都是该分词器的映射。
'''

'''
常用结巴分词词性对照表

a:形容词
c:连词
d:副词
e:叹词
logging.conf:方位词
i:成语
m:数词
n:名词
nr:人名
ns:地名
nt:机构团体
nz:其他专有名词
p:介词
r:代词
t:时间
u:助词
v:动词
vn:名动词
w:标点符号
un:未知词语
'''
import jieba
import jieba.posseg
import jieba.analyse
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")

sentence = "我喜欢上海东方明珠,云间科技,大数据和云计算,坚持学习云计算"
# 精准模式，默认模式
w1 = jieba.cut(sentence, cut_all=False)
for item in w1:
    print(item)
print("")

# 全模式
w2 = jieba.cut(sentence, cut_all=True)
for item in w2:
    print(item)
print("")

# 搜索引擎模式
w3 = jieba.cut_for_search(sentence)
for item in w3:
    print(item)
print("")

# 词性标注
w5 = jieba.posseg.cut(sentence)
for item in w5:
    print(item.word, "  ", item.flag)  # .flag词性 .word词语
print("")

jieba.load_userdict(config.get("data_path", "dict_path") + "dict.txt")
w6 = jieba.posseg.cut(sentence)
for item in w6:
    print(item.word, "  ", item.flag)
print("")

# 添加分词
jieba.add_word("大数据")
jieba.add_word("云计算")
w7 = jieba.posseg.cut(sentence)
for item in w7:
    print(item.word, "  ", item.flag)
print("")

# 更改词频
jieba.suggest_freq(("云间科技", "云计算"), True)
w8 = jieba.cut(sentence)
for item in w8:
    print(item)

# 提取关键词
tag = jieba.analyse.extract_tags(sentence, 3)
print("关键词", tag)
print("")

# 返回词语的位置
w9 = jieba.tokenize(sentence)
for item in w9:
    print(item)
print("")
# 搜索引擎模式
w10 = jieba.tokenize(sentence, mode="search")
for item in w10:
    print(item)
print("")
