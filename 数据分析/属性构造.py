# coding:utf-8
import numpy as np
import pandas as pda
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")

data = pda.read_excel(config.get("data_path", "db_yiihua_path"), sheet_name=0, skiprows=[1], usecols=[0, 12, 19])
date = data["日期"]
zjs = data["总局数"]
zrc = data["总人次"]
data["人均局数"] = zjs / zrc
print(data.describe())
data.to_excel(config.get("data_path", "temp_path") + "test.xlsx", index=False)
