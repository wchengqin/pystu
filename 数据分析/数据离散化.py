# coding:utf-8
import numpy as np
import pandas as pda
import configparser

'''
pandas.cut(x,bins,right=True,labels=None,retbins=False,precision=3,include_lowest=False)
x    : 进行划分的一维数组
bins : 整数---将x划分为多少个等间距的区间
labels : 是否用标记来代替返回的bins
precision: 精度
include_lowest:是否包含左端点
'''
config = configparser.ConfigParser()
config.read("../config.ini")

data = pda.read_excel(config.get("data_path", "db_yiihua_path"), sheet_name=0, skiprows=[1], usecols=[12, 19])
print(data.describe())
zjs = data["总局数"]
zrc = data["总人次"]
# 连续数据离散化
zjs_dkls = pda.cut(zjs, bins=3, labels=["低", "中", "高"])  # 等宽离散化
print(zjs_dkls)
zrc_fdkls = pda.cut(zrc, [150, 200, 350, zrc.max()], labels=["低", "中", "高"])  # 非等宽离散化
print(zrc_fdkls)
