# coding:utf-8
import numpy as np
import pandas as pda
import configparser
from sklearn.decomposition import PCA

config = configparser.ConfigParser()
config.read("../config.ini")

data = pda.read_excel(config.get("data_path", "db_yiihua_path"), sheet_name=0, skiprows=[1], usecols=[12, 19])
zjs = data["总局数"]
zrc = data["总人次"]
data["人均局数"] = zjs / zrc
# print(data.describe())

# --PCA算法--
pca1 = PCA()
pca1.fit(data)
Characteristic = pca1.components_  # 返回模型中各个特征量
print("特征量",Characteristic)
rate = pca1.explained_variance_ratio_  # 各个成分中各自方差百分比，贡献率
print("各个成分中各自方差百分比，贡献率",rate)

pca2 = PCA()
pca2.fit(data)
reduction = pca2.transform(data)  # 降维
print("降维",reduction)
recovery = pca2.inverse_transform(reduction)  # 恢复
#print(recovery)
