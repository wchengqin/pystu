import pandas as pda

pda.set_option('display.max_columns', None)
pda.set_option('display.max_rows', None)
x = pda.Series([8, 3, 1, 6])
print(x)

y = pda.Series(12345)  # Series一维数据
print(y)

z = pda.Series([8, 3, 1, 6], index=["a", "b", "c", "d"])
print(z)

h = pda.DataFrame([[8, 3, 1, 6], [5, 9, 2, 7], [4, 3, 5, 8]], columns=["a", "b", "c", "d"])  # DataFrame 二维数组
print(h)
print(h.describe())  # 统计
print(h.T)  # 行列转置

i = pda.DataFrame({
    "a": 3,
    "b": [8, 3, 1, 6],
    "c": list("9724"),
    "d": [4, 3, 5, 8]
})  # DataFrame 字典
print(i)
print(i.head(2))
print(i.tail(2))
