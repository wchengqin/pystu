import pandas as pda
import configparser

'''
io : string, path object ; excel 路径。
sheetname : string, int, mixed list of strings/ints, or None, default 0 返回多表使用sheetname=[0,1],若sheetname=None是返回全表 注意：int/string 返回的是dataframe，而none和list返回的是dict of dataframe
header : int, list of ints, default 0 指定列名行，默认0，即取第一行，数据为列名行以下的数据 若数据不含列名，则设定 header = None
skiprows : list-like,Rows to skip at the beginning，省略指定行数的数据
skip_footer : int,default 0, 省略从尾部数的int行数据
index_col : int, list of ints, default None指定列为索引列，也可以使用u”strings”
names : array-like, default None, 指定列的名字。
usecols : int or list, default None，读取指定的列
'''
config = configparser.ConfigParser()
config.read("../config.ini")
e = pda.read_excel(config.get("data_path", "db_yiihua_path"), sheet_name=0, header=0, skiprows=[1])

print(e)
print(e.shape)
# print(e.T) #行列转置
print(e.values)
print(e.describe())
print(e.sort_values(by="总局数"))
