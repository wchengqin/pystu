# coding:utf-8
import numpy as np
import pandas as pda
import configparser
config = configparser.ConfigParser()
config.read("../config.ini")
data = pda.read_excel(config.get("data_path","db_yiihua_path"), sheet_name=0, skiprows=[1], usecols=[12, 19])

# 离差标准化（最小-最大标准化）: 消除量纲（单位）影响以及变异大小因素的影响
# 公式：x1=（x-min）/（max-min）
data1 = (data - data.min()) / (data.max() - data.min())
# print(data1)

# 标准差标准化（零-均值标准化）：消除单位影响以及变量自身变异影响
# 公式：x1=（x-平均数）/标准差
data2 = (data - data.mean()) / data.std()
# print(data2)
#print("平均数：", data2.mean(), " 标准差：", data2.std())

# 小数定标规范化：消除单位影响
# 公式：x1=x/10**(k)、k=log10(x的绝对值的最大值)
# ceil : 向正无穷取整 朝正无穷大方向取整
data3 = data / 10 ** (np.ceil(np.log10(data.abs().max())))
print(data3)
