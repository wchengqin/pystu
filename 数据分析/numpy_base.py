import numpy

x = numpy.array([3, 2, 1])
print(x[0])
x.sort()  #排序
print(x)
print(x[1:3])  #切片
print(x[:2])
print(x[2:])

y = numpy.array([[1,2,3],[4,5,6],[7,8,9]])
print(y[1][2])
print(y.max())  #最大值
print(y.min())  #最小值
print(y.mean())  #平均数