
python -V
Python 3.8.8

1、升级、重置PIP

python -m pip install --upgrade pip

pip install --upgrade setuptools

python报错："no model named pip"，先执行：

python -m ensurepip

2、Python3虚拟环境

pip install virtualenv

pip3 install virtualenvwrapper-win

设置环境变量：WORKON_HOME=指定目录（D:\DEV\PythonEnvs\）

创建虚拟环境：

mkvirtualenv 环境名称

切换虚拟环境

workon 环境名称

3、安装、删除、升级模块

pip install 模块名==版本号

pip uninstall 模块名

pip install --upgrade 模块名==版本号


4、Python3安装PIL模块时报错时，需指定具体的包源、安装目录

pip install  -i https://pypi.doubanio.com/simple/  --trusted-host pypi.doubanio.com --target=D:\DEV\PythonEnvs\pystu\Lib\site-packages Pillow