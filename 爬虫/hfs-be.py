import sys, base64, datetime
import requests
from tabulate import tabulate


class HfsBe:
    base_path = 'https://hfs-be.yunxiao.com/'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36'}
    session = None
    login_info = None
    user_snapshot = None
    exam_list = None

    def __init__(self, username, password):
        """
        :param username:
        :param password: 密码的base64
        """
        print(f"username: {username}, password: {password}")
        self.session = requests.session()
        self._login(username, password)
        self._user_snapshot()
        self._exam_list()

    def api(self, version):
        return self.base_path + '/' + version + '/'

    def api_v2(self, ):
        return self.api('v2')

    def api_v3(self, ):
        return self.api('v3')

    def _parse(self, response):
        # 处理响应
        if response.status_code != 200:
            raise ValueError(f"请求异常（{response.status_code} ）：{response.text}")
        else:
            result = response.json()
            print(result)
            code = result.get('code')
            msg = result.get('msg')
            if code != 0:
                raise ValueError(f"请求异常（{code} ）：{msg}")
            else:
                return result

    # 系统登录
    def _login(self, username, password):
        api = self.api_v2() + 'users/sessions'
        data = {
            "loginName": username,
            "password": password,
            "roleType": 2,
            "loginType": 1,
            "rememberMe": 2
        }
        response = self.session.post(
            url=api,
            headers=self.headers,
            data=data
        )
        self.login_info = self._parse(response).get('data')

    # 用户信息
    def _user_snapshot(self):
        api = self.api_v2() + 'user-center/user-snapshot'
        response = self.session.get(
            url=api,
            headers=self.headers
        )
        self.user_snapshot = self._parse(response).get('data')

    # 历次考试列表成
    def _exam_list(self):
        api = self.api_v3() + 'exam/list?start=-1'
        response = self.session.get(
            url=api,
            headers=self.headers
        )
        self.exam_list = self._parse(response).get('data').get('list')

    def get_exam_info(self, filter, field=None):
        for index, exam in enumerate(self.exam_list):
            if (field is None and index + 1 == filter) or (field is not None and exam[field] == filter):
                if 'detail' not in exam:
                    api = self.api_v3() + 'exam/' + str(exam['examId']) + '/overview'
                    response = self.session.get(
                        url=api,
                        headers=self.headers
                    )
                    exam_detail = self._parse(response).get('data')
                    exam['detail'] = exam_detail
                return exam
        print(f"错误的考试：{field}={filter}")
        return None


def app_run():
    username = input("用户名: ")
    if not username:
        raise ValueError("用户名不能为空")
    password = input("密码: ")
    if not password:
        raise ValueError("登录密码不能为空")
    password = str(base64.b64encode(password.encode("utf-8")))[2:-1]
    hfs = HfsBe(username, password)
    if not hfs.exam_list:
        print("考试记录为空")
        return
    for index, exam in enumerate(hfs.exam_list):
        day = datetime.datetime.fromtimestamp(exam["time"] / 1000).strftime('%Y-%m-%d')
        print(tabulate([{"序号": index + 1, "考试ID": exam["examId"], "考试名称": exam["name"],
                         "考试时间": day,
                         "得分": exam["score"], "满分": exam["manfen"], "班级等级": exam["classRankPart"],
                         "年级等级": exam["gradeRankPart"]}],
                       headers="keys", tablefmt="grid"))
        data = hfs.get_exam_info(index + 1)
        if data is None:
            continue
        detail = data['detail']
        if detail is None:
            continue
        papers = detail['papers']
        if papers is None:
            continue
        tables = []
        for idx, paper in enumerate(papers):
            tables.append({"序号": idx + 1, "科目": paper["subject"], "得分": paper["score"], "满分": paper["manfen"]})
        # 打印表格
        print(tabulate(tables, headers="keys", tablefmt="grid"))


if __name__ == "__main__":
    app_run()
