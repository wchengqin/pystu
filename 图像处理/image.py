from enum import Enum
import cv2
import numpy as np


class ColorSpace(Enum):
    GRAY = 1
    BGR = 3
    BGRA = 4
    HSV = 5
    UNKNOWN = 0


# 颜色编码映射
CONVERT_IMAGE_COLOR_MAPPING = {
    (ColorSpace.GRAY, ColorSpace.BGR): cv2.COLOR_GRAY2BGR,  # 灰度图像 1通道  --  BGR 3通道
    (ColorSpace.GRAY, ColorSpace.BGRA): cv2.COLOR_GRAY2BGRA,  # 灰度图像 1通道  --  BGRA 4通道
    (ColorSpace.BGR, ColorSpace.GRAY): cv2.COLOR_BGR2GRAY,  # BGR 3通道  --  灰度图像 1通道
    (ColorSpace.BGR, ColorSpace.BGRA): cv2.COLOR_BGR2BGRA,  # BGR 3通道  --  BGRA 4通道
    (ColorSpace.BGR, ColorSpace.HSV): cv2.COLOR_BGR2HSV,  # BGR 3通道  --  HSV颜色空间
    (ColorSpace.BGRA, ColorSpace.GRAY): cv2.COLOR_BGRA2GRAY,  # BGRA 4通道  --  灰度图像 1通道
    (ColorSpace.BGRA, ColorSpace.BGR): cv2.COLOR_BGRA2BGR,  # BGRA 4通道  --  BGR 3通道
    (ColorSpace.BGRA, ColorSpace.HSV): [cv2.COLOR_BGRA2BGR, cv2.COLOR_BGR2HSV],  # BGRA 4通道  --  HSV颜色空间
    (ColorSpace.HSV, ColorSpace.BGR): cv2.COLOR_HSV2BGR,  # HSV颜色空间  --  BGR 3通道
    (ColorSpace.HSV, ColorSpace.BGRA): [cv2.COLOR_HSV2BGR, cv2.COLOR_BGR2BGRA],  # HSV颜色空间  --  BGRA 4通道
}


def rgb_to_bgr(rgb):
    """
     (R, G, B)  转  (B, G, R)
    OpenCV 默认使用 BGR 颜色空间来处理图像，而不是 RGB。
    :param rgb: RGB颜色
    :return: BGR颜色空间
    """
    return rgb[2], rgb[1], rgb[0]


def get_image_color_space(image):
    """
    获取图像的颜色编码
    :param image: CV2图像
    :return: 图像的颜色编码枚举值
    """
    if image is None:
        return ColorSpace.UNKNOWN

    if len(image.shape) == 2:  # 灰度图像
        return ColorSpace.GRAY
    elif len(image.shape) == 3:
        channels = image.shape[2]
        if channels == 3:
            if is_hsv_image(image):
                return ColorSpace.HSV
            else:
                return ColorSpace.BGR
        elif channels == 4:
            return ColorSpace.BGRA
        else:
            return ColorSpace.UNKNOWN
    else:
        return ColorSpace.UNKNOWN


def get_image_channel_count(image):
    """
    获取图像的通道数
    :param image: CV2图像
    :return: 图像通道数：1、3、4 或 None
    """
    if image is None:
        return None

    if len(image.shape) == 2:  # 灰度图像
        return 1
    elif len(image.shape) == 3:
        return image.shape[2]  # 返回通道数
    else:
        return None


def is_hsv_image(image):
    """
    判断图像是否已经是 HSV 颜色空间
    :param image: CV2图像
    :return: 布尔值，True 表示图像已经是 HSV 颜色空间，False 表示不是
    """
    if image is None:
        raise ValueError("图像文件异常")
    # 检查图像是否有三个通道
    if len(image.shape) != 3 or image.shape[2] != 3:
        return False
    # 提取各个通道
    h, s, v = cv2.split(image)
    # 检查 Hue 通道的值范围
    if not np.all((h >= 0) & (h <= 179)):
        return False
    # 检查 Saturation 和 Value 通道的值范围
    if not np.all((s >= 0) & (s <= 255)) or not np.all((v >= 0) & (v <= 255)):
        return False
    return True


def convert_image_color_space(image, target_color_space):
    """
    将图像转换为目标颜色编码
    :param image: CV2图像
    :param target_color_space: 目标颜色编码枚举值
    :return: 转换后的图像
    """
    if image is None:
        raise ValueError("图像文件异常")

    current_color_space = get_image_color_space(image)
    print(f"current_color_space: {current_color_space.name}   target_color_space: {target_color_space.name}")
    if current_color_space == target_color_space:
        return image

    key = (current_color_space, target_color_space)
    if key in CONVERT_IMAGE_COLOR_MAPPING:
        codes = CONVERT_IMAGE_COLOR_MAPPING[key]
        if isinstance(codes, list):
            for code in codes:
                image = cv2.cvtColor(image, code)
        else:
            image = cv2.cvtColor(image, codes)
        print(f"Conversion codes: {codes}")
    else:
        raise ValueError(f"无法转换颜色编码：从 {current_color_space.name} 到 {target_color_space.name}")

    return image


def get_image_color_mask(image, color_array):
    """
    创建一个颜色掩码，用于选择特定颜色范围内的像素。
    :param image: CV2图像
    :param color_array: 颜色范围数组 ，格式为[[lower_rgb, upper_rgb]...]，例如[[(0, 0, 221), (180, 30, 255)],[(0,0,46), (180,43,220)]...]
    :return: 图像掩码
    """
    # 初始化一个全零的掩码，大小与输入图像相同
    mask = np.zeros(image.shape[:2], dtype=np.uint8)
    for color_tuple in color_array:
        lower = np.array(color_tuple[0], dtype=np.uint8)  # 颜色范围的下界
        upper = np.array(color_tuple[1], dtype=np.uint8)  # 颜色范围的上界
        print(f"lower: {lower}, upper: {upper}")
        current_mask = cv2.inRange(image, lower, upper)
        # 使用逻辑或操作合并当前掩码和结果掩码
        mask = cv2.bitwise_or(mask, current_mask)
    return mask


def get_image_by_mask(image, color_mask):
    """
    根据颜色掩码获取图像区域
    :param image: CV2图像
    :param color_mask: 颜色掩码
    :return: 图像区域
    """
    # 提取原图中符合掩码的图像区域
    result = cv2.bitwise_and(image, image, mask=color_mask)
    # 转换为BGRA颜色空间，确保图像有Alpha通道
    result = convert_image_color_space(result, ColorSpace.BGRA)
    # 创建一个与掩码相反的掩码，用于标记要设置为透明的区域
    inverse_mask = cv2.bitwise_not(color_mask)
    # 将反向掩码应用到Alpha通道上，将对应区域的Alpha值设为0，使其透明
    result[:, :, 3][inverse_mask > 0] = 0
    return result


def create_overlay_image(background_image, overlay_image, region=None, expand_pixels=10, gray_offset_scale=200,
                         brightness_scale=1.0):
    """
    将具有透明度的前景图像融合到背景图像的指定区域。

    参数:
    :param background_image: 原始CV2图像（背景图像）
    :param overlay_image: 前景CV2图像（覆盖或叠加的图像，支持透明背景）
    :param region: tuple (start_x, start_y, end_x, end_y) 指定混合区域的左上角 (start_x, start_y) 和右下角 (end_x, end_y) 坐标。
    :param expand_pixels: int 每侧扩展的像素数
    :param gray_offset_scale: int 控制灰度偏移范围的缩放值
    :param brightness_scale: float 控制最终图像的亮度，默认为1.0（无变化）
    """

    # 将背景图像转换为灰度图（用于处理亮度和偏移效果）
    background_gray = convert_image_color_space(background_image, ColorSpace.GRAY).astype(int)

    # 确保前景图像带有 alpha 通道（透明度信息）
    if overlay_image.shape[2] == 4:
        overlay_alpha = overlay_image[:, :, 3] / 255.0  # 获取 alpha 通道并标准化为 [0, 1] 之间
        overlay_image = overlay_image[:, :, :3]  # 提取 RGB 通道，忽略 alpha 通道
    else:
        overlay_alpha = np.ones(overlay_image.shape[:2])  # 若无 alpha 通道，设置为全不透明

    # 从背景图像中提取指定区域 (start_x, start_y) 到 (end_x, end_y) 的部分
    # 如果没有指定 region，则默认使用背景图像的完整区域
    if region is None:
        background_shape = background_image.shape
        region = (0, 0, background_shape[1], background_shape[0])
    start_x, start_y, end_x, end_y = region
    region_width, region_height = end_x - start_x, end_y - start_y
    region_gray = background_gray[start_y:end_y, start_x:end_x]  # 获取区域内的灰度图像

    # 调整前景图像的尺寸，使其与目标区域相同
    resized_overlay = cv2.resize(overlay_image, (region_width, region_height), interpolation=cv2.INTER_NEAREST).astype(
        int)
    resized_alpha = cv2.resize(overlay_alpha, (region_width, region_height), interpolation=cv2.INTER_NEAREST)

    # 扩展前景图像和 alpha 通道，以便能够处理周围的边缘（扩展像素）
    overlay_height, overlay_width, overlay_channels = resized_overlay.shape
    expanded_overlay = np.zeros(
        (overlay_height + 2 * expand_pixels, overlay_width + 2 * expand_pixels, overlay_channels))
    expanded_alpha = np.zeros((overlay_height + 2 * expand_pixels, overlay_width + 2 * expand_pixels))

    # 将前景图像和 alpha 通道放置于扩展图像的中心
    expanded_overlay[expand_pixels:-expand_pixels, expand_pixels:-expand_pixels, :] = resized_overlay
    expanded_alpha[expand_pixels:-expand_pixels, expand_pixels:-expand_pixels] = resized_alpha

    # 使用邻近像素填充扩展区域的边缘，使得扩展区域的像素颜色渐变
    for i in range(expand_pixels):
        # 左侧和右侧填充颜色
        expanded_overlay[expand_pixels:-expand_pixels, i, :] = resized_overlay[:, 0, :]
        expanded_overlay[expand_pixels:-expand_pixels, -i - 1, :] = resized_overlay[:, -1, :]
        # 上方和下方填充颜色
        expanded_overlay[i, :, :] = expanded_overlay[expand_pixels, :, :]
        expanded_overlay[-i - 1, :, :] = expanded_overlay[-expand_pixels - 1, :, :]
        # 填充透明度信息
        expanded_alpha[expand_pixels:-expand_pixels, i] = resized_alpha[:, 0]
        expanded_alpha[expand_pixels:-expand_pixels, -i - 1] = resized_alpha[:, -1]
        expanded_alpha[i, :] = expanded_alpha[expand_pixels, :]
        expanded_alpha[-i - 1, :] = expanded_alpha[-expand_pixels - 1, :]

    # 计算灰度偏移量（用于创建立体效果）
    gray_offset_map = (region_gray - 128) / gray_offset_scale  # 灰度值减去128后按比例缩放
    gray_offset_floor = (np.floor(gray_offset_map) + expand_pixels).astype(int)  # 向下取整后偏移
    gray_offset_ceil = (np.ceil(gray_offset_map) + expand_pixels).astype(int)  # 向上取整后偏移

    # 计算插值权重，用于双线性插值
    interp_weight_1 = (gray_offset_map - np.floor(gray_offset_map)).flatten()
    interp_weight_2 = (np.ceil(gray_offset_map) - gray_offset_map).flatten()

    # 创建融合区域的网格坐标
    x_grid, y_grid = np.meshgrid(range(expanded_overlay.shape[1] - 2 * expand_pixels),
                                 range(expanded_overlay.shape[0] - 2 * expand_pixels))
    x_floor, y_floor = x_grid + gray_offset_floor, y_grid + gray_offset_floor  # 向下取整坐标
    x_ceil, y_ceil = x_grid + gray_offset_ceil, y_grid + gray_offset_ceil  # 向上取整坐标

    # 扩展图像与透明度的双线性插值
    c1 = expanded_overlay[y_floor.flatten(), x_floor.flatten(), :]
    c2 = expanded_overlay[y_ceil.flatten(), x_ceil.flatten(), :]
    alpha_floor = expanded_alpha[y_floor.flatten(), x_floor.flatten()]
    alpha_ceil = expanded_alpha[y_ceil.flatten(), x_ceil.flatten()]

    # 使用线性插值计算新颜色通道
    red_channel = np.where(interp_weight_1 == 0, c1[:, 0], c2[:, 0] * interp_weight_1 + c1[:, 0] * interp_weight_2)
    green_channel = np.where(interp_weight_1 == 0, c1[:, 1], c2[:, 1] * interp_weight_1 + c1[:, 1] * interp_weight_2)
    blue_channel = np.where(interp_weight_1 == 0, c1[:, 2], c2[:, 2] * interp_weight_1 + c1[:, 2] * interp_weight_2)
    alpha_channel = np.where(interp_weight_1 == 0, alpha_floor,
                             alpha_ceil * interp_weight_1 + alpha_floor * interp_weight_2)

    # 继续处理图像合成
    # 假设 red_channel, green_channel, blue_channel 已经计算完
    # 叠加效果：根据偏移的灰度值调整颜色的亮度，并准备最终的 RGB 图像
    overlay_arr = np.array([red_channel.reshape(region_gray.shape),
                            green_channel.reshape(region_gray.shape),
                            blue_channel.reshape(region_gray.shape)])
    # 使用 brightness_scale 调整亮度
    overlay_arr = (overlay_arr * region_gray / 250 * brightness_scale).transpose((1, 2, 0)).astype(np.uint8)

    # 使用 alpha 通道将前景和背景平滑融合
    alpha_channel = alpha_channel.reshape(region_gray.shape)
    overlay_result = (overlay_arr * alpha_channel[:, :, None] +
                      background_image[start_y:end_y, start_x:end_x, :3] * (1 - alpha_channel[:, :, None])).astype(
        np.uint8)

    # 将最终融合后的图像更新到背景图像上
    background_image[start_y:end_y, start_x:end_x, :3] = overlay_result

    return background_image


class ImageTool:
    image = None

    def __init__(self, image):
        self.image = image

    def get_color_space(self):
        return get_image_color_space(self.image)

    def get_channel_count(self):
        return get_image_channel_count(self.image)

    def convert_color_color(self, target_color_space):
        self.image = convert_image_color_space(self.image, target_color_space)

    def get_color_mask(self, color_array):
        """
        根据颜色范围获取图像源码
        :param color_array: 参考 get_image_color_mask.color_array
        :return: 图像掩码
        """
        return get_image_color_mask(self.image, color_array)

    def get_image_by_mask(self, color_mask):
        return get_image_by_mask(self.image, color_mask)

    def get_image_body_and(self, color_array):
        """
        根据颜色范围获取图像区域
        :param color_array:参考 get_image_color_mask.color_array
        :return: 图像区域
        """
        mask = self.get_color_mask(color_array)  # 创建颜色掩码
        return self.get_image_by_mask(mask)

    def get_image_body_not(self, color_array):
        """
        根据颜色范围删除图像区域
        :param color_array: 参考 get_image_color_mask.color_array
        :return: 图像区域
        """
        mask = self.get_color_mask(color_array)  # 创建颜色掩码
        mask = cv2.bitwise_not(mask)  # 反转颜色掩码
        return self.get_image_by_mask(mask)

    def overlay_image(self, target_image, region=None, expand_pixels=10, gray_offset_scale=200, brightness_scale=1.0,
                      replace=False):
        """
         参考：create_overlay_image
        :param target_image:
        :param region:
        :param expand_pixels:
        :param gray_offset_scale:
        :param brightness_scale:
        :param replace: 是否替换当前图像
        :return:
        """
        img = create_overlay_image(self.image, target_image, region=region, expand_pixels=expand_pixels,
                                   gray_offset_scale=gray_offset_scale, brightness_scale=brightness_scale)
        if replace:
            self.image = img
        return img

    @staticmethod
    def build(image):
        return ImageTool(image)
