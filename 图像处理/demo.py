import cv2
import numpy as np
from image import ColorSpace, ImageTool


def showBGRA(image):
    B, G, R, *A = cv2.split(image)
    cv2.imshow('B', B)
    cv2.imshow('G', G)
    cv2.imshow('R', R)
    if A:
        cv2.imshow('A', A[0])


def image_color_test_for():
    # 读取原始图像
    image_path = './source/sock.png'
    image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)  # 保留Alpha通道（如果适用）
    tool = ImageTool(image)
    tool.convert_color_color(ColorSpace.HSV)  # 转换成HSV颜色空间
    # 定义颜色范围遍历的步长
    step_size = 10

    # 循环遍历颜色范围
    for b in range(0, 256, step_size):
        for g in range(0, 256, step_size):
            for r in range(0, 256, step_size):
                lower_bgr = np.array([b, g, r])
                upper_bgr = np.array([b + step_size, g + step_size, r + step_size])
                # 创建特定颜色范围的掩码
                mask = tool.get_color_mask([[lower_bgr, upper_bgr]])
                # 形态学操作，去除噪声
                kernel = np.ones((5, 5), np.uint8)
                mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
                mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

                # 找到主体的轮廓
                contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

                if contours:
                    print(f"发现主成份，颜色范围: {lower_bgr}-{upper_bgr}，轮廓数量: {len(contours)}")
                    # 这里可以根据需求进一步处理找到的轮廓，比如提取主体图像等操作
                    # 以下是一个简单示例，提取最大轮廓并绘制在空白图像上
                    max_contour = max(contours, key=cv2.contourArea)
                    contour_image = np.zeros_like(image)
                    # cv2.drawContours(contour_image, [max_contour], -1, (255, 255, 255), thickness=cv2.FILLED)
                    # cv2.imshow('Contour Image', contour_image)
                    # cv2.waitKey(500)  # 显示图像一段时间，可根据需要调整等待时间
                    # cv2.destroyAllWindows()


def logo_test():
    path = './source/logo.jpg'
    image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    tool = ImageTool.build(image)
    print(f"image color space : {tool.get_color_space()}")

    # BGR颜色空间
    # 提取蓝色部份
    tool.convert_color_color(ColorSpace.BGR)  # 转换BGR颜色空间
    color_bgr_and = [[(60, 30, 0), (220, 150, 150)]]
    result_bgr_and = tool.get_image_body_and(color_bgr_and)
    cv2.imwrite('./output/logo_bgr_and.png', result_bgr_and)
    cv2.imshow("logo_bgr_and", result_bgr_and)
    # showBGRA(result_bgr_and)

    # 白色区域设置为透明
    color_bgr_not = [[(200, 200, 200), (255, 255, 255)]]
    result_bgr_not = tool.get_image_body_not(color_bgr_not)
    cv2.imwrite('./output/logo_bgr_not.png', result_bgr_not)
    cv2.imshow("logo_bgr_not", result_bgr_not)
    # showBGRA(result_bgr_not)

    # HSV颜色空间
    # 提取蓝色部份
    tool.convert_color_color(ColorSpace.HSV)  # 转换HSV颜色空间
    color_hsv_and = [[(100, 100, 0), (140, 255, 255)]]
    result_hsv_and = tool.get_image_body_and(color_hsv_and)
    cv2.imwrite('./output/logo_hsv_and.png', result_hsv_and)
    cv2.imshow("logo_hsv_and", result_hsv_and)
    # showBGRA(result_hsv_and)

    # 白色区域设置为透明
    color_hsv_not = [[(0, 0, 0), (179, 30, 255)]]
    result_hsv_not = tool.get_image_body_not(color_hsv_not)
    cv2.imwrite('./output/logo_hsv_not.png', result_hsv_not)
    cv2.imshow("logo_hsv_not", result_hsv_not)
    # showBGRA(result_hsv_not)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


def sock_test():
    path = './source/sock.jpg'
    image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    tool = ImageTool.build(image)
    print(f"image color space : {tool.get_color_space()}")

    # BGR颜色空间
    # 提取白色部份
    tool.convert_color_color(ColorSpace.BGR)  # 转换BGR颜色空间
    color_bgr_and = [[(200, 200, 200), (255, 255, 255)], [(170, 170, 170), (200, 200, 200)]]
    result_bgr_and = tool.get_image_body_and(color_bgr_and)
    cv2.imwrite('./output/sock_bgr_and.png', result_bgr_and)
    cv2.imshow("sock_bgr_and", result_bgr_and)
    # showBGRA(result_bgr_and)

    # 非白色区域设置为透明
    color_bgr_not = [[(160, 170, 175), (190, 200, 200)], [(0, 0, 0), (160, 170, 175)], [(60, 80, 100), (100, 130, 170)],
                     [(100, 130, 170), (155, 180, 210)]]
    result_bgr_not = tool.get_image_body_not(color_bgr_not)
    cv2.imwrite('./output/sock_bgr_not.png', result_bgr_not)
    cv2.imshow("sock_bgr_not", result_bgr_not)
    # showBGRA(result_bgr_not)

    # HSV颜色空间
    # 提取白色部份
    tool.convert_color_color(ColorSpace.HSV)  # 转换HSV颜色空间
    color_hsv_and = [[(0, 0, 250), (179, 20, 255)], [(0, 0, 200), (179, 20, 255)]]
    result_hsv_and = tool.get_image_body_and(color_hsv_and)
    cv2.imwrite('./output/sock_hsv_and.png', result_hsv_and)
    cv2.imshow("sock_hsv_and", result_hsv_and)
    # showBGRA(result_hsv_and)

    # 非白色区域设置为透明
    color_hsv_not = [[(0, 0, 100), (30, 20, 200)], [(10, 10, 150), (25, 50, 255)], [(10, 20, 20), (20, 60, 200)],
                     [(20, 20, 100), (40, 60, 200)],
                     [(20, 20, 20), (30, 60, 200)]]
    result_hsv_not = tool.get_image_body_not(color_hsv_not)
    cv2.imwrite('./output/sock_hsv_not.png', result_hsv_not)
    cv2.imshow("sock_hsv_not", result_hsv_not)
    # showBGRA(result_hsv_not)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


def overlay_image_test_1():
    # 读取背景图像
    sock_path = './source/sock.jpg'
    sock_image = cv2.imread(sock_path, cv2.IMREAD_UNCHANGED)  # 保留Alpha通道（如果适用）
    sock_tool = ImageTool.build(sock_image)
    print(f"sock image color space : {sock_tool.get_color_space()}")

    # 读取Logo图像并将其特定颜色范围内的区域设置为透明
    logo_path = './source/logo.jpg'
    logo_image = cv2.imread(logo_path, cv2.IMREAD_UNCHANGED)  # 使用IMREAD_UNCHANGED以保留Alpha通道
    logo_tool = ImageTool.build(logo_image)
    # 提取LOGO蓝色部份
    logo_tool.convert_color_color(ColorSpace.BGR)  # 转换BGR颜色空间
    logo_color_bgr_and = [[(60, 30, 0), (220, 150, 150)]]
    logo_image_body = logo_tool.get_image_body_and(logo_color_bgr_and)
    # cv2.imshow('Logo Image', logo_image_body)
    # 将Logo图像叠加到背景图像上
    sock_tool.overlay_image(logo_image_body, region=(260, 200, 340, 250), expand_pixels=10, gray_offset_scale=200,
                            brightness_scale=1.0, replace=True)
    sock_tool.overlay_image(logo_image_body, region=(430, 200, 510, 250), expand_pixels=10, gray_offset_scale=200,
                            brightness_scale=1.0, replace=True)

    # 猫
    size = (80, 60)
    cat_path = './source/cat.png'
    cat_image = cv2.imread(cat_path, cv2.IMREAD_UNCHANGED)  # 使用IMREAD_UNCHANGED以保留Alpha通道
    sock_tool.overlay_image(cat_image, region=(250, 450, 330, 510), expand_pixels=10, gray_offset_scale=200,
                            brightness_scale=1.0, replace=True)
    sock_tool.overlay_image(cat_image, region=(510, 410, 590, 470), expand_pixels=10, gray_offset_scale=200,
                            brightness_scale=1.0, replace=True)

    # 保存结果图像
    cv2.imwrite('./output/sock_overlay.png', sock_tool.image)

    # 显示结果图像
    cv2.imshow('Sock Overlay Image', sock_tool.image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def overlay_image_test_2():
    # 读取背景图像
    source_path = './source/blanket.jpg'
    source_image = cv2.imread(source_path, cv2.IMREAD_UNCHANGED)  # 保留Alpha通道（如果适用）
    # cv2.imshow('Blanket Image', source_image)
    tool = ImageTool.build(source_image)
    print(f"source image color space : {tool.get_color_space()}")
    # 融合图像
    fusion_path = './source/paint.jpg'
    fusion_image = cv2.imread(fusion_path, cv2.IMREAD_UNCHANGED)  # 使用IMREAD_UNCHANGED以保留Alpha通道
    # cv2.imshow('Paint Image', fusion_image)

    tool.overlay_image(fusion_image, region=None, expand_pixels=10, gray_offset_scale=200, brightness_scale=1.2,
                       replace=True)
    cv2.imshow('Blanket Paint Image', tool.image)

    # 保存结果图像
    cv2.imwrite('./output/blanket_paint.png', tool.image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def overlay_image_test_3():
    # 读取背景图像
    source_path = './source/cup.png'
    source_image = cv2.imread(source_path, cv2.IMREAD_UNCHANGED)  # 保留Alpha通道（如果适用）
    # cv2.imshow('Cup Image', source_image)
    tool = ImageTool.build(source_image)
    print(f"source image color space : {tool.get_color_space()}")
    # 融合图像
    fusion_path = './source/cat.png'
    fusion_image = cv2.imread(fusion_path, cv2.IMREAD_UNCHANGED)  # 使用IMREAD_UNCHANGED以保留Alpha通道
    # cv2.imshow('Cat Image', fusion_image)

    tool.overlay_image(fusion_image, region=(80, 180, 400, 400), expand_pixels=10, gray_offset_scale=200,
                       brightness_scale=1.0, replace=True)
    cv2.imshow('Cup Cat Image', tool.image)

    # 保存结果图像
    cv2.imwrite('./output/cup_cat.png', tool.image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # image_color_test_for()
    logo_test()
    overlay_image_test_1()
    overlay_image_test_2()
    overlay_image_test_3()
