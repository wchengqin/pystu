import numpy as np
import cv2


def blend_images_in_region_with_alpha(background_path, foreground_path, output_path, region):
    """
    将具有透明度的前景图像混合到背景图像的指定区域。

    参数:
    - background_path: str, 背景图像的路径。
    - foreground_path: str, 具有透明度的前景图像（PNG）的路径。
    - output_path: str, 保存输出图像的路径。
    - region: tuple, (x1, y1, x2, y2) 指定区域的左上角 (x1, y1) 和
              右下角 (x2, y2) 坐标，用于混合到背景图像。
    """
    # 1. 读取背景和前景图像
    bimg = cv2.imread(background_path)
    if bimg is None:
        raise FileNotFoundError(f"背景图像路径无效: {background_path}")

    bgray = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY).astype(int)

    # 读取带有 alpha 通道的前景图像
    fimg0 = cv2.imread(foreground_path, cv2.IMREAD_UNCHANGED)
    if fimg0 is None:
        raise FileNotFoundError(f"前景图像路径无效: {foreground_path}")

    if fimg0.shape[2] == 4:  # 确保前景图像有 alpha 通道
        alpha_channel = fimg0[:, :, 3] / 255.0
        fimg0 = fimg0[:, :, :3]  # 提取 RGB 通道
    else:
        alpha_channel = np.ones(fimg0.shape[:2])  # 若无 alpha 通道，则设置为不透明

    # 从背景图像中提取指定区域
    x1, y1, x2, y2 = region
    region_width = x2 - x1
    region_height = y2 - y1
    region_bgray = bgray[y1:y2, x1:x2]

    # 将前景图像和 alpha 通道调整到与区域尺寸匹配
    fimg = cv2.resize(fimg0, (region_width, region_height), interpolation=cv2.INTER_NEAREST).astype(int)
    alpha_channel = cv2.resize(alpha_channel, (region_width, region_height), interpolation=cv2.INTER_NEAREST)

    # 2. 创建扩展的前景图像（每侧扩展 13 个像素）
    Y, X, N = fimg.shape
    exfimg = np.zeros((Y + 26, X + 26, N))
    exalpha = np.zeros((Y + 26, X + 26))  # 扩展后的 alpha 通道
    exfimg[13:-13, 13:-13, :] = fimg  # 将前景图像置于扩展数组的中心
    exalpha[13:-13, 13:-13] = alpha_channel  # 将 alpha 通道置于中心

    # 扩展 RGB 和 alpha 通道的边缘
    for i in range(13):
        exfimg[13:-13, i, :] = fimg[:, 0, :]  # 左侧
        exfimg[13:-13, -i - 1, :] = fimg[:, -1, :]  # 右侧
        exalpha[13:-13, i] = alpha_channel[:, 0]  # 左侧 alpha
        exalpha[13:-13, -i - 1] = alpha_channel[:, -1]  # 右侧 alpha

    for i in range(13):
        exfimg[i, :, :] = exfimg[13, :, :]  # 上方
        exfimg[-i - 1, :, :] = exfimg[-14, :, :]  # 下方
        exalpha[i, :] = exalpha[13, :]  # 上方 alpha
        exalpha[-i - 1, :] = exalpha[-14, :]  # 下方 alpha

    # 3. 使用透明度处理像素映射
    goffset = (region_bgray - 128) / 10
    offsetLim1 = (np.floor(goffset) + 13).astype(int)
    offsetLim2 = (np.ceil(goffset) + 13).astype(int)
    sep1 = (goffset - np.floor(goffset)).flatten()
    sep2 = (np.ceil(goffset) - goffset).flatten()

    XX, YY = np.meshgrid(range(exfimg.shape[1] - 26), range(exfimg.shape[0] - 26))
    XX1, YY1 = XX + offsetLim1, YY + offsetLim1  # 向下取整的偏移坐标
    XX2, YY2 = XX + offsetLim2, YY + offsetLim2  # 向上取整的偏移坐标

    # 展平坐标以便于像素访问
    c1 = exfimg[YY1.flatten(), XX1.flatten(), :]
    c2 = exfimg[YY2.flatten(), XX2.flatten(), :]
    alpha1 = exalpha[YY1.flatten(), XX1.flatten()]
    alpha2 = exalpha[YY2.flatten(), XX2.flatten()]

    # 计算新颜色通道，使用插值
    p1 = np.where(sep1 == 0, c1[:, 0], c2[:, 0] * sep1 + c1[:, 0] * sep2)
    p2 = np.where(sep1 == 0, c1[:, 1], c2[:, 1] * sep1 + c1[:, 1] * sep2)
    p3 = np.where(sep1 == 0, c1[:, 2], c2[:, 2] * sep1 + c1[:, 2] * sep2)
    alpha_blend = np.where(sep1 == 0, alpha1, alpha2 * sep1 + alpha1 * sep2)

    # 4. 使用透明度应用正片叠底混合模式
    newarr = np.array([p1.reshape(region_bgray.shape), p2.reshape(region_bgray.shape), p3.reshape(region_bgray.shape)])
    newarr = (newarr * region_bgray / 250).transpose((1, 2, 0)).astype(np.uint8)

    # 使用 alpha 蒙版将透明区域与背景混合
    alpha_blend = alpha_blend.reshape(region_bgray.shape)
    newarr = (newarr * alpha_blend[:, :, None] + bimg[y1:y2, x1:x2] * (1 - alpha_blend[:, :, None])).astype(np.uint8)

    # 将混合结果放回原背景图像中
    bimg[y1:y2, x1:x2] = newarr

    # 保存最终输出
    cv2.imwrite(output_path, bimg)
    return bimg


# 示例使用
# blend_images_in_region_with_alpha('./images/16.png', './images/19.png', './images/result.jpg',
#                                   (700, 400, 1200, 1200))
# blend_images_in_region_with_alpha('./images/22.png', './images/23.png', './images/result.jpg',
#                                   (300, 900, 650, 1300))
img=blend_images_in_region_with_alpha('../source/cup.png', '../source/cat.png', './output/result.jpg',
                                  (100, 180, 420, 420))
# blend_images_in_region_with_alpha('./images/20.png', './images/24.png', './images/result1.jpg',
#                                   (400, 1500, 1050, 2100))
cv2.imshow('Cup Cat Image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()