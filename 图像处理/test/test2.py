import numpy as np
import cv2


def create_fusion_image(background_image, overlay_image, region=None, expand_pixels=10, gray_offset_scale=200):
    """
    将具有透明度的前景图像融合到背景图像的指定区域。

    参数:
    :param background_image: 原始CV2图像（背景图像）
    :param overlay_image: 前景CV2图像（覆盖或叠加的图像，支持透明背景）
    :param region: tuple (start_x, start_y, end_x, end_y) 指定混合区域的左上角 (start_x, start_y) 和右下角 (end_x, end_y) 坐标。
    :param expand_pixels: int 每侧扩展的像素数
    :param gray_offset_scale: int 控制灰度偏移范围的缩放值
    """

    # 将背景图像转换为灰度图（用于处理亮度和偏移效果）
    background_gray = cv2.cvtColor(background_image, cv2.COLOR_BGR2GRAY).astype(int)

    # 确保前景图像带有 alpha 通道（透明度信息）
    if overlay_image.shape[2] == 4:
        overlay_alpha = overlay_image[:, :, 3] / 255.0  # 获取 alpha 通道并标准化为 [0, 1] 之间
        overlay_image = overlay_image[:, :, :3]  # 提取 RGB 通道，忽略 alpha 通道
    else:
        overlay_alpha = np.ones(overlay_image.shape[:2])  # 若无 alpha 通道，设置为全不透明

    # 从背景图像中提取指定区域 (start_x, start_y) 到 (end_x, end_y) 的部分
    start_x, start_y, end_x, end_y = region
    region_width, region_height = end_x - start_x, end_y - start_y
    region_gray = background_gray[start_y:end_y, start_x:end_x]  # 获取区域内的灰度图像

    # 调整前景图像的尺寸，使其与目标区域相同
    resized_overlay = cv2.resize(overlay_image, (region_width, region_height), interpolation=cv2.INTER_NEAREST).astype(
        int)
    resized_alpha = cv2.resize(overlay_alpha, (region_width, region_height), interpolation=cv2.INTER_NEAREST)

    # 扩展前景图像和 alpha 通道，以便能够处理周围的边缘（扩展像素）
    overlay_height, overlay_width, overlay_channels = resized_overlay.shape
    expanded_overlay = np.zeros(
        (overlay_height + 2 * expand_pixels, overlay_width + 2 * expand_pixels, overlay_channels))
    expanded_alpha = np.zeros((overlay_height + 2 * expand_pixels, overlay_width + 2 * expand_pixels))

    # 将前景图像和 alpha 通道放置于扩展图像的中心
    expanded_overlay[expand_pixels:-expand_pixels, expand_pixels:-expand_pixels, :] = resized_overlay
    expanded_alpha[expand_pixels:-expand_pixels, expand_pixels:-expand_pixels] = resized_alpha

    # 使用邻近像素填充扩展区域的边缘，使得扩展区域的像素颜色渐变
    for i in range(expand_pixels):
        # 左侧和右侧填充颜色
        expanded_overlay[expand_pixels:-expand_pixels, i, :] = resized_overlay[:, 0, :]
        expanded_overlay[expand_pixels:-expand_pixels, -i - 1, :] = resized_overlay[:, -1, :]
        # 上方和下方填充颜色
        expanded_overlay[i, :, :] = expanded_overlay[expand_pixels, :, :]
        expanded_overlay[-i - 1, :, :] = expanded_overlay[-expand_pixels - 1, :, :]
        # 填充透明度信息
        expanded_alpha[expand_pixels:-expand_pixels, i] = resized_alpha[:, 0]
        expanded_alpha[expand_pixels:-expand_pixels, -i - 1] = resized_alpha[:, -1]
        expanded_alpha[i, :] = expanded_alpha[expand_pixels, :]
        expanded_alpha[-i - 1, :] = expanded_alpha[-expand_pixels - 1, :]

    # 计算灰度偏移量（用于创建立体效果）
    gray_offset_map = (region_gray - 128) / gray_offset_scale  # 灰度值减去128后按比例缩放
    gray_offset_floor = (np.floor(gray_offset_map) + expand_pixels).astype(int)  # 向下取整后偏移
    gray_offset_ceil = (np.ceil(gray_offset_map) + expand_pixels).astype(int)  # 向上取整后偏移

    # 计算插值权重，用于双线性插值
    interp_weight_1 = (gray_offset_map - np.floor(gray_offset_map)).flatten()
    interp_weight_2 = (np.ceil(gray_offset_map) - gray_offset_map).flatten()

    # 创建融合区域的网格坐标
    x_grid, y_grid = np.meshgrid(range(expanded_overlay.shape[1] - 2 * expand_pixels),
                                 range(expanded_overlay.shape[0] - 2 * expand_pixels))
    x_floor, y_floor = x_grid + gray_offset_floor, y_grid + gray_offset_floor  # 向下取整坐标
    x_ceil, y_ceil = x_grid + gray_offset_ceil, y_grid + gray_offset_ceil  # 向上取整坐标

    # 扩展图像与透明度的双线性插值
    c1 = expanded_overlay[y_floor.flatten(), x_floor.flatten(), :]
    c2 = expanded_overlay[y_ceil.flatten(), x_ceil.flatten(), :]
    alpha_floor = expanded_alpha[y_floor.flatten(), x_floor.flatten()]
    alpha_ceil = expanded_alpha[y_ceil.flatten(), x_ceil.flatten()]

    # 使用线性插值计算新颜色通道
    red_channel = np.where(interp_weight_1 == 0, c1[:, 0], c2[:, 0] * interp_weight_1 + c1[:, 0] * interp_weight_2)
    green_channel = np.where(interp_weight_1 == 0, c1[:, 1], c2[:, 1] * interp_weight_1 + c1[:, 1] * interp_weight_2)
    blue_channel = np.where(interp_weight_1 == 0, c1[:, 2], c2[:, 2] * interp_weight_1 + c1[:, 2] * interp_weight_2)
    alpha_channel = np.where(interp_weight_1 == 0, alpha_floor,
                             alpha_ceil * interp_weight_1 + alpha_floor * interp_weight_2)

    # 叠加效果：根据偏移的灰度值调整颜色的亮度，并准备最终的 RGB 图像
    overlay_arr = np.array([red_channel.reshape(region_gray.shape),
                            green_channel.reshape(region_gray.shape),
                            blue_channel.reshape(region_gray.shape)])
    overlay_arr = (overlay_arr * region_gray / 250).transpose((1, 2, 0)).astype(np.uint8)  # 调整颜色亮度

    # 使用 alpha 通道将前景和背景平滑融合
    alpha_channel = alpha_channel.reshape(region_gray.shape)
    overlay_result = (overlay_arr * alpha_channel[:, :, None] +
                      background_image[start_y:end_y, start_x:end_x, :3] * (1 - alpha_channel[:, :, None])).astype(
        np.uint8)

    # 将最终融合后的图像更新到背景图像上
    background_image[start_y:end_y, start_x:end_x, :3] = overlay_result

    return background_image


if __name__ == "__main__":
    # 读取背景图像（可能带有 alpha 通道）
    background_path = '../source/cup.png'
    background_image = cv2.imread(background_path, cv2.IMREAD_UNCHANGED)
    cv2.imshow('Background Image', background_image)  # 显示背景图像

    # 读取前景图像（可能带有 alpha 通道）
    overlay_path = '../source/cat.png'
    overlay_image = cv2.imread(overlay_path, cv2.IMREAD_UNCHANGED)
    cv2.imshow('Overlay Image', overlay_image)  # 显示前景图像

    # 融合图像，设置融合区域和扩展参数
    result_image = create_fusion_image(background_image, overlay_image, region=(100, 180, 420, 420), expand_pixels=10,
                                       gray_offset_scale=200)

    cv2.imshow('Result Image', result_image)  # 显示融合后的图像
    cv2.waitKey(0)  # 等待键盘输入以关闭窗口
    cv2.destroyAllWindows()  # 销毁所有显示窗口
