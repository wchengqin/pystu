import cv2
import numpy as np
from scipy.sparse import lil_matrix, csr_matrix
from scipy.sparse.linalg import spsolve


def create_fusion_poisson_image(source_image, fusion_image, mask=None, offset=(0, 0)):
    """
    使用泊松方程进行图像融合。
    :param source_image: 原始CV2图像
    :param fusion_image: 融合CV2图像
    :param mask: 掩码 (numpy.ndarray)，定义了源图像中要融合的部分。如果为 None，则默认使用整个源图像。
    :param offset: 源图像在目标图像上的偏移量 (tuple)

    返回:
    融合后的图像
    """
    # 如果没有提供掩码，则默认使用整个源图像
    if mask is None:
        mask = np.ones(source_image.shape[:2], dtype=np.uint8) * 255

    # 将图像转换为浮点数
    source_image = source_image.astype(np.float64)
    fusion_image = fusion_image.astype(np.float64)

    # 获取图像尺寸
    target_height, target_width, _ = fusion_image.shape
    source_height, source_width, _ = source_image.shape

    # 计算源图像在目标图像中的位置
    x_start, y_start = offset
    x_end, y_end = x_start + source_width, y_start + source_height

    # 确保源图像在目标图像内
    if x_end > target_width or y_end > target_height:
        raise ValueError(f"Source image exceeds the boundaries of the target image. "
                         f"Source image dimensions: ({source_width}, {source_height}), "
                         f"Target image dimensions: ({target_width}, {target_height}), "
                         f"Offset: ({x_start}, {y_start})")

    # 初始化融合后的图像
    result_image = fusion_image.copy()

    # 创建稀疏矩阵
    num_pixels = target_height * target_width
    A = lil_matrix((num_pixels, num_pixels))

    # 创建结果向量
    result_channels = []

    # 对每个通道独立处理
    for channel in range(3):
        b = np.zeros(num_pixels)

        for y in range(source_height):
            for x in range(source_width):
                if mask[y, x] > 0:
                    # 当前像素的位置
                    idx = (y + y_start) * target_width + (x + x_start)

                    # 泊松方程的核心部分
                    A[idx, idx] = 4
                    if y > 0 and mask[y - 1, x] > 0:
                        A[idx, (y + y_start - 1) * target_width + (x + x_start)] = -1
                    if y < source_height - 1 and mask[y + 1, x] > 0:
                        A[idx, (y + y_start + 1) * target_width + (x + x_start)] = -1
                    if x > 0 and mask[y, x - 1] > 0:
                        A[idx, (y + y_start) * target_width + (x + x_start - 1)] = -1
                    if x < source_width - 1 and mask[y, x + 1] > 0:
                        A[idx, (y + y_start) * target_width + (x + x_start + 1)] = -1

                    # 计算右端项
                    b[idx] = 4 * source_image[y, x, channel] - \
                             (source_image[max(y - 1, 0), x, channel] +
                              source_image[min(y + 1, source_height - 1), x, channel] +
                              source_image[y, max(x - 1, 0), channel] +
                              source_image[y, min(x + 1, source_width - 1), channel])

        # 转换为CSR格式并解泊松方程
        A_csr = A.tocsr()
        x_channel = spsolve(A_csr, b)
        x_channel = x_channel.reshape((target_height, target_width))

        # 保存每个通道的结果
        result_channels.append(x_channel)

    # 合并通道并赋值给结果图像
    for y in range(source_height):
        for x in range(source_width):
            if mask[y, x] > 0:
                for channel in range(3):
                    result_image[y + y_start, x + x_start, channel] = result_channels[channel][y + y_start, x + x_start]

    # 将结果图像转换回uint8类型
    result_image = np.clip(result_image, 0, 255).astype(np.uint8)

    return result_image


# 示例用法
if __name__ == "__main__":
    # 加载图像
    source_image = cv2.imread('../source/blanket.jpg')
    fusion_image = cv2.imread('../source/paint.jpg')

    # 检查图像是否加载成功
    if source_image is None or fusion_image is None:
        raise ValueError("One or both images could not be loaded. Please check the file paths.")

    # 获取图像尺寸
    source_height, source_width, _ = source_image.shape
    target_height, target_width, _ = fusion_image.shape

    # 计算一个合理的偏移量
    x_offset = (target_width - source_width) // 2
    y_offset = (target_height - source_height) // 2

    # 如果目标图像太小，调整目标图像的大小
    if target_height < source_height or target_width < source_width:
        new_target_width = max(target_width, source_width)
        new_target_height = max(target_height, source_height)
        fusion_image = cv2.resize(fusion_image, (new_target_width, new_target_height))
        target_height, target_width, _ = fusion_image.shape
        x_offset = (target_width - source_width) // 2
        y_offset = (target_height - source_height) // 2

    # 确保偏移量有效
    if x_offset + source_width > target_width or y_offset + source_height > target_height:
        raise ValueError(f"Adjusted offset still causes the source image to exceed the boundaries of the target image. "
                         f"Source image dimensions: ({source_width}, {source_height}), "
                         f"Target image dimensions: ({target_width}, {target_height}), "
                         f"Offset: ({x_offset}, {y_offset})")

    # 调用函数
    try:
        result_image = create_fusion_poisson_image(source_image, fusion_image, offset=(x_offset, y_offset))
    except ValueError as e:
        print(f"Error : {e}")
    else:
        # 保存结果图像
        cv2.imwrite('../output/blanket_paint_poisson.png', result_image)
        # 显示结果
        cv2.imshow('Result Image', result_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
