"""
exec(object[, globals[, locals]])

object：必选参数，表示需要被指定的 Python 代码。它必须是字符串或 code 对象。
        如果 object 是一个字符串，该字符串会先被解析为一组 Python 语句，然后再执行（除非发生语法错误）。
        如果 object 是一个 code 对象，那么它只是被简单的执行。
globals：可选参数，表示全局命名空间（存放全局变量），如果被提供，则必须是一个字典对象。
locals：可选参数，表示当前局部命名空间（存放局部变量），如果被提供，可以是任何映射对象。如果该参数被忽略，那么它将会取与 globals 相同的值。
"""

x = 10
expr = """
z = 30
sum = x + y + z
print(sum)
"""

y = 20


def func():
    exec(expr)  # 无参数，可直接引用外部变量
    exec(expr, {'x': 1, 'y': 2})
    exec(expr, {'x': 1, 'y': 2}, {'y': 3, 'z': 4})


func()
what = 'bye byte'
name = 'hello world'
code = """
#print(name,what)
def f(x):
    x = x + 1
    return x

print( 'This is my output.')
print(f(3))
print(name)
def say(w):
    print('say',w)
say(what)
"""
exec(code)  # 不带全局参数时，可以直接引用外部变量
exec(code, {'what': 'go go go', 'name': 'jerry'}, {'what': 'ha ha ha', 'name': 'wang'})  # locals参数优先

str_dict = "{'name':'wang','age':18}"
obj = eval(str_dict)
print(obj, obj.get('age'))

