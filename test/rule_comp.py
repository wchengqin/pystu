# -*- coding: utf-8 -*-

import re


class CompExpr:
    # rea = ['==', '>', '>=', '<', '<=', '<>']

    def __rule_comp(self, v, *args):
        flag = None
        try:
            v = float(v)
            for t in args:
                if t[0] is None or t[1] is None:
                    continue
                rea = t[0].strip()
                v1 = float(t[1].strip())
                temp = None
                if rea == '==':
                    temp = v == v1
                elif rea == '>':
                    temp = v > v1
                elif rea == '>=' or rea == '=>':
                    temp = v >= v1
                elif rea == '<':
                    temp = v < v1
                elif rea == '<=' or rea == '=<':
                    temp = v <= v1
                elif rea == '<>':
                    temp = v != v1
                if temp is None:
                    continue
                flag = temp if flag is None else (flag and temp)
        except Exception as e:
            flag = False
            print(e)
        return flag if flag is not None else False

    def rule_comp(self, v, expr):
        pattern = r'^([-+]?\d+|[-+]?\d+\.\d+)?(==|>|>=|<|<=|<>)?.(==|>|>=|<|<=|<>)?([-+]?\d+|[-+]?\d+\.\d+)?$'
        m = re.match(pattern, expr)
        if m:
            gs = m.groups()
            flag = self.__rule_comp(v, (gs[1], gs[0]), (gs[2], gs[3]))
            return flag
        else:
            return False


if __name__ == "__main__":
    cp = CompExpr()
    ss = ['V>=-1000', '800<=V<1000', '500<=V<800', '200<=V<500', 'V<200']
    for s in ss:
        print(cp.rule_comp(502, s))
