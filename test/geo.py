import json

from pyproj import Proj, CRS, Transformer, transform


def get_area_center(filename):
    data = []
    with open(filename, 'r', encoding='utf-8') as file:
        geo = json.load(file)
        features = geo['features']
        for area in features:
            properties = area['properties']
            geometry = area['geometry']
            row = {'code': properties['unique_id'], 'name': properties['name'], 'coordinates': geometry['coordinates']}
            print_sql(row)
            data.append(row)
    return data


def print_sql(row):
    coordinates = row['coordinates'];
    # print("update sys_area set geo_center='%s,%s' where code='%s';" % (coordinates[0], coordinates[1], row['code']))
    print("update sys_area set geo_center='%s,%s' where name='%s';" % (coordinates[0], coordinates[1], row['name']))


if __name__ == '__main__':
    # geo_file = 'E:\Software\Work\AreaCity\data\CGCS2000-GeoJSON-Geo-ok_geo4_ETD221128-230706-121532.json'
    # get_area_center(geo_file)
    cad_x, cad_y = 464902.096, 2942258.792

    cad_proj = Proj(init='EPSG:4547')  # CGCS2000/3-dgree Gauss-Kruger CM 114E
    wgs84_proj = Proj(proj='latlong', datum='WGS84', ellps='WGS84')
    gps_lon, gps_lat = transform(cad_proj, wgs84_proj, cad_x, cad_y)  # 坐标转换
    print(gps_lon, gps_lat)

    crs_CGCS2000 = CRS.from_epsg(4547)  # CGCS2000/3-dgree Gauss-Kruger  CM 114E
    crs_WGS84 = CRS.from_epsg(4326)  # WGS84地理坐标系

    transformer = Transformer.from_crs(crs_CGCS2000, crs_WGS84)
    gps_lon, gps_lat = transformer.transform(cad_y,cad_x)  # 坐标转换
    print(gps_lon, gps_lat)
