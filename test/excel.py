import os
import sys
import openpyxl
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")

SHEET_TITLE = "2007测试表"


def write07Excel(path):
    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = SHEET_TITLE

    value = [["名称", "价格", "出版社", "语言"],
             ["如何高效读懂一本书", "22.3", "机械工业出版社", "中文"],
             ["暗时间", "32.4", "人民邮电出版社", "中文"],
             ["拆掉思维里的墙", "26.7", "机械工业出版社", "中文"]]
    for i in range(0, 4):
        for j in range(0, len(value[i])):
            sheet.cell(row=i + 1, column=j + 1, value=str(value[i][j]))

    wb.save(path)
    print("写入数据成功！")


def read07Excel(path):
    wb = openpyxl.load_workbook(path)
    sheet = wb[SHEET_TITLE]
    for row in sheet.users:
        for cell in row:
            print(cell.value, "\t", end="")
        print()


def main():
    datapath = config.get("data_path", "temp_path") + "test.xlsx"
    write07Excel(datapath)
    read07Excel(datapath)


main()
