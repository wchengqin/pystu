import json
import openpyxl
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")


def set_bet(data, wbname):
    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = "比赛配置"
    sheet.cell(row=2, column=1, value="Level")
    sheet.cell(row=2, column=2, value="Ante")
    sheet.cell(row=2, column=3, value="Small Blind")
    sheet.cell(row=2, column=4, value="Big Blind")
    sheet.cell(row=2, column=5, value="Time")
    for b in data:
        sheet.cell(row=data.index(b) + 3, column=1, value=b["level"])
        sheet.cell(row=data.index(b) + 3, column=2, value=b["ex_bet"])
        sheet.cell(row=data.index(b) + 3, column=3, value=b["s_blind"])
        sheet.cell(row=data.index(b) + 3, column=4, value=b["b_blind"])
        sheet.cell(row=data.index(b) + 3, column=5, value=(b["gap_sec"] / 60))
    wb.save(filename=wbname)
    print("successful", wbname)


bet1 = [{"level": 1, "s_blind": 25, "b_blind": 50, "ex_bet": 0, "gap_sec": 300},
        {"level": 2, "s_blind": 50, "b_blind": 100, "ex_bet": 0, "gap_sec": 300},
        {"level": 3, "s_blind": 100, "b_blind": 200, "ex_bet": 50, "gap_sec": 300},
        {"level": 4, "s_blind": 150, "b_blind": 300, "ex_bet": 100, "gap_sec": 300},
        {"level": 5, "s_blind": 200, "b_blind": 400, "ex_bet": 150, "gap_sec": 300},
        {"level": 6, "s_blind": 300, "b_blind": 600, "ex_bet": 200, "gap_sec": 300},
        {"level": 7, "s_blind": 400, "b_blind": 800, "ex_bet": 300, "gap_sec": 300},
        {"level": 8, "s_blind": 500, "b_blind": 1000, "ex_bet": 300, "gap_sec": 300},
        {"level": 9, "s_blind": 700, "b_blind": 1400, "ex_bet": 400, "gap_sec": 300},
        {"level": 10, "s_blind": 1000, "b_blind": 2000, "ex_bet": 500, "gap_sec": 300},
        {"level": 11, "s_blind": 1500, "b_blind": 3000, "ex_bet": 750, "gap_sec": 300},
        {"level": 12, "s_blind": 2000, "b_blind": 4000, "ex_bet": 1000, "gap_sec": 300},
        {"level": 13, "s_blind": 2500, "b_blind": 5000, "ex_bet": 1500, "gap_sec": 300},
        {"level": 14, "s_blind": 3000, "b_blind": 6000, "ex_bet": 2000, "gap_sec": 300},
        {"level": 15, "s_blind": 3500, "b_blind": 7000, "ex_bet": 3000, "gap_sec": 300},
        {"level": 16, "s_blind": 4000, "b_blind": 8000, "ex_bet": 5000, "gap_sec": 300},
        {"level": 17, "s_blind": 5000, "b_blind": 10000, "ex_bet": 7500, "gap_sec": 300},
        {"level": 18, "s_blind": 6000, "b_blind": 12000, "ex_bet": 10000, "gap_sec": 300},
        {"level": 19, "s_blind": 70000, "b_blind": 140000, "ex_bet": 30000, "gap_sec": 300},
        {"level": 20, "s_blind": 8000, "b_blind": 16000, "ex_bet": 20000, "gap_sec": 300},
        {"level": 21, "s_blind": 10000, "b_blind": 20000, "ex_bet": 30000, "gap_sec": 300},
        {"level": 22, "s_blind": 15000, "b_blind": 30000, "ex_bet": 50000, "gap_sec": 300},
        {"level": 23, "s_blind": 20000, "b_blind": 40000, "ex_bet": 75000, "gap_sec": 300},
        {"level": 24, "s_blind": 30000, "b_blind": 60000, "ex_bet": 100000, "gap_sec": 300},
        {"level": 25, "s_blind": 50000, "b_blind": 100000, "ex_bet": 150000, "gap_sec": 300},
        {"level": 26, "s_blind": 70000, "b_blind": 140000, "ex_bet": 200000, "gap_sec": 300},
        {"level": 27, "s_blind": 100000, "b_blind": 200000, "ex_bet": 300000, "gap_sec": 300},
        {"level": 28, "s_blind": 150000, "b_blind": 300000, "ex_bet": 500000, "gap_sec": 300},
        {"level": 29, "s_blind": 200000, "b_blind": 400000, "ex_bet": 750000, "gap_sec": 300},
        {"level": 30, "s_blind": 300000, "b_blind": 600000, "ex_bet": 1000000, "gap_sec": 300}]

bet2 = [{"level": 1, "s_blind": 25, "b_blind": 50, "ex_bet": 0, "gap_sec": 300},
        {"level": 2, "s_blind": 50, "b_blind": 100, "ex_bet": 0, "gap_sec": 300},
        {"level": 3, "s_blind": 100, "b_blind": 200, "ex_bet": 50, "gap_sec": 300},
        {"level": 4, "s_blind": 150, "b_blind": 300, "ex_bet": 100, "gap_sec": 300},
        {"level": 5, "s_blind": 200, "b_blind": 400, "ex_bet": 150, "gap_sec": 300},
        {"level": 6, "s_blind": 300, "b_blind": 600, "ex_bet": 200, "gap_sec": 300},
        {"level": 7, "s_blind": 400, "b_blind": 800, "ex_bet": 300, "gap_sec": 300},
        {"level": 8, "s_blind": 500, "b_blind": 1000, "ex_bet": 300, "gap_sec": 300},
        {"level": 9, "s_blind": 700, "b_blind": 1400, "ex_bet": 400, "gap_sec": 300},
        {"level": 10, "s_blind": 1000, "b_blind": 2000, "ex_bet": 500, "gap_sec": 300},
        {"level": 11, "s_blind": 1500, "b_blind": 3000, "ex_bet": 750, "gap_sec": 300},
        {"level": 12, "s_blind": 2000, "b_blind": 4000, "ex_bet": 1000, "gap_sec": 300},
        {"level": 13, "s_blind": 2500, "b_blind": 5000, "ex_bet": 1500, "gap_sec": 300},
        {"level": 14, "s_blind": 3000, "b_blind": 6000, "ex_bet": 2000, "gap_sec": 300},
        {"level": 15, "s_blind": 3500, "b_blind": 7000, "ex_bet": 3000, "gap_sec": 300},
        {"level": 16, "s_blind": 4000, "b_blind": 8000, "ex_bet": 5000, "gap_sec": 300},
        {"level": 17, "s_blind": 5000, "b_blind": 10000, "ex_bet": 7500, "gap_sec": 300},
        {"level": 18, "s_blind": 6000, "b_blind": 12000, "ex_bet": 10000, "gap_sec": 300},
        {"level": 19, "s_blind": 70000, "b_blind": 140000, "ex_bet": 30000, "gap_sec": 300},
        {"level": 20, "s_blind": 8000, "b_blind": 16000, "ex_bet": 20000, "gap_sec": 300},
        {"level": 21, "s_blind": 10000, "b_blind": 20000, "ex_bet": 30000, "gap_sec": 300},
        {"level": 22, "s_blind": 15000, "b_blind": 30000, "ex_bet": 50000, "gap_sec": 300},
        {"level": 23, "s_blind": 20000, "b_blind": 40000, "ex_bet": 75000, "gap_sec": 300},
        {"level": 24, "s_blind": 30000, "b_blind": 60000, "ex_bet": 100000, "gap_sec": 300},
        {"level": 25, "s_blind": 50000, "b_blind": 100000, "ex_bet": 150000, "gap_sec": 300},
        {"level": 26, "s_blind": 70000, "b_blind": 140000, "ex_bet": 200000, "gap_sec": 300},
        {"level": 27, "s_blind": 100000, "b_blind": 200000, "ex_bet": 300000, "gap_sec": 300},
        {"level": 28, "s_blind": 150000, "b_blind": 300000, "ex_bet": 500000, "gap_sec": 300},
        {"level": 29, "s_blind": 200000, "b_blind": 400000, "ex_bet": 750000, "gap_sec": 300},
        {"level": 30, "s_blind": 300000, "b_blind": 600000, "ex_bet": 1000000, "gap_sec": 300}]

ftemp = config.get("data_path", "temp_path")
set_bet(bet1, ftemp + "bet1.xlsx")
set_bet(bet2, ftemp + "bet2.xlsx")
