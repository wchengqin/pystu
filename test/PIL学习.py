from PIL import Image, ImageDraw, ImageFont, ImageFilter
import random
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")


# 随机字母:
def rndChar():
    return chr(random.randint(65, 90))


# 随机颜色1:
def rndColor():
    return (random.randint(64, 255), random.randint(64, 255), random.randint(64, 255))


# 随机颜色2:
def rndColor2():
    return (random.randint(32, 127), random.randint(32, 127), random.randint(32, 127))


def gen_code(fname):
    # 240 x 60:
    width = 60 * 4
    height = 60
    image = Image.new('RGB', (width, height), (255, 255, 255))
    # 创建Font对象:
    font = ImageFont.truetype(
        r'C:\Windows\winsxs\amd64_microsoft-windows-font-truetype-arial_31bf3856ad364e35_6.1.7601.22154_none_d107b273dd38d599\arial.ttf',
        36)
    # 创建Draw对象:
    draw = ImageDraw.Draw(image)
    # 填充每个像素:
    for x in range(width):
        for y in range(height):
            draw.point((x, y), fill=rndColor())
    # 输出文字:
    for t in range(4):
        draw.text((60 * t + 10, 10), rndChar(), font=font, fill=rndColor2())
    # 模糊:
    image = image.filter(ImageFilter.BLUR)
    image.save(fname, 'jpeg')


def img_prc():
    ftemp = config.get("data_path", "temp_path")
    fname = config.get("data_path", "img_path") + "Penguins.jpg"
    img = Image.open(fname)
    img.resize((1024, 768))  # 调整图像的大小
    print(img.format, img.format_description, img.size)
    img = img.crop((100, 50, 920, 750))  # 裁剪图像(X轴，Y轴，长，宽)
    w, h = img.size  # 获得图像尺寸
    img.thumbnail((w // 2, h // 2))  # 缩放到50%
    img.filter(ImageFilter.GaussianBlur(100))  # 添加滤镜 高斯模糊
    img.rotate(50)  # 旋转和翻转图像
    img.transpose(Image.FLIP_TOP_BOTTOM)  # 上下对调
    img.show()
    img.save(ftemp + "thum.jpg", 'jpeg')  # 把缩放后的图像用jpeg格式保存
    # img.show()
    img.close()


if __name__ == "__main__":
    fname = config.get("data_path", "temp_path") + "vcode.jpg"
    gen_code(fname)
    img_prc()
