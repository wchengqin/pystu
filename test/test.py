# 同文件夹方法调用
# 不同文件夹方法调用
import sys

from lib_ext.TestMdl import *

sys.path.append(r'../../pysimplechain')
# simple_chain.manager()

# 模块调用

# print(cal_test())#直接调用模块中的方法
# print(MathTest.cfkj())#调用模块中类的静态方法
print(MathTest.chu(10, -5))
print(digui(995))  # 最大递归层数998，不同机器还不一样？
rst = list(map(MathTest.pow, [1, 2], [1, 3]))  # map方法测试
print(rst)
# rst = list(map(lambda x, y: x ** y, [1, 2], [1, 3]))
# print(rst)

rst = filter(lambda x: x % 2 == 0, [1, 2, 3, 4, 5, 6, 7, 8, 9, 0])  # 过滤偶数
print(list(rst))

print(get_private_var())

user1 = Student("wang", 80)
user2 = Student("zhang", 100)
user3 = Student("liu", 50)
room = Room([user1, user2, user3])
stus = room.getStudents()
print("stu_say_times=", Student.stu_say_times)
for s in stus:
    s.say("A")
    s.say("A", "1", c="CC", b="BB", name="D", port="E")
    print(s.get_grade())
print("stu_say_times=", Student.stu_say_times)

print(get_private_var())


class Test():
    def __init__(self):
        pass

    def a(self, s):
        print(s)

    def _a(self, s):
        print(s)

    def __a(self, s):
        print(s)


test = Test()
test.a('a')
test._a('_a')
test._Test__a('__a')
test.__a('__a')
