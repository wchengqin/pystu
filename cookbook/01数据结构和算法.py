# 第一章:数据结构和算法
# 1.1 将序列分解为单独的变量
# 任何的序列（或者是可迭代对象）可以通过一个简单的赋值操作来分解为单独的变量。 唯一的要求就是变量的总数和结构必须与序列相吻合。
data = ['ACME', 50, 91.1, (2012, 12, 21)]
name, shares, price, date = data
print(date)
name, shares, price, (year, mon, day) = data
print(year)
# 使用点位符（临时无意义的变量），解压部分数据
_, shares, price, _ = data
print(price)

# 1.2 解压可迭代对象赋值给多个变量
# 星号表达式unpack可迭代的变量
record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
name, email, *phone_numbers = record
print(phone_numbers)  # 列表类型
print(*phone_numbers)

# 1.3 保留最后 N 个元素 Python对列
from collections import deque

data = deque(maxlen=4)
data.append(1)
data.append(2)
data.appendleft(3)
print(data)
data.pop()
print(data)
data.popleft()
print(data)

# 1.4 查找最大或最小的 N 个元素  #1.19 转换并同时计算数据
# 堆数据结构最重要的特征是 heap[0] 永远是最小的元素。并且剩余的元素可以很容易的通过调用 heapq.heappop() 方法得到， 该方法会先将第一个元素弹出来，然后用下一个最小的元素来取代被弹出元素（这种操作时间复杂度仅仅是 O(log N)，N 是堆大小）。
import heapq

nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
print(heapq.nlargest(3, nums))
print(heapq.nsmallest(3, nums))
data = sum(x * x for x in nums)
print(data)

portfolio = [
    {'name': 'IBM', 'shares': 100, 'price': 91.1},
    {'name': 'AAPL', 'shares': 50, 'price': 543.22},
    {'name': 'FB', 'shares': 200, 'price': 21.09},
    {'name': 'HPQ', 'shares': 35, 'price': 31.75},
    {'name': 'YHOO', 'shares': 45, 'price': 16.35},
    {'name': 'ACME', 'shares': 75, 'price': 115.65}
]
cheap = heapq.nsmallest(3, portfolio, key=lambda s: s['price'])
expensive = heapq.nlargest(3, portfolio, key=lambda s: s['price'])
print(cheap)
print(expensive)

heap = list(nums)
heapq.heapify(heap)
print(heap)
print(heapq.heappop(heap))

# 1.5 实现一个优先级队列
# 队列包含了一个 (-priority, index, item) 的元组。 优先级为负数的目的是使得元素按照优先级从高到低排序。
# index 变量的作用是保证同等优先级元素的正确排序。 通过保存一个不断增加的 index 下标变量，可以确保元素按照它们插入的顺序排序。 而且， index 变量也在相同优先级元素比较的时候起到重要作用。
import heapq


class PriorityQueue:
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]

    def queue(self):
        return self._queue


data = PriorityQueue()
one = 'foo', 1
data.push(*one)
two = 'bar', 10
data.push(*two)
data.push('spam', 4)
data.push('grok', 1)
print(data)
print(data.queue())
print(data.pop())
print(data.pop())
print(one < two)
# 元组比较大小，不理解？？？
one = (1, 0, 'foo')
two = (5, 1, 'bar')
three = (1, 2, 'grok')
print(one > two, two > three)

# 1.6 字典中的键映射多个值
# defaultdict 的一个特征是它会自动初始化每个 key 刚开始对应的值，所以你只需要关注添加元素操作了。
from collections import defaultdict

# data = defaultdict(set)
data = defaultdict(list)
data['one'].append(1)
data['one'].append(2)
data['two'].append('a')
data['two'].append('b')
data['two'].append('b')
print(data)
for key, value, *_ in data.items():
    print(key, value, _)

# 1.7 字典排序   1.8 字典的运算   1.9 查找两字典的相同点  1.17 从字典中提取子集   1.20 合并多个字典或映射
# OrderedDict在迭代操作的时候它会保持元素被插入时的顺序
from collections import OrderedDict

one, two, three = 10, 20, 15
data1 = dict()
data2 = OrderedDict()
data1['a'] = one
data1['c'] = three
data1['b'] = two
data2['a'] = one
data2['c'] = three
data2['b'] = two
data3 = zip(data1)
print(data3)
print(data1, data2)
for key, value in data1.items():
    print(key, value)
for key, value in data2.items():
    print(key, value)

import json

# 没区别 ？？？
print(json.dumps(data1))
print(json.dumps(data2))
# zip() 函数将键和值反转
_min = min(zip(data1.values(), data1.keys()))
_max = max(zip(data1.values(), data1.keys()))
print(_min, _max)
# sorted() 函数排列字典数据
_sorted = sorted(zip(data1.values(), data1.keys()))
print(_sorted)

print(max(data1), max(data1.values()), max(data1, key=lambda k: data1[k]))
print(max(zip(data1.values(), data1.keys())))

data1['d'] = 25
data2['e'] = 30
print(data1.keys() & data2.keys())
print(data1.keys() - data2.keys())
print(data2.keys() - data1.keys())
print(data1.items() & data2.items())
# 可以使用大括号 { } 或者 set() 函数创建集合，注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典
temp1 = {'a', 'b', 'c'}
temp2 = {'a', 'b'}
print(temp1, type(temp1), temp1 - temp2)

# 过滤字典元素
data = {key: data1[key] for key in (data1.keys() - {'a', 'b', 'c'})}
print(data)
data3 = {key: value for key, value in data1.items() if value >= 20}
print(data3)
data4 = {key: value for key, value in data1.items() if key in ['a', 'c']}
print(data4)

from collections import ChainMap

data1['a'] = 'abc'
data5 = ChainMap(data1, data2)  # ChainMap 使用原来的字典，它自己不创建新的字典。
print('-----------------------')
print(data5, data5.new_child(), data5.parents, data5['a'])  # 如果出现重复键，那么第一次出现的映射值会被返回
data3.update(data4)  # 更新字典
print(data3)


# 1.10 删除序列相同元素并保持顺序


def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


data = dedupe([1, 5, 2, 1, 9, 1, 5, 10])
print(data, list(data))

# 1.11 命名切片
record = '....................100 .......513.25 ..........'
SHARES = slice(20, 23)
PRICE = slice(31, 37)
cost = int(record[SHARES]) * float(record[PRICE])
print(cost)
print(SHARES, SHARES.start, SHARES.stop, SHARES.step)

data = SHARES.indices(len(record))
print(data)

# 1.12 序列中出现次数最多的元素
words = [
    'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
    'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
    'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
    'my', 'eyes', "you're", 'under'
]
from collections import Counter

word_counts = Counter(words)
top_three = word_counts.most_common(3)  # 出现频率最高的3个单词
print(top_three)
print(word_counts['eyes'])
word_counts['eyes'] += 1  # 手动增加计数
print(word_counts['eyes'])
word_counts.update(['eyes', 'eyes'])
print(word_counts['eyes'])

# 1.13 通过某个关键字排序一个字典列表  1.14 排序不支持原生比较的对象  1.15 通过某个字段将记录分组  1.16 过滤序列元素
users = [
    {'fname': 'Brian', 'lname': 'Jones', 'birthday': '07/01/2012', 'uid': 1003},
    {'fname': 'David', 'lname': 'Beazley', 'birthday': '09/01/2012', 'uid': 1002},
    {'fname': 'John', 'lname': 'Cleese', 'birthday': '08/01/2012', 'uid': 1001},
    {'fname': 'Big', 'lname': 'Jones', 'birthday': '07/01/2012', 'uid': 1004}
]


class User:
    def __init__(self, uid, fname, lname, birthday):
        self.uid = uid
        self.fname = fname
        self.lname = lname
        self.birthday = birthday

    def __repr__(self):
        return 'User({})'.format(self.uid)


from operator import itemgetter

users_by_uid = sorted(users, key=itemgetter('uid'))
print(users_by_uid)
users_by_fname = sorted(users, key=itemgetter('fname'))
print(users_by_fname)
users_by_lfname = sorted(users, key=itemgetter('lname', 'fname'))
print(users_by_lfname)

user_list = []
for u in users:
    user_list.append(User(**u))

print(user_list, sorted(user_list, key=lambda u: u.uid))

from operator import itemgetter
from itertools import groupby

users.sort(key=itemgetter('birthday'))
# Iterate in groups
for date, items in groupby(users, key=itemgetter('birthday')):
    print(date)
    for i in items:
        print(' ', i)

data = [u for u in users if u['uid'] > 1002]
print(data)


def uid_filter(u):
    return True if u['uid'] > 1002 else False


data = list(filter(uid_filter, users))
print(data)

from itertools import compress

uids = [1001, 1002, 1003, 1004]
_filter = [n > 1002 for n in uids]
print(_filter)
data = list(compress(users, _filter))
print(users)
print(data)

# 1.18 映射名称到序列元素
"""
命名元组的一个主要用途是将你的代码从下标操作中解脱出来。 因此，如果你从数据库调用中返回了一个很大的元组列表，通过下标去操作其中的元素， 当你在表中添加了新的列的时候你的代码可能就会出错了。
命名元组另一个用途就是作为字典的替代，因为字典存储需要更多的内存空间。 如果你需要构建一个非常大的包含字典的数据结构，那么使用命名元组会更加高效。 但是需要注意的是，不像字典那样，一个命名元组是不可更改的。
"""

from collections import namedtuple

Subscriber = namedtuple('Subscriber', ['addr', 'date'])
sub = Subscriber('jonesy@example.com', '2012-10-19')
print(sub)
sub = Subscriber(addr='jonesy@example.com', date='2012-10-19')
print(sub, sub.addr, sub.date)
sub = sub._replace(addr='abc')  # 不能直接修改值，需要用_replace方法
print(sub)
