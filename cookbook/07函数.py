# 第七章：函数
#
# 7.1 可接受任意数量参数的函数
def avg(first, *args):  # *参数代表所有其他位置参数组成的元组。
    return (first + sum(args)) / (1 + len(args))


print(avg(1, 2, 3, 4))

import html


def make_element(name, value, **attrs):  # **参数代表任意数量的关键字参数
    keyvals = [' %s="%s"' % item for item in attrs.items()]
    attr_str = ''.join(keyvals)
    element = '<{name}{attrs}>{value}</{name}>'.format(
        name=name,
        attrs=attr_str,
        value=html.escape(value))
    return element


print(make_element('item', 'Albatross', size='large', quantity=6))
print(make_element('item', 'Albatross', **{'size': 'large', 'quantity': 6}))


# 一个*参数只能出现在函数定义中最后一个位置参数后面，而 **参数只能出现在最后一个参数。 有一点要注意的是，在*参数后面仍然可以定义其他参数。

def anyargs(id, *args, **kwargs):
    print(type(args), args)  # A tuple
    print(type(kwargs), kwargs)  # A dict


anyargs(1, 'item', 'Albatross', size='large', quantity=6)


# 7.2 只接受关键字参数的函数
# 强制关键字参数：放到某个*参数或者单个*后面的关键字参数
def recv(maxsize, *, block):
    'Receives a message'
    pass


# recv(1024, True) # TypeError
recv(1024, block=True)  # Ok


# 7.3 给函数参数增加元信息
# python解释器不会对这些注解添加任何的语义。它们不会被类型检查，运行时跟没有加注解之前的效果也没有任何差距。
def add(x: int, y: int) -> int:
    return x + y


print(help(add))


# 7.4 返回多个值的函数
def myfun():
    return 1, 2, 3  # 元组


data = myfun()
print(type(data), data)


# 7.5 定义有默认参数的函数
def spam(a, b=42):
    print(a, b)
    return a + b


print(spam(1))  # Ok. a=1, b=42
print(spam(1, 2))  # Ok. a=1, b=2
# 7.6 定义匿名或内联函数
'''
lambda只能指定单个表达式，它的值就是最后的返回值。
也就是说不能包含其他的语言特性了， 包括多个语句、条件表达式、迭代以及异常处理等等。
'''
add = lambda x, y: x + y
print(add(2, 3))

names = ['David Beazley', 'Raymond Hettinger', 'Ned Batchelder', 'Brian Jones']
names = sorted(names, key=lambda name: name.split()[0].lower())
print(names)

# 7.7 匿名函数捕获变量值
# lambda表达式中的x是一个自由变量， 在运行时绑定值，而不是定义时就绑定，这跟函数的默认值参数定义是不同的。 因此，在调用这个lambda表达式的时候，x的值是执行时的值。
x = 10
a = lambda y: x + y
print(a(10))
x = 20
b = lambda y: x + y
print(a(10), b(10))

x = 10
a = lambda y, x=x: x + y
x = 20
b = lambda y, x=x: x + y
print(a(10), b(10))

funcs = [lambda x: x + n for n in range(5)]
for f in funcs:
    print(f(0))

funcs = [lambda x, n=n: x + n for n in range(5)]
for f in funcs:
    print(f(0))
# 7.8 减少可调用对象的参数个数
# partial() 函数允许你给一个或多个参数设置固定的值，减少接下来被调用时的参数个数。
from functools import partial


def spam(a, b, c, d):
    return a, b, c, d


s1 = partial(spam, 1)  # a = 1
print(*s1(2, 3, 4))
s2 = partial(spam, d=4)  # d = 4
print(*s2(1, 2, 3))
s3 = partial(spam, 1, 2, d=4)  # a = 1, b = 2, d = 4
print(*s3(3))
s4 = partial(spam, 1, c=3)  # 错误，不允许减少中间参数？
# print(*s4(2, 4))
s5 = partial(spam, 1, d=4)  # 正确
print(*s5(2, 3))

import math


def distance(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return math.hypot(x2 - x1, y2 - y1)


points = [(1, 2), (3, 4), (5, 6), (7, 8)]
points.sort(key=partial(distance, (4, 3)))
print(points)
# 7.9 将单方法的类转换为函数
from urllib.request import urlopen


class UrlTemplate:
    def __init__(self, template):
        self.template = template

    def open(self, **kwargs):
        url = self.template.format_map(kwargs)
        print(url)
        return urlopen(url)


url_temp = 'https://www.baidu.com/s?wd={kw}&rqlang={lang}'
yahoo = UrlTemplate(url_temp)
for line in yahoo.open(kw='python', lang='cn'):
    print(line.decode('utf-8'))


def urltemplate(template):
    def opener(**kwargs):
        return urlopen(template.format_map(kwargs))

    return opener


handler = urltemplate(url_temp)
for line in handler(kw='python', lang='cn'):
    print(line.decode('utf-8'))


# 7.10 带额外状态信息的回调函数

def make_handler():
    sequence = 0
    while True:
        result = yield
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))
        if 'quit' == result:
            break


handler = make_handler()
next(handler)
next(handler)
handler.send('good')
try:
    handler.send('quit')
    handler.send('over')
except StopIteration as e:
    pass


def apply_async(func, args, *, callback):
    # Compute the result
    result = func(*args)

    # Invoke the callback with the result
    callback(result)


def add(x, y):
    return x + y


handler = make_handler()
next(handler)  # Advance to the yield
apply_async(add, (2, 3), callback=handler.send)
apply_async(add, ('hello', 'world'), callback=handler.send)
# apply_async(add, ('quit', ''), callback=handler.send)

# 7.11 内联回调函数

from queue import Queue
from functools import wraps


class Async:
    def __init__(self, func, args):
        self.func = func
        self.args = args


def inlined_async(func):
    @wraps(func)
    def wrapper(*args):
        f = func(*args)
        result_queue = Queue()
        result_queue.put(None)
        while True:
            result = result_queue.get()
            try:
                a = f.send(result)
                apply_async(a.func, a.args, callback=result_queue.put)
            except StopIteration:
                break

    return wrapper


@inlined_async
def test():
    r = yield Async(add, (2, 3))
    print(r)
    r = yield Async(add, ('hello', 'world'))
    print(r)
    for n in range(10):
        r = yield Async(add, (n, n))
        print(r)
    print('Goodbye')


test()


# 7.12 访问闭包中定义的变量
def sample():
    n = 0

    # Closure function
    def func():
        print('n=', n)

    # Accessor methods for n
    def get_n():
        return n

    def set_n(value):
        nonlocal n
        n = value

    # Attach as function attributes
    print(func)
    func.get_n = get_n
    func.set_n = set_n
    return func


f = sample()
print(f)
f()
f.set_n(10)
f()
print(f.get_n())
