# 第八章：类与对象
#
# 8.1 改变对象的字符串显示
'''
__repr__() 方法返回一个实例的代码表示形式，通常用来重新构造这个实例。 内置的 repr() 函数返回这个字符串，跟我们使用交互式解释器显示的值是一样的。
__str__() 方法将实例转换为一个字符串，使用 str() 或 print() 函数会输出这个字符串。
'''


class Pair:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Pair({0.x!r}, {0.y!r})'.format(self)

    def __str__(self):
        return '({0.x!s}, {0.y!s})'.format(self)


data = Pair(2, 3)
print(data)
# !r 格式化代码指明输出使用 __repr__() 来代替默认的 __str__()
print('Pair is {0!r}'.format(data))
# 8.2 自定义字符串的格式化
# __format__() 方法自定义字符串的格式化
_formats = {
    'ymd': '{d.year}-{d.month}-{d.day}',
    'mdy': '{d.month}/{d.day}/{d.year}',
    'dmy': '{d.day}/{d.month}/{d.year}'
}


class Date:
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day

    def __format__(self, code):
        if code == '':
            code = 'ymd'
        fmt = _formats[code]
        return fmt.format(d=self)


d = Date(2012, 12, 21)
print(d, format(d))
print('The date is {:ymd}'.format(d), 'The date is {:mdy}'.format(d))

from datetime import date

d = date(2012, 12, 21)
print(format(d))
print(format(d, '%A, %B %d, %Y'))
print('The end is {:%d %b %Y}. Goodbye'.format(d))
# 8.3 让对象支持上下文管理协议
# 为了让一个对象兼容 with 语句，你需要实现 __enter__() 和 __exit__() 方法。
from queue import Queue


class LazyPool:

    def __init__(self, size=10):
        self.size = size
        self.pool = None

    def __enter__(self):
        if self.pool is not None:
            raise RuntimeError('Already Running')
        self.pool = Queue(maxsize=self.size)
        return self.pool

    def __exit__(self, exc_ty, exc_val, tb):
        print(self.pool.qsize())
        self.pool.empty()
        self.pool = None


pool = LazyPool(5)
with pool as p:
    p.put(1)
# 8.4 创建大量对象时节省内存方法
'''
定义 __slots__ 后，Python就会为实例使用一种更加紧凑的内部表示。 实例通过一个很小的固定大小的数组来构建，而不是为每个实例定义一个字典，这跟元组或列表很类似。 
在 __slots__ 中列出的属性名在内部被映射到这个数组的指定小标上。 使用slots一个不好的地方就是我们不能再给实例添加新的属性了，只能使用在 __slots__ 中定义的那些属性名。
'''


class Date:
    __slots__ = ['year', 'month', 'day']

    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day


# 8.5 在类中封装属性名
'''
Python程序员不去依赖语言特性去封装数据，而是通过遵循一定的属性和方法命名规约来达到这个效果。 
第一个约定是任何以单下划线_开头的名字都应该是内部实现。
使用双下划线开始会导致访问名称变成其他形式，这种属性通过继承是无法被覆盖的。
'''


class A:
    def __init__(self):
        self._internal = 0  # An internal attribute
        self.public = 1  # A public attribute
        self.__private = 2  # A private attribute

    def public_method(self):
        return self.public

    def _internal_method(self):
        return self._internal

    def __private_method(self):
        return self.__private


data = A()
print(data._internal, data._internal_method(), data.public, data.public_method())
print(data._A__private, data._A__private_method())  # 私有名称 __private 和 __private_method 被重命名为 _类名__private_method，子类不能覆盖


# 8.6 创建可管理的属性
class Person:
    def __init__(self, first_name):
        self._first_name = first_name

    # Getter function
    @property
    def first_name(self):
        return self._first_name

    # Setter function
    @first_name.setter
    def first_name(self, value):
        if not isinstance(value, str):
            raise TypeError('Expected a string')
        self._first_name = value

    # Deleter function (optional)
    @first_name.deleter
    def first_name(self):
        raise AttributeError("Can't delete attribute")


data = Person('wang')
print(data.first_name)
data.first_name = 'abc'
print(data.first_name)

# 8.7 调用父类方法  8.8 子类中扩展property  8.9 创建新的类或实例属性  8.10 使用延迟计算属性
'''
一个描述器就是一个实现了三个核心的属性访问操作(get, set, delete)的类， 分别为 __get__() 、__set__() 和 __delete__() 这三个特殊的方法。 
这些方法接受一个实例作为输入，之后相应的操作实例底层的字典。
'''
import re


class Color:
    def __init__(self, name, r, g, b):
        self.name = name
        self.r = r
        self.g = g
        self.b = b

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if value is None or isinstance(value, Color):
            instance.__dict__[self.name] = value
        elif isinstance(value, tuple) and len(value) == 3:
            instance.__dict__[self.name] = Color(self.name, *value)
        elif isinstance(value, str) and re.match(r'#\d{6}', value) is not None:
            rgb = re.findall(r'\d{2}', value[1:])
            instance.__dict__[self.name] = Color(self.name, *list(map(int, rgb)))
        else:
            raise TypeError('error color %s ' % value)

    def __delete__(self, instance):
        del instance.__dict__[self.name]

    def __str__(self):
        s = '#{:0>2d}{:0>2d}{:0>2d}'.format(self.r, self.g, self.b)
        return s


c = Color('color', 1, 2, 3)
print(c)


class lazyproperty:
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            value = self.func(instance)
            setattr(instance, self.func.__name__, value)
            return value


class Animal:
    color = Color('color', 0, 0, 0)

    def __init__(self, type, name, leg, weight, color):
        self.type = type
        self.weight = weight
        self.leg = leg
        self.color = color
        self._name = name

    def say(self, text):
        print('%s say % s' % (self.type, text))

    def run(self, speed):
        print('%s run %d m/s' % (self.type, speed))

    # Getter function
    @property
    def name(self):
        return self._name

    # Setter function
    @name.setter
    def name(self, value):
        if not isinstance(value, str):
            raise TypeError('Expected a string')
        self._name = value

    # Deleter function
    @name.deleter
    def name(self):
        raise AttributeError("Can't delete attribute")


class Dog(Animal):  # 继承
    def __init__(self, name, weight, color):
        super().__init__('dog', name, 4, weight, color)

    def say(self, text):  # 重写
        print('i am %s' % self.name)
        super().say(text)  # 调用父类

    @Animal.name.getter
    def name(self):
        return super().name.title()

    @lazyproperty
    def fat(self):
        print('the dog too fat ?')
        return self.weight > 10


d = Dog('peter', 10, c)
print(d.type, d.name, d.leg, d.weight, d.color)
d.say('wang-wang。。。。')
d.run(7)
d.color = 0, 1, 23
print(d.color)
print(d.fat, d.fat, d.fat)


# 8.11 简化数据结构的初始化
class Base:
    _fields = []

    def __init__(self, *args, **kwargs):
        if len(args) > len(self._fields):
            raise TypeError('Expected {} arguments'.format(len(self._fields)))

        # Set all of the positional arguments
        for name, value in zip(self._fields, args):
            setattr(self, name, value)

        # Set the remaining keyword arguments
        for name in self._fields[len(args):]:
            setattr(self, name, kwargs.pop(name))

        # Check for any remaining unknown arguments
        if kwargs:
            raise TypeError('Invalid argument(s): {}'.format(','.join(kwargs)))

    def __str__(self):
        print(self.__dict__)
        return super().__str__()


class Stock(Base):
    _fields = ['name', 'shares', 'price']


print(Stock('ACME', 50, 91.1), '\r', Stock('ACME', 50, price=91.1), '\r', Stock('ACME', shares=50, price=91.1))
# 8.12 定义接口或者抽象基类
'''
@abstractmethod 还能注解静态方法、类方法和 properties 
'''
from abc import ABCMeta, abstractmethod


class MBDrive(metaclass=ABCMeta):
    def __init__(self, port, baudrate=9600, bytesize=8, parity='N', stopbits=1, timeout=5, verbose=True):
        self.port = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout
        self.verbose = verbose

    @abstractmethod
    def read(self, function_code, starting_address, quantity_of_x):
        pass

    @abstractmethod
    def write(self, function_code, starting_address, output_value):
        pass


# 温度计
class Temperature(MBDrive):
    def read(self, function_code, starting_address, quantity_of_x):
        print(self.baudrate)
        print(
            'function_code=%d starting_address=%d quantity_of_x=%d ' % (function_code, starting_address, quantity_of_x))

    def write(self, function_code, starting_address, output_value):
        print('function_code=%d starting_address=%d output_value=%d ' % (function_code, starting_address, output_value))


port = 'com1'
drive = Temperature(port)
drive.read(1, 2, 3)
print(isinstance(drive, MBDrive))


# 8.13 实现数据模型的类型约束
# Base class. Uses a descriptor to set a value
class Descriptor:
    def __init__(self, name=None, **opts):
        self.name = name
        for key, value in opts.items():
            setattr(self, key, value)

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value


# Decorator for applying type checking
def Typed(expected_type, cls=None):
    if cls is None:
        return lambda cls: Typed(expected_type, cls)
    super_set = cls.__set__

    def __set__(self, instance, value):
        if not isinstance(value, expected_type):
            raise TypeError('expected ' + str(expected_type))
        super_set(self, instance, value)

    cls.__set__ = __set__
    return cls


# Decorator for unsigned values
def Unsigned(cls):
    super_set = cls.__set__

    def __set__(self, instance, value):
        if value < 0:
            raise ValueError('Expected >= 0')
        super_set(self, instance, value)

    cls.__set__ = __set__
    return cls


# Decorator for allowing sized values
def MaxSized(cls):
    super_init = cls.__init__

    def __init__(self, name=None, **opts):
        if 'size' not in opts:
            raise TypeError('missing size option')
        super_init(self, name, **opts)

    cls.__init__ = __init__

    super_set = cls.__set__

    def __set__(self, instance, value):
        if len(value) > self.size:
            raise ValueError('size must be < ' + str(self.size))
        super_set(self, instance, value)

    cls.__set__ = __set__
    return cls


# Specialized descriptors
@Typed(int)
class Integer(Descriptor):
    pass


@Unsigned
class UnsignedInteger(Integer):
    pass


@Typed(float)
class Float(Descriptor):
    pass


@Unsigned
class UnsignedFloat(Float):
    pass


@Typed(str)
class String(Descriptor):
    pass


@MaxSized
class SizedString(String):
    pass


class Stock:
    # Specify constraints
    name = SizedString('name', size=8)
    shares = UnsignedInteger('shares')
    price = UnsignedFloat('price')

    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price

    def __str__(self):
        print(self.__dict__)
        return super().__str__()


data = Stock('12345678', 1, 2.1)
print(data)
# 8.14 实现自定义容器
from collections.abc import Iterable, Sequence
import bisect


class A(Iterable):
    def __init__(self):
        self._children = []

    @property
    def children(self):
        return self._children

    def __iter__(self):
        return iter(self._children)


print(A().children)


class SortedItems(Sequence):
    def __init__(self, initial=None):
        self._items = sorted(initial) if initial is not None else []

    # Required sequence methods
    def __getitem__(self, index):
        return self._items[index]

    def __len__(self):
        return len(self._items)

    # Method for adding an item in the right location
    def add(self, item):
        bisect.insort(self._items, item)


items = SortedItems([5, 1, 3])
print(list(items))
print(items[0], items[-1])
items.add(2)
print(list(items))


# 8.15 属性的代理访问
class B:
    def spam(self, x):
        print(x)

    def foo(self, x):
        print(x)


class B1:
    """简单的代理"""

    def __init__(self, target):
        if not isinstance(target, B):
            raise TypeError('Expected a B')
        self._b = target

    def spam(self, x):
        # Delegate to the internal self._a instance
        return self._b.spam(x)

    def foo(self, x):
        # Delegate to the internal self._a instance
        return self._b.foo(x)

    def bar(self):
        pass


data = B1(B())
data.spam(123)


class B2:
    """使用__getattr__的代理，代理方法比较多时候"""

    def __init__(self, target):
        if not isinstance(target, B):
            raise TypeError('Expected a B')
        self._b = target

    def bar(self):
        pass

    # Expose all of the methods defined on class A
    def __getattr__(self, name):
        """这个方法在访问的attribute不存在的时候被调用
        the __getattr__() method is actually a fallback method
        that only gets called when an attribute is not found"""
        return getattr(self._b, name)


data = B2(B())
data.spam(123)


# 代理模式
# A proxy class that wraps around another object, but exposes its public attributes
class Proxy:
    def __init__(self, obj):
        self._obj = obj

    # Delegate attribute lookup to internal obj
    def __getattr__(self, name):
        print('getattr:', name)
        return getattr(self._obj, name)

    # Delegate attribute assignment
    def __setattr__(self, name, value):
        if name.startswith('_'):
            super().__setattr__(name, value)
        else:
            print('setattr:', name, value)
            setattr(self._obj, name, value)

    # Delegate attribute deletion
    def __delattr__(self, name):
        if name.startswith('_'):
            super().__delattr__(name)
        else:
            print('delattr:', name)
            delattr(self._obj, name)


class B3:
    def __init__(self, x):
        self.x = x

    def bar(self, y):
        print('Spam.bar:', self.x, y)


data = Proxy(B3(3))
data.bar(4)

# 8.16 在类中定义多个构造器  8.17 创建不调用init方法的实例

'''
类方法的一个主要用途就是定义多个构造器。它接受一个 class 作为第一个参数(cls)。 你应该注意到了这个类被用来创建并返回最终的实例。
'''
import time


class Date:
    """方法一：使用类方法"""

    # Primary constructor
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day

    # Alternate constructor
    @classmethod
    def today(cls):
        t = time.localtime()
        return cls(t.tm_year, t.tm_mon, t.tm_mday)

    def __str__(self):
        print(self.__dict__)
        return super().__str__()


print(Date(2012, 12, 21), Date.today())

data = Date.__new__(Date)  # __new__() 方法创建一个未初始化的实例
data = {'year': 2012, 'month': 8, 'day': 29}
for key, value in data.items():
    setattr(d, key, value)
print(data)
# 8.18 利用Mixins扩展类功能
'''

混入类不能直接被实例化使用。 
混入类没有自己的状态信息，也就是说它们并没有定义 __init__() 方法，并且没有实例属性。 
这也是为什么我们在上面明确定义了 __slots__ = () 。
'''


class Mixins1:
    __slots__ = ()  # 混入类都没有实例变量，因为直接实例化混入类没有任何意义

    def say1(self, text):
        print('say1', text)


class Mixins2:
    __slots__ = ()  # 混入类都没有实例变量，因为直接实例化混入类没有任何意义

    def say2(self, text):
        print('say2', text)


# 多重继承
class Mixins3(list, Mixins1, Mixins2):
    pass


data = Mixins3()
data.say1('a')
data.say2('a')
data.append(1)
print(data, isinstance(data, list), isinstance(data, Mixins1), isinstance(data, Mixins2))


# 8.19 实现状态对象或者状态机
class Connection:
    """对每个状态定义一个类"""

    def __init__(self):
        self.new_state(ClosedConnectionState)

    def new_state(self, newstate):
        self._state = newstate
        # Delegate to the state class

    def read(self):
        return self._state.read(self)

    def write(self, data):
        return self._state.write(self, data)

    def open(self):
        return self._state.open(self)

    def close(self):
        return self._state.close(self)


# Connection state base class
class ConnectionState:
    @staticmethod
    def read(conn):
        raise NotImplementedError()

    @staticmethod
    def write(conn, data):
        raise NotImplementedError()

    @staticmethod
    def open(conn):
        raise NotImplementedError()

    @staticmethod
    def close(conn):
        raise NotImplementedError()


# Implementation of different states
class ClosedConnectionState(ConnectionState):
    @staticmethod
    def read(conn):
        raise RuntimeError('Not open')

    @staticmethod
    def write(conn, data):
        raise RuntimeError('Not open')

    @staticmethod
    def open(conn):
        conn.new_state(OpenConnectionState)

    @staticmethod
    def close(conn):
        raise RuntimeError('Already closed')


class OpenConnectionState(ConnectionState):
    @staticmethod
    def read(conn):
        print('reading')

    @staticmethod
    def write(conn, data):
        print('writing')

    @staticmethod
    def open(conn):
        raise RuntimeError('Already open')

    @staticmethod
    def close(conn):
        conn.new_state(ClosedConnectionState)


c = Connection()
print(c._state)
try:
    c.read()
except Exception as e:
    print(e)
c.open()
print(c._state)
c.read()
c.write('hello')
c.close()
print(c._state)

# 8.20 通过字符串调用对象方法
'''
反射
通过字符串形式的方法名称调用某个对象的对应方法。
'''
import math, operator


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Point({!r:},{!r:})'.format(self.x, self.y)

    def distance(self, x, y):
        return math.hypot(self.x - x, self.y - y)


p = Point(2, 3)
data = getattr(p, 'distance')(0, 0)  # Calls p.distance(0, 0)
print(data)
data = operator.methodcaller('distance', 0, 0)(p)
print(data)


# 8.21 实现访问者模式  8.22 不用递归实现访问者模式
class Node:
    pass


class UnaryOperator(Node):
    def __init__(self, operand):
        self.operand = operand


class BinaryOperator(Node):
    def __init__(self, left, right):
        self.left = left
        self.right = right


class Add(BinaryOperator):
    pass


class Sub(BinaryOperator):
    pass


class Mul(BinaryOperator):
    pass


class Div(BinaryOperator):
    pass


class Negate(UnaryOperator):
    pass


class Number(Node):
    def __init__(self, value):
        self.value = value


class NodeVisitor:
    def visit(self, node):
        methname = 'visit_' + type(node).__name__
        meth = getattr(self, methname, None)
        if meth is None:
            meth = self.generic_visit
        return meth(node)

    def generic_visit(self, node):
        raise RuntimeError('No {} method'.format('visit_' + type(node).__name__))


class Evaluator(NodeVisitor):
    def visit_Number(self, node):
        return node.value

    def visit_Add(self, node):
        return self.visit(node.left) + self.visit(node.right)

    def visit_Sub(self, node):
        return self.visit(node.left) - self.visit(node.right)

    def visit_Mul(self, node):
        return self.visit(node.left) * self.visit(node.right)

    def visit_Div(self, node):
        return self.visit(node.left) / self.visit(node.right)

    def visit_Negate(self, node):
        return -node.operand


t1 = Sub(Number(3), Number(4))
t2 = Mul(Number(2), t1)
t3 = Div(t2, Number(5))
t4 = Add(Number(1), t3)
e = Evaluator()
print(e.visit(t4))
#
# 8.23 循环引用数据结构的内存管理
# 8.24 让类支持比较操作
'''
饰器 functools.total_ordering 就是用来简化对象比较处理的。 
使用它来装饰一个类，你只需定义一个 __eq__() 方法， 外加其他方法(__lt__, __le__, __gt__, or __ge__)中的一个即可。 然后装饰器会自动为你填充其它比较方法。
'''
from functools import total_ordering


class Room:
    def __init__(self, name, length, width):
        self.name = name
        self.length = length
        self.width = width
        self.square_feet = self.length * self.width


@total_ordering
class House:
    def __init__(self, name, style):
        self.name = name
        self.style = style
        self.rooms = list()

    @property
    def living_space_footage(self):
        return sum(r.square_feet for r in self.rooms)

    def add_room(self, room):
        self.rooms.append(room)

    def __str__(self):
        return '{}: {} square foot {}'.format(self.name,
                                              self.living_space_footage,
                                              self.style)

    def __eq__(self, other):
        return self.living_space_footage == other.living_space_footage

    def __lt__(self, other):
        return self.living_space_footage < other.living_space_footage


# Build a few houses, and add rooms to them
h1 = House('h1', 'Cape')
h1.add_room(Room('Master Bedroom', 14, 21))
h1.add_room(Room('Living Room', 18, 20))
h1.add_room(Room('Kitchen', 12, 16))
h1.add_room(Room('Office', 12, 12))
h2 = House('h2', 'Ranch')
h2.add_room(Room('Master Bedroom', 14, 21))
h2.add_room(Room('Living Room', 18, 20))
h2.add_room(Room('Kitchen', 12, 16))
h3 = House('h3', 'Split')
h3.add_room(Room('Master Bedroom', 14, 21))
h3.add_room(Room('Living Room', 18, 20))
h3.add_room(Room('Office', 12, 16))
h3.add_room(Room('Kitchen', 15, 17))
houses = [h1, h2, h3]
print('Is h1 bigger than h2?', h1 > h2)  # prints True
print('Is h2 smaller than h3?', h2 < h3)  # prints True
print('Is h2 greater than or equal to h1?', h2 >= h1)  # Prints False
print('Which one is biggest?', max(houses))  # Prints 'h3: 1101-square-foot Split'
print('Which is smallest?', min(houses))  # Prints 'h2: 846-square-foot Ranch'

# 8.25 创建缓存实例
import weakref


class Spam:
    _spam_cache = weakref.WeakValueDictionary()

    def __new__(cls, name):
        if name in cls._spam_cache:
            return cls._spam_cache[name]
        else:
            self = super().__new__(cls)
            cls._spam_cache[name] = self
            return self

    def __init__(self, name):
        print('Initializing Spam')
        self.name = name


s = Spam('Dave')
t = Spam('Dave')
print(s, t, s is t)

import weakref


class CachedSpamManager:
    def __init__(self):
        self._cache = weakref.WeakValueDictionary()

    def get_spam(self, name):
        if name not in self._cache:
            temp = Spam._new(name)  # Modified creation
            self._cache[name] = temp
        else:
            temp = self._cache[name]
        return temp

    def clear(self):
        self._cache.clear()


class Spam:
    manager = CachedSpamManager()

    def __init__(self, *args, **kwargs):
        raise RuntimeError("Can't instantiate directly")

    # Alternate constructor
    @classmethod
    def _new(cls, name):
        self = cls.__new__(cls)
        self.name = name
        return self


def get_spam(name):
    return Spam.manager.get_spam(name)


s = get_spam('foo')
t = get_spam('foo')
print(s, t, s is t)


class FooParent(object):
    def __init__(self):
        self.parent = 'I\'m the parent.'
        print ('Parent')

    def bar(self,message):
        print ("%s from Parent" % message)

class FooChild(FooParent):
    def __init__(self):
        # super(FooChild,self) 首先找到 FooChild 的父类（就是类 FooParent），然后把类 FooChild 的对象转换为类 FooParent 的对象
        super(FooChild,self).__init__()
        print ('Child')

    def bar(self,message):
        super(FooChild, self).bar(message)
        print ('Child bar fuction')
        print (self.parent)

class FooChild1(FooParent):
    def __init__(self):
        super().__init__()
        print ('Child')

    def bar(self,message):
        super().bar(message)
        print ('Child bar fuction')
        print (self.parent)

fooChild = FooChild()
fooChild.bar('HelloWorld')
fooChild1 = FooChild1()
fooChild1.bar('HelloWorld')

print(isinstance(fooChild,FooParent))
print(isinstance(fooChild1,FooParent))