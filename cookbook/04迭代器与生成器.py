# 第四章：迭代器与生成器
import traceback

# 4.1 手动遍历迭代器
with open('00基础.py', encoding='UTF-8') as f:
    try:
        while True:
            line = next(f, None)
            if line is None:
                break
            print(line, end='')
    except StopIteration as e:
        print(e.value)
        traceback.print_exc()
data = [1, 2, 3]
it = iter(data)
print(next(it), next(it), next(it), next(it, None))


# 4.2 代理迭代
# ython的迭代器协议需要 __iter__() 方法返回一个实现了 __next__() 方法的迭代器对象。
class Node:
    def __init__(self, value):
        self._value = value
        self._children = []

    def __repr__(self):
        return 'Node({!r})'.format(self._value)

    def add_child(self, node):
        self._children.append(node)
        return self

    def __iter__(self):
        return iter(self._children)


root = Node(0)
root.add_child(Node(1)).add_child(Node(2))
print(root)
for ch in root:
    print(ch)


# 4.3 使用生成器创建新的迭代模式
def frange(start, stop, increment):
    x = start
    while x < stop:
        yield x
        x += increment


for n in frange(0, 4, 0.5):
    print(n)


def countdown(n):
    print('Starting to count from', n)
    while n > 0:
        yield n
        n -= 1
    print('Done!')


data = countdown(3)
print(next(data), next(data), next(data), next(data, None))


# 4.4 实现迭代器协议
class Node1:
    def __init__(self, value):
        self._value = value
        self._children = []

    def __repr__(self):
        return 'Node({!r})'.format(self._value)

    def add_child(self, node):
        self._children.append(node)
        return self

    def __iter__(self):
        return iter(self._children)

    def depth_first(self):
        yield self
        for c in self:
            yield from c.depth_first()


root = Node1(0)
child1 = Node1(1)
child2 = Node1(2)
root.add_child(child1)
root.add_child(child2)
child1.add_child(Node1(3))
child1.add_child(Node1(4))
child2.add_child(Node1(5))

for ch in root.depth_first():
    print(ch)
# 4.5 反向迭代
data = [1, 2, 3]
for x in reversed(data):
    print(x)

data = open('00基础.py', encoding='UTF-8')
for line in reversed(list(data)):
    print(line, end='')


class Countdown:
    def __init__(self, start):
        self.start = start

    # Forward iterator
    def __iter__(self):
        n = self.start
        while n > 0:
            yield n
            n -= 1

    # Reverse iterator
    def __reversed__(self):
        n = 1
        while n <= self.start:
            yield n
            n += 1


for rr in reversed(Countdown(3)):
    print(rr)
for rr in Countdown(3):
    print(rr)
# 4.6 带有外部状态的生成器函数
from collections import deque


class linehistory:
    def __init__(self, lines, histlen=3):
        self.lines = lines
        self.history = deque(maxlen=histlen)

    def __iter__(self):
        for lineno, line in enumerate(self.lines, 1):
            self.history.append((lineno, line))
            yield line

    def clear(self):
        self.history.clear()


with open('00基础.py', encoding='UTF-8') as f:
    lines = linehistory(f)
    for line in lines:
        if 'print' in line:
            for lineno, hline in lines.history:
                print('{}:{}'.format(lineno, hline), end='')

f = open('00基础.py', encoding='UTF-8')
lines = linehistory(f)
it = iter(lines)
print(next(it), next(it), next(it), next(it, None))


# 4.7 迭代器切片
def count(n):
    while True:
        yield n
        n += 1


import itertools

# 函数 islice() 返回一个可以生成指定元素的迭代器，它通过遍历并丢弃直到切片开始索引位置的所有元素。 然后才开始一个个的返回元素，并直到切片结束索引位置。
data = count(0)

for x in itertools.islice(data, 10, 13):
    print(x)
# 4.8 跳过可迭代对象的开始部分
from itertools import dropwhile

with open('00基础.py', encoding='UTF-8') as f:
    for line in dropwhile(lambda line: not line.startswith('#'), f):
        print(line, end='')

from itertools import islice

data = ['a', 'b', 'c', 1, 4, 10, 15]
for x in islice(data, 2, 4):
    print(x)
# 4.9 排列组合的迭代
data = ['a', 'b', 'c', 1, 4, 10, 15]
from itertools import permutations

# itertools.permutations() ， 它接受一个集合并产生一个元组序列，每个元组由集合中所有元素的一个可能排列组成，也就是说通过打乱集合中元素排列顺序生成一个元组。
for p in permutations(data):
    print(p)

from itertools import combinations

# itertools.combinations() 可得到输入集合中元素的所有的组合。
for c in combinations(data, 3):
    print(c)

from itertools import combinations_with_replacement

# itertools.combinations_with_replacement() 允许同一个元素被选择多次
for c in combinations_with_replacement(data, 3):
    print(c)
# 4.10 序列上索引值迭代
data = ['a', 'b', 'c', 1, 4, 10, 15]
for idx, val in enumerate(data):
    print(idx, val)

from collections import defaultdict

word_summary = defaultdict(list)

with open('00基础.py', 'r', encoding='UTF-8') as f:
    lines = f.readlines()

for idx, line in enumerate(lines):
    # Create a list of words in current line
    words = [w.strip().lower() for w in line.split()]
    for word in words:
        word_summary[word].append(idx)
print(word_summary)

data = [(1, 2), (3, 4), (5, 6), (7, 8)]
for n, (x, y) in enumerate(data):
    print(n, x, y)
# 4.11 同时迭代多个序列
# zip(a, b) 会生成一个可返回元组 (x, y) 的迭代器，其中x来自a，y来自b。 一旦其中某个序列到底结尾，迭代宣告结束。 因此迭代长度跟参数中最短序列长度一致。
data1 = [1, 5, 4, 2, 10, 7]
data2 = [101, 78, 37, 15, 62, 99, 33, 44, 55]
for x, y in zip(data1, data2):
    print(x, y)
data = list(zip(data1, data2))  # 二维数组
print(data)

from itertools import zip_longest

for i in zip_longest(data1, data2):
    print(i)
for i in zip_longest(data1, data2, fillvalue=0):
    print(i)

headers = ['name', 'shares', 'price']
values = ['ACME', 100, 490.1]
for name, val in zip(headers, values):
    print(name, '=', val)
data = dict(zip(headers, values))
for key, value in data.items():
    print('%s = %s' % (key, value))
# 4.12 不同集合上元素的迭代
# itertools.chain() 接受一个或多个可迭代对象作为输入参数。 然后创建一个迭代器，依次连续的返回每个可迭代对象中的元素。 这种方式要比先将序列合并再迭代要高效的多。
from itertools import chain

data1 = [1, 2, 3, 4]
data2 = ['x', 'y', 'z']
for x in data1 + data2:
    print(x)
for x in chain(data1, data2):
    print(x)
# 4.13 创建数据处理管道
import os
import fnmatch
import gzip
import bz2
import re


def gen_find(filepat, top):
    '''
    Find all filenames in a directory tree that match a shell wildcard pattern
    '''
    for path, dirlist, filelist in os.walk(top):
        for name in fnmatch.filter(filelist, filepat):
            yield os.path.join(path, name)


def gen_opener(filenames):
    '''
    Open a sequence of filenames one at a time producing a file object.
    The file is closed immediately when proceeding to the next iteration.
    '''
    for filename in filenames:
        if filename.endswith('.gz'):
            f = gzip.open(filename, 'rt', encoding='UTF-8')
        elif filename.endswith('.bz2'):
            f = bz2.open(filename, 'rt', encoding='UTF-8')
        else:
            f = open(filename, 'rt', encoding='UTF-8')
        yield f
        f.close()


def gen_concatenate(iterators):
    '''
    Chain a sequence of iterators together into a single sequence.
    '''
    for it in iterators:
        yield from it


def gen_grep(pattern, lines):
    '''
    Look for a regex pattern in a sequence of lines
    '''
    pat = re.compile(pattern)
    for line in lines:
        if pat.search(line):
            yield line


lognames = gen_find('*.py', './')
files = gen_opener(lognames)
lines = gen_concatenate(files)
pylines = gen_grep('print.*', lines)
for line in pylines:
    print(line)
# 4.14 展开嵌套的序列
from collections.abc import Iterable


def flatten(items, ignore_types=(str, bytes)):
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            yield from flatten(x)
        else:
            yield x


data = [1, 2, [3, 4, [5, 6], 7], 8]
for x in flatten(data):
    print(x)
data = ['Dave', 'Paula', ['Thomas', 'Lewis']]
for x in flatten(data):
    print(x)
# 4.15 顺序迭代合并后的排序迭代对象
import heapq

data1 = [1, 3, 5, 7]
data2 = [2, 4, 6, 8]
for c in heapq.merge(data1, data2):
    print(c)
# 4.16 迭代器代替while无限循环
