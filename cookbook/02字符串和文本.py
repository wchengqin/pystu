# 第二章：字符串和文本
import re, os, sys

'''
正则表达式
正则表达式里面有括号的时候，括号表示为一个分组，其内容直接作为结果输出，而不把整个正则表达式匹配到的内容作为结果输出。
为了获得我们想匹配的整个字符串，就要用到非捕获组的概念了。以 (?) 开头的组是纯的非捕获组，它不捕获文本，也不针对组合计进行计数。
就是说，如果小括号中以?号开头，那么这个分组就不会捕获文本。
'''
# 2.1 使用多个界定符分割字符串
line = 'asdf fjdk; afed, fjek,asdf, foo'
data = re.split(r'(;|,|\s)\s*', line)
values = data[::2]
delimiters = data[1::2] + ['']
print(data, values, delimiters)
# 2.2 字符串开头或结尾匹配
url = 'http://www.python.org'
print(url.startswith('file:'), url.startswith('http://'), url.endswith('.txt'), url.endswith('.org'),
      re.match('http:|https:|ftp:', url))
filenames = os.listdir('.')
data1 = [name for name in filenames if name.endswith(('.py', '.txt')) and name.startswith(('01', '02'))]
print(filenames, data1)

# 2.3 用Shell通配符匹配字符串
from fnmatch import fnmatch, fnmatchcase

print(fnmatch('foo.txt', '*.txt'), fnmatch('foo.txt', '?oo.txt'), fnmatch('Dat45.csv', 'Dat[0-9]*'))
filenames = ['Dat1.csv', 'Dat2.csv', 'config.ini', 'foo.py']
data1 = [name for name in filenames if fnmatch(name, 'Dat*.csv')]
print(data1)
print(fnmatchcase('foo.txt', '*.TXT'))  # 区别大小写
# 2.4 字符串匹配和搜索   2.5 字符串搜索和替换   2.6 字符串忽略大小写的搜索替换
text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
print(text.find('yes'), text.find('starts'))
data1 = re.findall('Starts', text, flags=re.IGNORECASE)
print('IGNORECASE', data1)
datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
print(re.match(datepat, text), datepat.findall(text))
for m in datepat.finditer(text):
    print(m.groups())

data1 = text.replace('PyCon', 'ABC')
print(data1)
# sub() 函数中的第一个参数是被匹配的模式，第二个参数是替换模式。反斜杠数字比如 \3 指向前面模式的捕获组号。
data1 = re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text)
data2 = datepat.sub(r'\3-\1-\2', text)
data3 = re.sub('Starts', 'xxxxx', text, flags=re.IGNORECASE)
print(data1, data2, data3)
data1 = re.sub(r'(?P<month>\d+)/(?P<day>\d+)/(?P<year>\d+)', r'\g<year>-\g<month>-\g<day>', text)
print(data1)

from calendar import month_abbr


def change_date(m):
    mon_name = month_abbr[int(m.group(1))]
    return '{} {} {}'.format(m.group(2), mon_name, m.group(3))


data1 = datepat.sub(change_date, text)
print(data1)

data1, count = datepat.subn(r'\3-\1-\2', text)
print(data1, count)
# 2.7 最短匹配模式   2.8 多行匹配模式
text = 'Computer says "no." Phone says "yes."'
pat1 = re.compile(r'"(.*)"')
print("贪婪模式", pat1.findall(text))
pat2 = re.compile(r'"(.*?)"')  # 通过在 * 或者 + 这样的操作符后面添加一个 ? 可以强制匹配算法改成寻找最短的可能匹配
print("非贪婪模式", pat2.findall(text))

text = '''/* Computer say "no."
but 
Phone says "yes."  */
'''
pat1 = re.compile(r'/\*(.*?)\*/')
print(pat1.findall(text))  # 默认不支持换行
pat2 = re.compile(r'/\*((?:.|\n)*?)\*/')
print(pat2.findall(text))
pat1 = re.compile(r'/\*(.*?)\*/', re.DOTALL)  # re.DOTALL 可以让正则表达式中的点(.)匹配包括换行符在内的任意字符
print(pat1.findall(text))
# 2.9 将Unicode文本标准化   2.10 在正则式中使用Unicode
'''
normalize() 第一个参数指定字符串标准化的方式。 NFC表示字符应该是整体组成(比如可能的话就使用单一编码)，
而NFD表示字符应该分解为多个组合字符表示。
'''
import unicodedata

data1 = 'Spicy Jalape\u00f1o'
data2 = 'Spicy Jalapen\u0303o'
print(data1, data2, data1 == data2)
data1 = unicodedata.normalize('NFC', data1)
data2 = unicodedata.normalize('NFC', data2)
print(data1, data2, data1 == data2, ascii(data1))
data1 = unicodedata.normalize('NFD', data1)
data2 = unicodedata.normalize('NFD', data2)
print(data1, data2, data1 == data2, ascii(data1))
arabic = re.compile('[\u0600-\u06ff\u0750-\u077f\u08a0-\u08ff]+')
print(arabic.match('123'))
# 2.11 删除字符串中不需要的字符    2.12 审查清理文本字符串
'''
strip() 方法能用于删除开始或结尾的字符。 
lstrip() 和 rstrip() 分别从左和从右执行删除操作。 
默认情况下，这些方法会去除空白字符，但是你也可以指定其他字符。
'''
data = ' hello world!!! \n'
print(data.strip(), data.lstrip(), data.rstrip())
print(data.replace(' ', ''), re.sub('\s+', ' ', data))
data = '!hello world!!!'
print(data.strip('!'), data.lstrip('!'), data.rstrip('!'))
data = 'pýtĥöñ\fis\tawesome\r\n'
data1 = unicodedata.normalize('NFD', data)
print(data1.encode('ascii', 'ignore').decode('ascii'))
remap = {ord('\t'): ' ',
         ord('\f'): ' ',
         ord('\r'): None  # Deleted
         }
print(data.translate(remap))
cmb_chrs = dict.fromkeys(c for c in range(sys.maxunicode) if unicodedata.combining(chr(c)))
data1 = unicodedata.normalize('NFD', data).translate(cmb_chrs)
print(data1)

digitmap = {c: ord('0') + unicodedata.digit(chr(c)) for c in range(sys.maxunicode) if
            unicodedata.category(chr(c)) == 'Nd'}
print(len(digitmap))
data = '\u0661\u0662\u0663'
print(data.translate(digitmap))
# 2.13 字符串对齐  2.14 合并拼接字符串  2.15 字符串中插入变量   2.16 以指定列宽格式化字符串
data = 'Hello World'
print(data.ljust(20), data.rjust(20), data.center(20))
print(data.ljust(20, '*'), data.rjust(20, '*'), data.center(20, '*'))
print(format(data, '>20'), format(data, '<20'), format(data, '^20'))
data = ['Is', 'Chicago', 'Not', 'Chicago?']
print(' '.join(data))
print('{} {} {} {}'.format(*data))
data1 = ''
for s in data:
    data1 += (' ' + s)
print(data1[1:])
data = '{name} has {n} messages.'
print(data.format(name='Guido', n=37))
# 如果要被替换的变量能在变量域中找到， 那么你可以结合使用 format_map() 和 vars() 。
name = 'Wang'
n = 33
print(data.format_map(vars()))
import textwrap

data = "Look into my eyes, look into my eyes, the eyes, the eyes, \
the eyes, not around the eyes, don't look around the eyes, \
look into my eyes, you're under."
print(textwrap.fill(data, 170))
print(textwrap.fill(data, 40, initial_indent='    '))
# print(os.get_terminal_size().columns)  # 获取终端的大小尺寸
# 2.17 在字符串中处理html和xml
import html
from html.parser import HTMLParser
from xml.sax import saxutils

data = 'Elements are written as "<tag>text</tag>".'
print(html.escape(data), html.escape(data, quote=False))  # 特殊字符转义
data = 'Spicy Jalapeño'
print(data.encode('ascii', errors='xmlcharrefreplace'))  # ascii码转义
data = 'Spicy &quot;Jalape&#241;o&quot.'
# parse = HTMLParser()
print(html.unescape(data))
data = 'The prompt is &gt;&gt;&gt;'
print(saxutils.unescape(data))
# 2.18 字符串令牌解析
from collections import namedtuple

data = 'foo = 23 + 42 * 10'
tokens = [('NAME', 'foo'), ('EQ', '='), ('NUM', '23'), ('PLUS', '+'),
          ('NUM', '42'), ('TIMES', '*'), ('NUM', '10')]
NAME = r'(?P<NAME>[a-zA-Z_][a-zA-Z_0-9]*)'
NUM = r'(?P<NUM>\d+)'
PLUS = r'(?P<PLUS>\+)'
TIMES = r'(?P<TIMES>\*)'
EQ = r'(?P<EQ>=)'
WS = r'(?P<WS>\s+)'

master_pat = re.compile('|'.join([NAME, NUM, PLUS, TIMES, EQ, WS]))


def generate_tokens(pat, text):
    Token = namedtuple('Token', ['type', 'value'])
    scanner = pat.scanner(text)
    for m in iter(scanner.match, None):
        yield Token(m.lastgroup, m.group())


for tok in generate_tokens(master_pat, 'foo = 42'):
    print(tok)
tokens = (tok for tok in generate_tokens(master_pat, data)
          if tok.type != 'WS')
for tok in tokens:
    print(tok)
# 2.19 实现一个简单的递归下降分析器
# !/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Topic: 下降解析器
Desc :
"""
import re
import collections

# Token specification
NUM = r'(?P<NUM>\d+)'
PLUS = r'(?P<PLUS>\+)'
MINUS = r'(?P<MINUS>-)'
TIMES = r'(?P<TIMES>\*)'
DIVIDE = r'(?P<DIVIDE>/)'
LPAREN = r'(?P<LPAREN>\()'
RPAREN = r'(?P<RPAREN>\))'
WS = r'(?P<WS>\s+)'

master_pat = re.compile('|'.join([NUM, PLUS, MINUS, TIMES,
                                  DIVIDE, LPAREN, RPAREN, WS]))
# Tokenizer
Token = collections.namedtuple('Token', ['type', 'value'])


def generate_tokens(text):
    scanner = master_pat.scanner(text)
    for m in iter(scanner.match, None):
        tok = Token(m.lastgroup, m.group())
        if tok.type != 'WS':
            yield tok


# Parser
class ExpressionEvaluator:
    '''
    Implementation of a recursive descent parser. Each method
    implements a single grammar rule. Use the ._accept() method
    to test and accept the current lookahead token. Use the ._expect()
    method to exactly match and discard the next token on on the input
    (or raise a SyntaxError if it doesn't match).
    '''

    def parse(self, text):
        self.tokens = generate_tokens(text)
        self.tok = None  # Last symbol consumed
        self.nexttok = None  # Next symbol tokenized
        self._advance()  # Load first lookahead token
        return self.expr()

    def _advance(self):
        'Advance one token ahead'
        self.tok, self.nexttok = self.nexttok, next(self.tokens, None)

    def _accept(self, toktype):
        'Test and consume the next token if it matches toktype'
        if self.nexttok and self.nexttok.type == toktype:
            self._advance()
            return True
        else:
            return False

    def _expect(self, toktype):
        'Consume next token if it matches toktype or raise SyntaxError'
        if not self._accept(toktype):
            raise SyntaxError('Expected ' + toktype)

    # Grammar rules follow
    def expr(self):
        "expression ::= term { ('+'|'-') term }*"
        exprval = self.term()
        while self._accept('PLUS') or self._accept('MINUS'):
            op = self.tok.type
            right = self.term()
            if op == 'PLUS':
                exprval += right
            elif op == 'MINUS':
                exprval -= right
        return exprval

    def term(self):
        "term ::= factor { ('*'|'/') factor }*"
        termval = self.factor()
        while self._accept('TIMES') or self._accept('DIVIDE'):
            op = self.tok.type
            right = self.factor()
            if op == 'TIMES':
                termval *= right
            elif op == 'DIVIDE':
                termval /= right
        return termval

    def factor(self):
        "factor ::= NUM | ( expr )"
        if self._accept('NUM'):
            return int(self.tok.value)
        elif self._accept('LPAREN'):
            exprval = self.expr()
            self._expect('RPAREN')
            return exprval
        else:
            raise SyntaxError('Expected NUMBER or LPAREN')


tool = ExpressionEvaluator()
print(tool.parse('2'))
print(tool.parse('2 + 3'))
print(tool.parse('2 + 3 * 4'))
print(tool.parse('2 + (3 + 4) * 5'))

# 2.20 字节字符串上的字符串操作
data = b'Hello World'
print(data[0:5], data.startswith(b'Hello'), data.split(), data.replace(b'Hello', b'Hello Cruel'))
data = bytearray(b'Hello World')
print(data[0:5], data.startswith(b'Hello'), data.split(), data.replace(b'Hello', b'Hello Cruel'))
print('{:10s} {:10d} {:10.2f}'.format('ACME', 100, 490.1).encode('ascii'))
