# 第九章：元编程 （注解）
import logging

logging.basicConfig(level=logging.DEBUG)
# 9.1 在函数上添加包装器   9.2 创建装饰器时保留函数元信息   9.3 解除一个装饰器
'''
需要强调的是装饰器并不会修改原始函数的参数签名以及返回值。 使用 *args 和 **kwargs 目的就是确保任何参数都能适用。
而返回结果值基本都是调用原始函数 func(*args, **kwargs) 的返回结果，其中func就是原始函数。
任何时候你定义装饰器的时候，都应该使用 functools 库中的 @wraps 装饰器来注解底层包装函数。
'''
import time
from functools import wraps


def exec_log(func):
    '''
    Decorator that reports the execution time.
    '''

    @wraps(func)  # @wraps可复制元信息
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        logger = logging.getLogger(func.__name__)
        logger.info('开始时间：%s，结束时间：%s，执行时长：%d' % (str(start), str(end), end - start))
        return result

    return wrapper


@exec_log  # 注解方式
def test1(n) -> str:
    '''
    :param n: number
    :return: str
    '''
    for i in range(n):
        # print(i)
        pass
    return 'ok'


test1(10)
test1(20)
test1(30)


def test2(n) -> str:
    '''
    :param n: number
    :return: str
    '''
    for i in range(n):
        # print(i)
        pass
    return 'ok'


test2 = exec_log(test2)  # 代码方式
test2(10)
test2(20)
test2(30)

print(test1, test1(10), test1.__name__, test1.__doc__, test1.__annotations__)
# @wraps 有一个重要特征是它能让你通过属性 __wrapped__ 直接访问被包装函数。
print(test1.__wrapped__(10))
# __wrapped__ 属性还能让被装饰函数正确暴露底层的参数签名信息。
from inspect import signature

print(signature(test1))
# 假设装饰器是通过 @wraps来实现的，那么你可以通过访问 __wrapped__ 属性来访问原始函数
temp = test1.__wrapped__
print(temp(5))
# 9.4 定义一个带参数的装饰器
from functools import wraps


def logged(level, name=None, message=None):
    """
    Add logging to a function. level is the logging
    level, name is the logger name, and message is the
    log message. If name and message aren't specified,
    they default to the function's module and name.
    """

    def decorate(func):
        logname = name if name else func.__module__
        log = logging.getLogger(logname)
        logmsg = message if message else func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log.log(level, logmsg)
            return func(*args, **kwargs)

        return wrapper

    return decorate


# Example use
@logged(logging.DEBUG, name='test log', message='where')
def test1(x, y) -> int:
    '''

    :param x:
    :param y:
    :return:
    '''
    return x + y


print(test1, test1(10, 20), test1.__name__, test1.__doc__, test1.__annotations__)
# 9.5 可自定义属性的装饰器
from functools import wraps, partial
import logging


# Utility decorator to attach a function as an attribute of obj
def attach_wrapper(obj, func=None):
    if func is None:
        return partial(attach_wrapper, obj)
    setattr(obj, func.__name__, func)
    return func


def logged(level, name=None, message=None):
    '''
    Add logging to a function. level is the logging
    level, name is the logger name, and message is the
    log message. If name and message aren't specified,
    they default to the function's module and name.
    '''

    def decorate(func):
        logname = name if name else func.__module__
        log = logging.getLogger(logname)
        logmsg = message if message else func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log.log(level, logmsg)
            return func(*args, **kwargs)

        # Attach setter functions
        @attach_wrapper(wrapper)
        def set_level(newlevel):
            nonlocal level
            level = newlevel

        @attach_wrapper(wrapper)
        def set_message(newmsg):
            nonlocal logmsg
            logmsg = newmsg

        return wrapper

    return decorate


# Example use
@logged(logging.DEBUG)
def test1(x, y):
    return x + y


print(test1, test1(10, 20), test1.__name__, test1.__doc__, test1.__annotations__)
test1.set_message('Add called')
test1.set_level(logging.WARNING)
print(test1, test1(10, 20), test1.__name__, test1.__doc__, test1.__annotations__)

# 9.6 带可选参数的装饰器
from functools import wraps, partial
import logging


def logged(func=None, *, level=logging.DEBUG, name=None, message=None):
    if func is None:
        return partial(logged, level=level, name=name, message=message)

    logname = name if name else func.__module__
    log = logging.getLogger(logname)
    logmsg = message if message else func.__name__

    @wraps(func)
    def wrapper(*args, **kwargs):
        log.log(level, logmsg)
        return func(*args, **kwargs)

    return wrapper


# Example use
@logged
def test1(x, y):
    return x + y


print(test1, test1(10, 20), test1.__name__, test1.__doc__, test1.__annotations__)


@logged(level=logging.CRITICAL, name='example')
def spam():
    print('Spam!')


spam()


def spam():
    print('Spam!')


spam = logged(level=logging.CRITICAL, name='example')(spam)
spam()
# 9.7 利用装饰器强制函数上的类型检查
from inspect import signature
from functools import wraps


def typeassert(*ty_args, **ty_kwargs):
    def decorate(func):
        # If in optimized mode, disable type checking
        if not __debug__:
            return func

        # Map function argument names to supplied types
        sig = signature(func)
        bound_types = sig.bind_partial(*ty_args, **ty_kwargs).arguments

        @wraps(func)
        def wrapper(*args, **kwargs):
            bound_values = sig.bind(*args, **kwargs)
            # Enforce type assertions across supplied arguments
            for name, value in bound_values.arguments.items():
                if name in bound_types:
                    if not isinstance(value, bound_types[name]):
                        raise TypeError(
                            'Argument {} must be {}'.format(name, bound_types[name])
                        )
            return func(*args, **kwargs)

        return wrapper

    return decorate


@typeassert(int, int)
def test1(x, y):
    return x + y


print(test1(1, 2))
try:
    print(test1(1, '2'))
except Exception as e:
    print(e)

# 9.8 将装饰器定义为类的一部分
# 9.9 将装饰器定义为类
# 9.10 为类和静态方法提供装饰器
# 9.11 装饰器为被包装函数增加参数
# 9.12 使用装饰器扩充类的功能
# 9.13 使用元类控制实例的创建
# 9.14 捕获类的属性定义顺序
# 9.15 定义有可选参数的元类
# 9.16 *args和**kwargs的强制参数签名
# 9.17 在类上强制使用编程规约
# 9.18 以编程方式定义类
# 9.19 在定义的时候初始化类的成员
# 9.20 利用函数注解实现方法重载
# 9.21 避免重复的属性方法
# 9.22 定义上下文管理器的简单方法
# 9.23 在局部变量域中执行代码
# 9.24 解析与分析Python源码
# 9.25 拆解Python字节码
