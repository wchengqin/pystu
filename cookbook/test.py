import re

s = '#010203'
a = re.match(r'#\d{6}', s)
print(re.findall(r'\d{2}',s[1:]))


class result(dict):
    def __init__(self, status: int = 1, message: str = 'success', **kwargs):
        super().__init__(kwargs)
        self.update({'status': status, 'message': message})

    def get_status(self):
        return self.get('status')

    def get_message(self):
        return self.get('message')

    def get_data(self):
        return self.get('data')

    def set_data(self, data=None):
        return self.update({'data': data})

    @property
    def success(self):
        return self.get_status() == 1
a=result()
print(a)
