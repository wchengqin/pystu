# 第六章：数据编码和处理
# 6.1 读写CSV数据
import csv
from collections import namedtuple

filename = 'temp/stocks.csv'

headers = ['Symbol', 'Price', 'Date', 'Time', 'Change', 'Volume']
rows = [('AA', 39.48, '6/11/2007', '9:36am', -0.18, 181800),
        ('AIG', 71.38, '6/11/2007', '9:36am', -0.15, 195500),
        ('AXP', 62.58, '6/11/2007', '9:36am', -0.46, 935000)
        ]

with open(filename, 'w', encoding='UTF-8', newline='') as f:
    f_csv = csv.writer(f)
    f_csv.writerow(headers)
    f_csv.writerows(rows)

headers = ['Symbol', 'Price', 'Date', 'Time', 'Change', 'Volume']
rows = [{'Symbol': 'AA', 'Price': 39.48, 'Date': '6/11/2007',
         'Time': '9:36am', 'Change': -0.18, 'Volume': 181800},
        {'Symbol': 'AIG', 'Price': 71.38, 'Date': '6/11/2007',
         'Time': '9:36am', 'Change': -0.15, 'Volume': 195500},
        {'Symbol': 'AXP', 'Price': 62.58, 'Date': '6/11/2007',
         'Time': '9:36am', 'Change': -0.46, 'Volume': 935000}
        ]

with open(filename, 'w', encoding='UTF-8', newline='') as f:
    f_csv = csv.DictWriter(f, headers)  # 字典序列
    f_csv.writeheader()
    f_csv.writerows(rows)

with open(filename) as f:
    f_csv = csv.reader(f)
    headers = next(f_csv)
    Column = namedtuple('Column', headers)
    print(headers)
    for row in f_csv:
        print(row)
        row_tuple = Column(*row)  # 命名元组便于访问
        print(row_tuple.Date)

with open(filename) as f:
    f_csv = csv.DictReader(f)  # 字典序列
    for row in f_csv:
        print(row)

# 6.2 读写JSON数据
import json

filename = 'temp/stocks.json'
data = json.dumps(rows)
print(type(data), data)
data = json.loads(data)
print(type(data), data)
print(json.dumps(data, indent=4))  # 格式化字符串后输出
with open(filename, 'w', encoding='UTF-8') as f:
    json.dump(data, f)
with open(filename, 'r', encoding='UTF-8') as f:
    data = json.load(f)
    print(data)
from collections import OrderedDict

data = json.loads(json.dumps(rows), object_pairs_hook=OrderedDict)
print(data)


class JSONObject:
    def __init__(self, d):
        self.__dict__ = d


data = json.loads(json.dumps(rows), object_hook=JSONObject)
print(data, len(data), data[0].Date)


# 自定义对象的序列化
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


data = Point(2, 3)
try:
    json.dumps(data)
except Exception as e:
    print(e)


# 如果你想序列化对象实例，你可以提供一个函数，它的输入是一个实例，返回一个可序列化的字典。
def serialize_instance(obj):
    d = {'__classname__': type(obj).__name__}
    d.update(vars(obj))
    return d


# Dictionary mapping names to known classes
classes = {
    'Point': Point
}


def unserialize_object(d):
    clsname = d.pop('__classname__', None)
    if clsname:
        cls = classes[clsname]
        obj = cls.__new__(cls)  # Make instance without calling __init__
        for key, value in d.items():
            setattr(obj, key, value)
        return obj
    else:
        return d


data = json.dumps(data, default=serialize_instance)
print(data)
data = json.loads(data, object_hook=unserialize_object)
print(data, data.x, data.y)
# 6.3 解析简单的XML数据
from urllib.request import urlopen
from xml.etree.ElementTree import parse

filename = 'temp/data.xml'

# u = urlopen('http://planet.python.org/rss20.xml')
# data = parse(u)
# print(data)
# for item in data.iterfind('channel/item'):
#     title = item.findtext('title')
#     date = item.findtext('pubDate')
#     link = item.findtext('link')
#     print(title, date, link)
# print(data.find('channel/title').tag, data.find('channel/title').text)

data = parse(filename)
print(data)
# 6.4 增量式解析大型XML文件
import traceback
from xml.etree.ElementTree import iterparse


def parse_and_remove(filename, path):
    path_parts = path.split('/')
    doc = iterparse(filename, events=('start', 'end'))
    next(doc)  # Skip the root element

    for event, elem in doc:
        if event == 'start' and elem.tag == path:
            yield elem


for data in parse_and_remove(filename, 'movie'):
    print(data, data.findtext('type'), data.findtext('format'), data.findtext('episodes'), data.findtext('rating'),
          data.findtext('stars'), data.findtext('description'))

# 6.5 将字典转换为XML
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import tostring


def dict_to_xml(tag, d):
    '''
    Turn a simple dict of key/value pairs into XML
    '''
    elem = Element(tag)
    for key, val in d.items():
        child = Element(key)
        child.text = str(val)
        elem.append(child)
    return elem


data = {'name': 'GOOG', 'shares': 100, 'price': 490.1}
elem = dict_to_xml('stock', data)
elem.set('level', 'A')
data1 = tostring(elem)
print(data1)

from xml.sax.saxutils import escape, unescape

# 特殊字符转义
data1 = escape(str(data1))
print(data1)
data1 = unescape(data1)
print(data1)

# 6.6 解析和修改XML
from xml.etree.ElementTree import parse, Element, tostring

data = parse(filename)
root = data.getroot()
print(root, root.tag, root.get('shelf'))
children = list(root)  # 子节点
print(len(children))
one = children.pop()
print(tostring(one))
one.remove(one.find('description'))
print(tostring(one), one.get('title'))
one.set('level', 'A')
elem = Element('remark')
elem.text = 'This is a test'
one.insert(2, elem)
elem = Element('id')
elem.text = '1'
one.append(elem)
print(tostring(one))
for e in one[2:4]:  # 对元素使用索引和切片
    print(e.tag, e.text)
# data.write('temp/data1.xml', xml_declaration=True)
# 6.7 利用命名空间解析XML文档
# 6.8 与关系型数据库的交互
# 6.9 编码和解码十六进制数  6.10 编码解码Base64数据
import binascii, base64

data = b'hello'
print(data, binascii.b2a_hex(data))
print(data, base64.b16encode(data), base64.b16decode(base64.b16encode(data)))
print(data, base64.b64encode(data), base64.b64decode(base64.b64encode(data)))
print(base64.b64encode(data).decode('ascii'))
#
# 6.11 读写二进制数组数据
from struct import Struct

'''
结构体通常会使用一些结构码值i, d, f等 [参考 Python文档 ]。 
这些代码分别代表某个特定的二进制数据类型如32位整数，64位浮点数，32位浮点数等。 
第一个字符 < 指定了字节顺序。在这个例子中，它表示”低位在前”。 更改这个字符为 > 表示高位在前，或者是 ! 表示网络字节顺序。
'''
struct_fmt = '<idd'
filename_bin = 'temp/data.bin'
records = [(1, 2.3, 4.5), (6, 7.8, 9.0), (12, 13.4, 56.7)]
# 写入二进制文件
with open(filename_bin, 'wb') as f:
    record_struct = Struct(struct_fmt)
    for r in records:
        f.write(record_struct.pack(*r))
# 读取二进制文件
with open(filename_bin, 'rb') as f:
    record_struct = Struct(struct_fmt)
    chunks = iter(lambda: f.read(record_struct.size), b'')
    data = (record_struct.unpack(chunk) for chunk in chunks)
    # print(data, next(data), next(data), next(data))
    for rec in data:
        print(rec)
# 一次性读取，分片解析
with open(filename_bin, 'rb') as f:
    data = f.read()
    record_struct = Struct(struct_fmt)
    data1 = (record_struct.unpack_from(data, offset) for offset in range(0, len(data), record_struct.size))
    for rec in data1:
        print(rec)

import struct

data = struct.pack(struct_fmt, 1, 2.0, 3.0)
print(data, struct.unpack(struct_fmt, data))

f = open(filename_bin, 'rb')
chunks = iter(lambda: f.read(20), b'')
for chk in chunks:
    print(chk)
f.close()

# 6.12 读取嵌套和可变长二进制数据
# 6.13 数据的累加与统计操作
