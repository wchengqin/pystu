# 第三章：数字日期和时间
# 3.1 数字的四舍五入
print(round(1.23, 1), round(1.27, 1), round(-1.27, 1), round(1.25361, 3))
data = 1627731
print(data, round(data, -1), round(data, -2), round(data, -3), round(data, -4))
data = 1.23456
print(data, format(data, '0.2f'), format(data, '0.3f'), 'value is {:0.4f}'.format(data))
# 3.2 执行精确的浮点数运算
data1 = 4.2
data2 = 2.1
data = data1 + data2
print(data, data == 6.3)
from decimal import Decimal

data1 = Decimal('4.2')
data2 = Decimal('2.1')
data = data1 + data2
print(data, data == Decimal('6.3'))
# 3.3 数字的格式化输出
data = 1234.56789
print(data, format(data, '0.2f'), format(data, '>10.1f'), format(data, '<10.1f'), format(data, '^10.1f'),
      format(data, ','), format(data, '0,.1f'), format(data, 'e'), format(data, '0.2E'))
print('The value is {:0,.2f}'.format(data))
swap_separators = {ord('.'): ',', ord(','): '.'}
print(format(data, ',').translate(swap_separators))
print(data, '%0.2f' % data, '%10.1f' % data, '%-10.1f' % data)
# 3.4 二八十六进制整数
# 为了将整数转换为二进制、八进制或十六进制的文本串， 可以分别使用 bin() , oct() 或 hex() 函数
data = 1234
print(data, bin(data), oct(data), hex(data))
# 如果你不想输出 0b , 0o 或者 0x 的前缀的话，可以使用 format() 函数
print(data, format(data, 'b'), format(data, 'o'), format(data, 'x'))
data = -1234
print(data, bin(data), oct(data), hex(data))
print(data, format(data, 'b'), format(data, 'o'), format(data, 'x'))
# 3.5 字节到大整数的打包与解包
data = b'\x00\x124V\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
print(data, len(data), int.from_bytes(data, 'little'), int.from_bytes(data, 'big'))
import struct

hi, lo = struct.unpack('>QQ', data)
print((hi << 64) + lo)
data = 94522842520747284487117727783387188
print(data, data.to_bytes(16, 'little'), data.to_bytes(16, 'big'))

data = 0x01020304
print(data, data.to_bytes(4, 'little'), data.to_bytes(4, 'big'))
data = 523 ** 23
print(data, data.bit_length())
nbytes, rem = divmod(data.bit_length(), 8)
if rem:
    nbytes += 1
print(data.to_bytes(nbytes, 'little'))
# 3.6 复数的数学运算
data = complex(2, 4)
print(data, data.real, data.imag, data.conjugate(), abs(data))
data1 = 3 - 5j
print(data + data1, data - data1, data * data1, data / data1)

import math, cmath

print(cmath.sin(data), cmath.cos(data), cmath.exp(data))
# 3.7 无穷大与NaN
print(float('inf'), float('-inf'), float('nan'), math.isinf(float('inf')), math.isnan(float('nan')))
# 3.8 分数运算
from fractions import Fraction

data1 = Fraction(5, 4)
data2 = Fraction(7, 16)
data = data1 * data2
print(data1, data2, data1 + data2, data, data.numerator, data.denominator, data.limit_denominator(8), float(data))
data = 3.75
print(Fraction(*data.as_integer_ratio()))
# 3.9 大型数组运算
data1 = [1, 2, 3, 4]
data2 = [5, 6, 7, 8]
print(data1 * 2, data1 + data2)
import numpy as np

data1 = np.array([1, 2, 3, 4])
data2 = np.array([5, 6, 7, 8])
print(data1 * 2, data1 + 10, data1 + data2, data1 * data2)

# 3.10 矩阵与线性代数运算
import numpy as np

data = np.matrix([[1, -2, 3], [0, 4, 5], [7, 8, -9]])
print(data, '\n', data.T, '\n', data.I)
data1 = np.matrix([[2], [3], [4]])
print(data1)
print(data * data1)
# 3.11 随机选择
import random

data = [1, 2, 3, 4, 5, 6, 7, 8, 9]
for i in range(5):
    print(random.choice(data), random.sample(data, i))  # 随机选择元素
random.shuffle(data)  # 打乱序列中元素的顺序
print(data)
print(random.randint(0, 10))  # 生成随机整数
print(random.random())  # 生成0到1范围内均匀分布的浮点数
print(random.getrandbits(100))  # 获取N位随机位(二进制)的整数
print(random.seed(), random.random(), random.seed(12345), random.random(), random.seed(b'bytedata'),
      random.random())  # 修改初始化种子
print(random.uniform(1, 10))  # 计算均匀分布随机数，
print(random.gauss(1, 10))  # 计算正态分布随机数
# 3.12 基本的日期与时间转换
from datetime import timedelta, datetime

# 时间段
data1 = timedelta(days=2, hours=6)
data2 = timedelta(hours=4.5)
data = data1 + data2
print(data.days, data.seconds, data.seconds / 3600, data.total_seconds() / 3600)

data1 = datetime(2012, 9, 23)
print(data1 + timedelta(days=10))
data2 = datetime(2012, 12, 21)
data = data2 - data1
print(data.days)
print(datetime.today(), datetime.today() + timedelta(minutes=10))
# datetime 自动处理闰年
data1 = datetime(2012, 3, 1)
data2 = datetime(2012, 2, 28)
data = data1 - data2
print(data.days)
data1 = datetime(2013, 3, 1)
data2 = datetime(2013, 2, 28)
data = data1 - data2
print(data.days)

from dateutil.relativedelta import relativedelta

data1 = datetime(2012, 9, 23)
print(data1, data1 + relativedelta(months=+1), data1 + relativedelta(months=+4))
# 3.13 计算上一个周五的日期
"""
Topic: 最后的周五
Desc :
"""
from datetime import datetime, timedelta

weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
            'Friday', 'Saturday', 'Sunday']


def get_previous_byday(dayname, start_date=None):
    if start_date is None:
        start_date = datetime.today()
    day_num = start_date.weekday()
    day_num_target = weekdays.index(dayname)
    days_ago = (7 + day_num - day_num_target) % 7
    if days_ago == 0:
        days_ago = 7
    target_date = start_date - timedelta(days=days_ago)
    return target_date


print(get_previous_byday('Sunday', datetime(2021, 7, 2)))
# 3.14 计算当前月份的日期范围
from datetime import datetime, date, timedelta
import calendar


def get_month_range(start_date=None):
    if start_date is None:
        start_date = date.today().replace(day=1)
    _, days_in_month = calendar.monthrange(start_date.year, start_date.month)
    end_date = start_date + timedelta(days=days_in_month)
    return (start_date, end_date)


first_day, last_day = get_month_range()
print(first_day, last_day)
# 3.15 字符串转换为日期
from datetime import datetime

data = '2020-09-20'
data1 = datetime.strptime(data, '%Y-%m-%d')
print(data1, datetime.now() - data1)


def parse_ymd(s):
    year_s, mon_s, day_s = s.split('-')
    return datetime(int(year_s), int(mon_s), int(day_s))


print(parse_ymd(data))
# 3.16 结合时区的日期操作
from datetime import datetime
from pytz import timezone, utc, country_timezones

data = datetime(2012, 12, 21, 9, 30, 0)
print(data)
central = timezone('US/Central')
loc_d = central.localize(data)
print(loc_d)
bang_d = loc_d.astimezone(timezone('Asia/Kolkata'))
print(bang_d)
from datetime import timedelta

later = central.normalize(loc_d + timedelta(minutes=30))
print(later)
utc_d = loc_d.astimezone(utc)
print(utc_d)
later_utc = utc_d + timedelta(minutes=30)
print(later_utc.astimezone(central))
print(country_timezones['IN'])
