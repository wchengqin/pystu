from abc import ABCMeta, abstractmethod


class MBDrive(metaclass=ABCMeta):
    def __init__(self, port, baudrate=9600, bytesize=8, parity='N', stopbits=1, timeout=5, verbose=True):
        self.port = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout
        self.verbose = verbose

    @abstractmethod
    def read(self, function_code, starting_address, quantity_of_x):
        pass

    @abstractmethod
    def write(self, function_code, starting_address, output_value):
        pass


if __name__ == "__main__":  # main program 程序入口
    import importlib

    module_name = 'cookbook.drive.humidity'  # 模块名的字符串
    metaclass = importlib.import_module(module_name)  # 导入的就是需要导入的那个metaclass
    metaclass.process(2051)
    data = metaclass.read(1, 2, 3)  # 调用下面的方法
    print(data)
    data = metaclass.write(1, 2, 3)
    print(data)

    module_name = 'cookbook.drive.default'
    try:
        metaclass = importlib.import_module(module_name)
    except ModuleNotFoundError as e:
        print(e)
