def process(command, *args, **kwargs):
    print(command)
    methname = 'cmd_%d' % command
    meth = getattr(methname, None)
    if meth is None:
        return 0
    else:
        meth(args, kwargs)
        return 1


def cmd_2051(*args, **kwargs):
    print('oooo')


def read(function_code, starting_address, quantity_of_x):
    print(
        'function_code=%d starting_address=%d quantity_of_x=%d ' % (function_code, starting_address, quantity_of_x))
    return 1, 2, 3


def write(function_code, starting_address, output_value):
    print('function_code=%d starting_address=%d output_value=%d ' % (function_code, starting_address, output_value))
    return 'success'
