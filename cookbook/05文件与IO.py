# 第五章：文件与IO
filepath = '../../pystu'
filename = '00基础.py'
new_filename = 'temp/data.txt'

f = open(filename, 'rt', encoding='UTF-8')
data = f.read()
print(data)
f.close()

# with语句给被使用到的文件创建了一个上下文环境，with 控制块结束时，文件会自动关闭
with open(filename, 'rt', encoding='UTF-8') as f:
    data = f.read()
    print(data)

# Iterate over the lines of the file
with open(filename, 'rt', encoding='UTF-8') as f:
    for line in f:
        print(line)
# 5.1 读写文本数据   5.2 打印输出至文件中
data = 'print("text1")'
with open(new_filename, 'wt') as f:
    f.write(data)
with open(new_filename, 'wt') as f:
    print(data, file=f)
    print(data, file=f)

# 5.3 使用其他分隔符或行终止符打印
print('ACME', 50, 91.5, sep=',', end='!!\n')
print(','.join(('ACME', '50', '91.5')))
row = ('ACME', 50, 91.5)
# print(','.join(row))
print(*row, sep=',')
# 5.4 读写字节数据
with open(new_filename, 'wb') as f:
    f.write(b'Hello World')
data = b'Hello World'
print(data[0])
for c in data:
    print(c)
with open(filename, 'rb') as f:
    data = f.read(16)
    text = data.decode('utf-8')
    print(text)

with open(new_filename, 'wb') as f:
    text = 'Hello World HaHa'
    f.write(text.encode('utf-8'))

import array

nums = array.array('i', [1, 2, 3, 4])
with open(new_filename, 'wb') as f:
    f.write(nums)
data = array.array('i', [0, 0, 0, 0, 0, 0, 0, 0])
with open(new_filename, 'rb') as f:
    f.readinto(data)
print(data)
# 5.5 文件不存在才能写入
# with open(new_filename, 'xt') as logging.conf:
#     logging.conf.write('Hello\n')
# 5.6 字符串的I/O操作
import io

s = io.StringIO()
s.write('Hello World\n')
print('This is a test', file=s)
print(s.getvalue())

s = io.StringIO('Hello\nWorld\n')
print(s.read(4))

print(s.read())
s = io.BytesIO()
s.write(b'binary data')
print(s.getvalue())
# 5.7 读写压缩文件
import os, gzip, bz2


def size_format(size):
    if size < 1000:
        return '%i' % size + 'Bit'
    elif 1000 <= size < 1000000:
        return '%.1f' % float(size / 1000) + 'KB'
    elif 1000000 <= size < 1000000000:
        return '%.1f' % float(size / 1000000) + 'MB'
    elif 1000000000 <= size < 1000000000000:
        return '%.1f' % float(size / 1000000000) + 'GB'
    elif 1000000000000 <= size:
        return '%.1f' % float(size / 1000000000000) + 'TB'


data = 'abcdefghijklmnopqrstuvwxyz|1234567890|'
for i in range(10):
    data += data
with open(new_filename, 'wt', encoding='UTF-8') as f:
    f.write(data)
with open(new_filename, 'rt', encoding='UTF-8') as f:
    print(f.read())
print('common', size_format(os.path.getsize(new_filename)))
with gzip.open(new_filename, 'wt', compresslevel=2, encoding='UTF-8') as f:
    f.write(data)
with gzip.open(new_filename, 'rt', encoding='UTF-8') as f:
    print(f.read())
print('gzip', size_format(os.path.getsize(new_filename)))
with bz2.open(new_filename, 'wt', encoding='UTF-8') as f:
    f.write(data)
with bz2.open(new_filename, 'rt', encoding='UTF-8') as f:
    print(f.read())
print('bz2', size_format(os.path.getsize(new_filename)))
# 5.8 固定大小记录的文件迭代
from functools import partial

RECORD_SIZE = 32

with open(filename, 'rb') as f:
    records = iter(partial(f.read, RECORD_SIZE), b'')
    for r in records:
        print(r)


# 5.9 读取二进制数据到可变缓冲区中
def read_into_buffer(filename):
    buf = bytearray(os.path.getsize(filename))
    with open(filename, 'rb') as f:
        f.readinto(buf)
    return buf


buf = read_into_buffer(filename)
print(buf)
buf = bytearray(RECORD_SIZE)
with open(filename, 'rb') as f:
    while True:
        n = f.readinto(buf)
        if n < RECORD_SIZE:
            break
print(buf)
# memoryview可以通过零复制的方式对已存在的缓冲区执行切片操作，甚至还能修改它的内容
data = memoryview(buf)
print(data)
# 5.10 内存映射的二进制文件
import os, mmap


def memory_map(filename, access=mmap.ACCESS_WRITE):
    size = os.path.getsize(filename)
    fd = os.open(filename, os.O_RDWR)
    return mmap.mmap(fd, size, access=access)


with open(new_filename, 'wb') as f:
    f.seek(1000 - 1)  # 将文件内容扩充到指定大小
    f.write(b'\x00')
print('filesize', os.path.getsize(new_filename))

data = memory_map(new_filename)
print(len(data), data[0:10])
data[0:11] = b'Hello World'
print(data[0:11])
data.close()
with open(new_filename, 'rb') as f:
    print(f.read(11))
with memory_map(new_filename, access=mmap.ACCESS_READ) as m:
    print(len(m), m[0:10])
# 5.11 文件路径名的操作
print(os.path.basename(filename))  # Get the last component of the path
print(os.path.dirname(filename))  # Get the directory name
print(os.path.join('pystu', 'cookbook', os.path.basename(filename)))  # Join path components together
print(os.path.expanduser(filename))  # Expand the user's home directory
print(os.path.splitext(filename))  # Split the file extension

# 5.12 测试文件是否存在
import os, time

print(os.path.exists(filename), os.path.exists(filepath))
print(os.path.isfile(filename), os.path.isfile(filepath))
print(os.path.isdir(filename), os.path.isdir(filepath))
print(os.path.islink(filename), os.path.islink('C:/Users/wang/Documents/Tencent Files'))  # Is a symbolic link
print(os.path.realpath(filename), os.path.realpath(filepath))
print(time.ctime(os.path.getmtime(filename)))
# 5.13 获取文件夹中的文件列表
import os

data = os.listdir(filepath)
for n in data:
    print(n)
# Get all regular files
names = [name for name in os.listdir(filepath)
         if os.path.isfile(os.path.join(filepath, name))]
print(names)
# Get all dirs
dirnames = [name for name in os.listdir(filepath)
            if os.path.isdir(os.path.join(filepath, name))]
print(dirnames)
pyfiles = [name for name in os.listdir(filepath + '/cookbook')
           if name.endswith('.py')]
print(pyfiles)
import glob

pyfiles = glob.glob(filepath + '/cookbook/*.py')
print(pyfiles)
from fnmatch import fnmatch

pyfiles = [name for name in os.listdir(filepath + '/cookbook')
           if fnmatch(name, '*.py')]
print(pyfiles)

yfiles = glob.glob('*.py')

# Get file sizes and modification dates
name_sz_date = [(name, os.path.getsize(name), os.path.getmtime(name))
                for name in pyfiles]
for name, size, mtime in name_sz_date:
    print(name, size, mtime)

# Alternative: Get file metadata
file_metadata = [(name, os.stat(name)) for name in pyfiles]
for name, meta in file_metadata:
    print(name, meta.st_size, meta.st_mtime)
# 5.14 忽略文件名编码   5.15 打印不合法的文件名
import sys

print('systemencoding', sys.getfilesystemencoding())


def bad_filename(filename):
    temp = filename.encode(sys.getfilesystemencoding(), errors='surrogateescape')
    return temp.decode('utf-8')


print(bad_filename(filename), bad_filename('filename'))

# 5.16 增加或改变已打开文件的编码
import urllib.request
import io

u = urllib.request.urlopen('http://www.baidu.com')
f = io.TextIOWrapper(u, encoding='utf-8')
text = f.read()
print(text)
print('encoding', sys.stdout.encoding)
sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding='latin-1')
print('encoding', sys.stdout.encoding)

f = open(new_filename, 'w', encoding='UTF-8')
print(f, f.buffer, f.buffer.raw)
f = io.TextIOWrapper(f.buffer, encoding='latin-1')
print(f, f.buffer, f.buffer.raw)
# f1 = logging.conf.detach()
# f1.write('Hello')
sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding='UTF-8', errors='xmlcharrefreplace')
print('Jalape\u00f1o')

# 5.17 将字节写入文本文件
sys.stdout.buffer.write(b'Hello\n')
# 5.18 将文件描述符包装成文件对象
import sys, os

fd = os.open(new_filename, os.O_WRONLY | os.O_CREAT)
f = open(fd, 'wt')
f.write('hello world\n')
f.close()

# Create a binary-mode file for stdout
bstdout = open(sys.stdout.fileno(), 'wb', closefd=False)
bstdout.write(b'Hello World\n')
bstdout.flush()
# 5.19 创建临时文件和文件夹
from tempfile import TemporaryFile, TemporaryDirectory, NamedTemporaryFile, mkstemp, mkdtemp, gettempdir

with TemporaryFile('w+t') as f:
    # Read/write to the file
    f.write('Hello World\n')
    f.write('Testing\n')

    # Seek back to beginning and read the data
    f.seek(0)
    data = f.read()
    print(data)
f = TemporaryFile('w+t')
f.write('abc\n')
data = f.read()
print(data)
f.close()  # Temporary file is destroyed

with TemporaryDirectory() as dirname:
    print('dirname is:', dirname)
with NamedTemporaryFile('w+t', delete=True) as f:
    print('filename is:', f.name)

data = mkstemp(prefix='pystu')
print(data)
data = mkdtemp(prefix='pystu')
print(data)
# logging.conf = NamedTemporaryFile(prefix='mytemp', suffix='.txt', dir='pystu')
# print(logging.conf)
data = gettempdir()
print(data)
# 5.20 与串行端口的数据通信
import serial

# port = 'com1'
# ser = serial.Serial(port, baudrate=9600, bytesize=8, parity='N', stopbits=1)
# ser.write(b'G1 X50 Y50\r\n')
# data = ser.readline()
# print(data)
# ser.close()
# 5.21 序列化Python对象
import pickle
import math

data = [1, 2, 3, 'A', 'B', 'C']
f = open(new_filename, 'wb')
pickle.dump(data, f)
s = pickle.dumps(data)
print(s)

f = open(new_filename, 'rb')  # Restore from a file
data = pickle.load(f)
print(data)

data = pickle.loads(s)  # Restore from a string
print(data)

data = pickle.dumps(math.cos)
print(data)

import time
import threading


class Countdown:
    def __init__(self, n):
        self.n = n
        self.thr = threading.Thread(target=self.run)
        self.thr.daemon = True
        self.thr.start()

    def run(self):
        while self.n > 0:
            print('T-minus', self.n)
            self.n -= 1
            time.sleep(5)

    def __getstate__(self):
        return self.n

    def __setstate__(self, n):
        self.__init__(n)


data = Countdown(5)
print(data)
f = open(new_filename, 'wb')
pickle.dump(data, f)
f.close()
f = open(new_filename, 'rb')
data = pickle.load(f)
print(data)
