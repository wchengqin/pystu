# 基础类型
list1 = [1, 2, 3]
tuple1 = (4, 5, 6, list1)
tuple1[3].append(7)
list2 = list(tuple1)
print(tuple1, *tuple1, list1, list2)
dict1 = {}
dict2 = {'name': 'Dave', 'age': 18}
print(dict1, dict2)
set1 = set()
set1.add(0)
set2 = {1, 2, 3}
print(set1, set2)

# * 表达式
"""
*运算符可用于将一个迭代项解压缩到函数调用中的参数中
**运算符允许我们获取键-值对字典，并在函数调用中将其解压为关键字参数
"""


def test(name, age=15, *args, **kw):
    print('hello %s %d  args %s' % (name, age, str(args)))


record = ({'name': 'Dave', 'age': 18}, 'dave@example.com', '773-555-1212', '847-555-1212')
user, email, *phone_numbers = record
print(user)  # 字典类型
test(**user)  # 解压字典当作关键字参数
test('wang', 18, ('A', 'B', 'C'))  #
print(phone_numbers)  # 列表类型
print(*phone_numbers)

# 函数参数
"""
在Python中一种可以使用5中传递参数的方式（位置参数、默认参数、变长参数、关键字参数、命名关键字参数），书写时要遵循顺序
位置参数：必选参数
默认参数：在调用函数的时候使用一些包含默认值的参数
变长参数：可变参数就是允许在调用参数的时候传入多个（≥0个）参数（类似于列表、字典）
关键字参数：允许在调用时以字典形式传入0个或多个参数，在传递参数时用等号（=）连接键和值
命名关键字参数：在关键字参数的基础上限制传入的的关键字的变量名，需要一个用来区分的分隔符*，它后面的参数被认为是命名关键字参数
"""


def test1(a, b, c=0, *args, **kw):  # *args是可变参数，args接收的是一个tuple; **kw是关键字参数，kw接收的是一个dict
    print("a = ", a, "b = ", b, "args = ", args, "kw = ", kw)


def test2(a, b, c=0, *, d, **kw):
    print("a = ", a, "b = ", b, "c = ", c, "d = ", d, "kw = ", kw)


test1(1, 2)
test1(1, 2, c=3)
test1(1, 2, 3, 'a', 'b')
test1(1, 2, 3, 'a', 'b', x=99)
test2(1, 2, 3, d=(4, 5), e1=10, e2=20)

# yield
""" yield 的作用就是把一个函数变成一个 generator，带有 yield 的函数不再是一个普通函数，
Python 解释器会将其视为一个 generator，调用 fab(5) 不会执行 fab 函数，而是返回一个 iterable 对象！
在 for 循环执行时，每次循环都会执行 fab 函数内部的代码，执行到 yield b 时，fab 函数就返回一个迭代值，
下次迭代时，代码从 yield b 的下一条语句继续执行，而函数的本地变量看起来和上次中断执行前是完全一样的，
于是函数继续执行，直到再次遇到 yield。
"""
from inspect import isgeneratorfunction
from collections.abc import Iterable
import types


# 斐波那契数列，，除第一个和第二个数外，任意一个数都可由前两个数相加得到
def yield_test(_max):
    r = []
    n, a, b = 0, 0, 1
    while n < _max:
        yield b
        r.append(b)
        a, b = b, a + b
        n = n + 1
    return r


print(isgeneratorfunction(yield_test))
print(isinstance(yield_test, types.GeneratorType))
print(isinstance(yield_test(5), Iterable))

for n in yield_test(5):
    print(n)
data = yield_test(5)
# 手动调用next()方法
print(data, next(data), next(data), next(data), next(data), next(data))


def foo():
    print("starting...")
    while True:
        res = yield 4
        print("res:", res)


# ext()跟send()不同的地方是，next()只能以None作为参数传递，而send()可以传递yield的值。
g = foo()
print(next(g))
print("*" * 20)
print(g.send(7))

# pass
"""
Python pass 是空语句，是为了保持程序结构的完整性。
pass 不做任何事情，一般用做占位语句。
"""
for i in range(1, 5):
    if i % 2 == 0:
        pass
    else:
        print(i)

# 切片
"""
一个完整的切片表达式包含两个“:”，用于分隔三个参数(start_index、end_index、step)。
当只有一个“:”时，默认第三个参数step=1；当一个“:”也没有时，start_index=end_index，表示切取start_index指定的那个元素。
Python可切片对象的索引方式，包括：正索引和负索引两部分。

切片操作基本表达式：object[start_index:end_index:step]
step：正负数均可，其绝对值大小决定了切取数据时的‘‘步长”，而正负号决定了“切取方向”，正表示“从左往右”取值，负表示“从右往左”取值。当step省略时，默认为1，即从左往右以步长1取值。“切取方向非常重要！”“切取方向非常重要！”“切取方向非常重要！”，重要的事情说三遍！
start_index：表示起始索引（包含该索引对应值）；该参数省略时，表示从对象“端点”开始取值，至于是从“起点”还是从“终点”开始，则由step参数的正负决定，step为正从“起点”开始，为负从“终点”开始。
end_index：表示终止索引（不包含该索引对应值）；该参数省略时，表示一直取到数据“端点”，至于是到“起点”还是到“终点”，同样由step参数的正负决定，step为正时直到“终点”，为负时直到“起点”。

"""
one = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
three = [[[]], [[]]]
print('第二个元素', one[1], '倒数第二个元素', one[-2], '正序所有', one[:], one[::], '逆序所有', one[::-1], )
print('正序2-3个', one[1:3], '逆序2-3个', one[8:6:-1], '前三个', one[:3], '后三个', one[7:], one[-3:], '逆序后三个', one[:6:-1],
      one[-1:-4:-1])
print('取奇数位置', one[1::2], '取偶数位置', one[::2], '步长为3', one[2:8:3])
# 拷贝，[:]和.copy()都属于“浅拷贝”，只拷贝最外层元素，内层嵌套元素则通过引用方式共享，而非独立分配内存，如果需要彻底拷贝则需采用“深拷贝”方式
data1 = one[2:6]
print(data1, id(one), id(data1))
data1[3] = ['a', 'b']
data2 = data1.copy()
print(data2, id(data2), id(data1))
# 数组插入、替换
data2[2:4] = [11, 22]
data2[4:6] = ['x', 'y']
print(data2)
# 多维数组切片，基于每一个维度，逗号前面是一个维度。
# 只有转为明确的数组结构，才能多维切片
import numpy as np

two = [[1, 2, 3, 4], [11, 12, 13, 14], [21, 22, 23, 24]]
print(type(two), two)
data1 = np.array(two)
print(type(data1), data1)
data2 = data1[1:3, -1:-3:-1]
print(type(data2), data2)

# 变量域
print(vars())

# 作用域
'''
nonlocal关键字用来在函数或其他作用域中使用外层(非全局)变量。
'''


def work1():
    x = 0

    def handler():
        nonlocal x
        x = x + 3
        return x

    return handler


f = work1()
print(f())
print(f())
print(f())


def work3():
    x = 0

    def add(a):
        d = a + x
        return d

    return add


f = work3()
print(f(5))
print(f(5))
print(f(5))

data = 0


def work2():
    global data
    data = data + 3
    return data


print(work2(), data)
print(work2(), data)
print(work2(), data)
