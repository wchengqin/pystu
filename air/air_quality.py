import time

import pandas as pda
import pyecharts.options as opts
import requests
from bs4 import BeautifulSoup
from pyecharts.charts import Line
from pyecharts.faker import Faker

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}


def file_name(city, year):
    filename = '../data/air_%s_%d.csv' % (city, year)
    return filename


def pull_data(city, year):
    res = []
    for i in range(1, 13):
        url = 'http://www.tianqihoubao.com/aqi/%s-%d%02d.html' % (city, year, i)
        response = requests.get(url=url, headers=headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        rows = soup.select('div.api_month_list > table >tr')
        for row in rows[1:]:
            data = row.select('td')
            date = data[0].get_text().strip()  # 日期
            level = data[1].get_text().strip()  # 质量等级
            aqi = data[2].get_text().strip()  # AQI指数
            rank = data[3].get_text().strip()  # 当天AQI排名
            pm25 = data[4].get_text().strip()  # PM2.5
            pm10 = data[5].get_text().strip()  # PM10
            so2 = data[6].get_text().strip()  # So2
            no2 = data[7].get_text().strip()  # No2
            co = data[8].get_text().strip()  # Co
            o3 = data[9].get_text().strip()  # O3
            res.append((date, level, aqi, rank, pm25, pm10, so2, no2, co, o3))
            time.sleep(2)
    return res


def write_file(city, year):
    filename = file_name(city, year)
    with open(filename, 'a+', encoding='utf-8-sig') as f:
        res = pull_data(city, year)
        for data in res:
            s = ','.join(data)
            f.write(s + '\n')


def line_char(city, year):
    df = pda.read_csv(file_name(city, year), header=None,
                      names=["date", "level", "aqi", "rank", "pm25", "pm10", "so2", "no2", "co", "o3"])
    date = df['date']
    aqi = df['aqi']

    line2 = (
        Line()
            .add_xaxis(Faker.values())
            .add_yaxis('AQI', aqi)
            .add_yaxis('日期', date)
            .set_global_opts(
            title_opts=opts.TitleOpts(title='Line 数值x轴'),
            xaxis_opts=opts.AxisOpts(type_='value')  # 设置x轴类型属性为value数值类型
        )
    )
    line2.render('../data/air-line.html')


if __name__ == '__main__':
    city, year = 'tianjin', 2019
    # write_file(city,year)
    line_char(city, year)
