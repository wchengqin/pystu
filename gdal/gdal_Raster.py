# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 20:41:35 2022

@author: Angelo
"""

from osgeo import gdal, osr, ogr, gdal_array
import numpy as np


def raster_metadata(ds):
    """获取元数据
    Args:
        ds: ds = gdal.Open(data)

    Returns:
        metadata

    """
    metadata = dict()
    # 也可以是 LongName。GTiff 与 GeoTiff 的区别
    metadata['format'] = ds.GetDriver().ShortName
    # 波段数
    metadata['band_count'] = ds.RasterCount
    # band 1
    band = ds.GetRasterBand(1)
    metadata['nodata'] = band.GetNoDataValue()
    # 统计值
    band_stat = band.GetStatistics(True, True)
    metadata['min'] = band_stat[0]
    metadata['max'] = band_stat[1]
    metadata['mean'] = band_stat[2]
    metadata['stddev'] = band_stat[3]

    # 空间参考系统
    srs = osr.SpatialReference(ds.GetProjectionRef())
    metadata['proj4'] = srs.ExportToProj4()
    metadata['wkt'] = srs.ExportToWkt()
    # 地理坐标系
    metadata['geocs'] = srs.GetAttrValue('GEOGCS')
    metadata['uom'] = srs.GetAttrValue('UNIT')
    # 投影坐标系
    metadata['projcs'] = srs.GetAttrValue('PROJCS')  # if projected
    metadata['epsg'] = srs.GetAuthorityCode(None)
    # or
    # metadata['srid'] = srs.GetAttrValue('AUTHORITY', 1)
    # 是否有地图投影（平面坐标）
    metadata['is_projected'] = srs.IsProjected()

    # 仿射变换信息，6参数：
    # upper left x, x(w-e) resolution, x skew, upper left y, y skew, y(s-n) resolution
    ulx, xres, xskew, uly, yskew, yres = ds.GetGeoTransform()
    lrx = ulx + (ds.RasterXSize * xres)  # low right x
    lry = uly + (ds.RasterYSize * yres)  # low right y

    metadata['minx'] = ulx
    maxx = lrx  # ulx + xres * ds.RasterXSize
    metadata['maxx'] = maxx
    miny = lry  # uly + yres * ds.RasterYSize
    metadata['miny'] = miny
    metadata['maxy'] = uly
    # 中心点 centroid
    cx = ulx + xres * (ds.RasterXSize / 2)
    cy = uly + yres * (ds.RasterYSize / 2)
    metadata['center_x'] = cx
    metadata['center_y'] = cy
    metadata['resolution'] = xres
    # geographic width
    metadata['width'] = ds.RasterXSize * xres
    # geographic height，negative，负值
    metadata['height'] = ds.RasterYSize * yres
    # image width
    metadata['size_width'] = ds.RasterXSize
    metadata['size_height'] = ds.RasterYSize
    # minx,miny,maxx,maxy
    metadata['extent'] = [ulx, miny, maxx, uly]
    metadata['centroid'] = [cx, cy]
    return metadata


def band_fusion(inputfiles, path_tiff_outfile, NoDataValue=None):
    """波段融合

    Args:
        inputfiles: 文件列表，如[file1, file2, ...]
        path_tiff_outfile: 输出GeoTIFF文件名
        NoDataValue: 无数据指定值，默认为None

    Returns:
        STRING ERROR TYPE

    """
    im_geotrans = ''
    im_proj = ''
    data_array = None
    for i in range(len(inputfiles)):
        # 打开数据
        try:
            dataset = gdal.Open(inputfiles[i])
        except RuntimeError as e:
            return e
        if dataset.GetRasterBand(1).GetNoDataValue():
            if not NoDataValue:
                continue
            else:
                dataset.GetRasterBand(1).SetNoDataValue(NoDataValue)
        if i == 0:
            data_array = np.empty([len(inputfiles), dataset.RasterYSize, dataset.RasterXSize],
                                  dtype=dataset.ReadAsArray().dtype)
            data_array[0] = dataset.ReadAsArray()
            im_geotrans = dataset.GetGeoTransform()
            im_proj = dataset.GetProjection()
        else:
            data_array[i] = dataset.ReadAsArray()
        del dataset

    # 获取图像数据类型
    datatype = gdal_array.flip_code(data_array.dtype)
    # if 'int8' in data_array.dtype.name:
    #     datatype = gdal.GDT_Byte
    # elif 'int16' in data_array.dtype.name:
    #     datatype = gdal.GDT_UInt16
    # else:
    #     datatype = gdal.GDT_Float32

    # 获取图像元数据
    if len(data_array.shape) == 3:
        im_bands, im_height, im_width = data_array.shape
    else:
        im_bands, (im_height, im_width) = 1, data_array.shape

    # 创建数据
    diver = gdal.GetDriverByName('GTiff')
    new_dataset = diver.Create(path_tiff_outfile, im_width, im_height, im_bands, datatype)

    # 设置地理坐标和投影
    new_dataset.SetGeoTransform(im_geotrans)
    new_dataset.SetProjection(im_proj)

    # 设置NAN
    if NoDataValue:
        data_array[np.isnan(data_array)] = NoDataValue

    # 写入数据
    gdal_array.DatasetWriteArray(new_dataset, data_array)
    # if im_bands == 1:
    #     new_dataset.GetRasterBand(1).WriteArray(data_array)
    #
    # else:
    #     for i in range(im_bands):
    #         new_dataset.GetRasterBand(i+1).WriteArray(data_array[i])

    new_dataset = None
    del new_dataset
    return 'Successful Band Fusion!!'


def array2gtiff(path_tiff_outfile, im_geotrans, im_proj, data_array, NAN=None):
    '''数据矩阵写入GeoTiff图像
    
    Args:
        path_tiff_outfile: tiff输出文件名
        im_geotrans: 地理坐标系
        im_proj: 投影坐标系
        data_array: 数据矩阵 np.array([b1.ReadAsArray(),b2.ReadAsArray(),b3.ReadAsArray()])
        NAN: NoDataValue
        
    Returns:
        None  
    '''
    # 获取图像数据类型
    datatype = gdal_array.flip_code(data_array.dtype)
    # if 'int8' in data_array.dtype.name:
    #     datatype = gdal.GDT_Byte
    # elif 'int16' in data_array.dtype.name:
    #     datatype = gdal.GDT_UInt16
    # else:
    #     datatype = gdal.GDT_Float32

    # 获取图像元数据
    if len(data_array.shape) == 3:
        im_bands, im_height, im_width = data_array.shape
    else:
        im_bands, (im_height, im_width) = 1, data_array.shape

    # 创建数据
    diver = gdal.GetDriverByName('GTiff')
    new_dataset = diver.Create(path_tiff_outfile, im_width, im_height, im_bands, datatype)

    # 设置地理坐标和投影
    new_dataset.SetGeoTransform(im_geotrans)
    new_dataset.SetProjection(im_proj)

    # 设置NAN
    print(np.isnan(data_array))
    if NAN:
        print(np.isnan(data_array))
        data_array[np.isnan(data_array)] = NAN
        # new_dataset.SetNoDataValue(99)

    # 写入数据
    gdal_array.DatasetWriteArray(new_dataset, data_array)
    # if im_bands == 1:
    #     new_dataset.GetRasterBand(1).WriteArray(data_array)
    #
    # else:
    #     for i in range(im_bands):
    #         new_dataset.GetRasterBand(i+1).WriteArray(data_array[i])

    # outBand = new_dataset.GetRasterBand(1)
    # outBand.SetNoDataValue(99)
    # outBand.FlushCache()

    new_dataset = None
    del new_dataset


def Read_img2array(img_file_path):
    """
    读取栅格数据，将其转换成对应数组
    img_file_path: 栅格数据路径
    :return: 返回投影，几何信息，和转换后的数组
    """
    dataset = gdal.Open(img_file_path)  # 读取栅格数据
    print('处理图像波段数总共有：', dataset.RasterCount)
    # 判断是否读取到数据
    if dataset is None:
        print('Unable to open *.tif')
        import sys
        sys.exit(1)  # 退出
    projection = dataset.GetProjection()  # 投影
    geotrans = dataset.GetGeoTransform()  # 几何信息
    im_width = dataset.RasterXSize  # 栅格矩阵的列数
    im_height = dataset.RasterYSize  # 栅格矩阵的行数
    im_bands = dataset.RasterCount  # 波段数
    # 直接读取dataset
    img_array = dataset.ReadAsArray()
    return im_width, im_height, im_bands, projection, geotrans, img_array


def polygonize_raster(raster_file, poly_name):
    '''栅格转为矢量
    input:
        raster_file： 栅格数据
        poly_name： 矢量数据名称
    '''
    ds = gdal.Open(raster_file)
    srcband = ds.GetRasterBand(1)
    maskband = srcband.GetMaskBand()
    drv = ogr.GetDriverByName('ESRI Shapefile')
    dst_ds = drv.CreateDataSource(poly_name)
    # 空间参考
    srs = osr.SpatialReference()
    srs.ImportFromWkt(ds.GetProjection())

    dst_layername = 'out'
    dst_layer = dst_ds.CreateLayer(dst_layername, srs=srs)
    dst_fieldname = 'DN'
    fd = ogr.FieldDefn(dst_fieldname, ogr.OFTInteger)
    dst_layer.CreateField(fd)
    dst_field = 0
    options = []
    # 参数  输入栅格图像波段\掩码图像波段、矢量化后的矢量图层、需要将DN值写入矢量字段的索引、算法选项、进度条回调函数、进度条参数
    gdal.Polygonize(srcband, maskband, dst_layer, dst_field, options)


def vector2raster(inputfile, outputfile, bands=[1], burn_values=[0], field="", all_touch="False"):
    '''矢量数据 转 栅格数据
        需要注意field，all_touch这些option的值必须为字符串
        vector2raster('test.shp', 'test.tif', field="DN")
    '''
    # 输入矢量文件
    inputfile = inputfile
    # 输出栅格文件
    outputfile = outputfile
    # 打开矢量文件
    vector = ogr.Open(inputfile)
    # 获取矢量图层
    layer = vector.GetLayer()

    spatialRef = layer.GetSpatialRef()
    # print(spatialRef)
    # 确定栅格大小
    pixel_size = 10
    x_min, x_max, y_min, y_max = layer.GetExtent()
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)
    print("x:{0}, y:{1}".format(x_res, y_res))
    # 查看要素数量
    # featureCount = layer.GetFeatureCount()
    # 创建输出的TIFF栅格文件
    targetDataset = gdal.GetDriverByName('GTiff').Create(outputfile, x_res, y_res, 1, gdal.GDT_Byte)
    # 设置栅格坐标系与投影
    targetDataset.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))
    targetDataset.SetProjection(str(spatialRef))
    # 目标band 1
    band = targetDataset.GetRasterBand(1)
    # 白色背景
    NoData_value = 255
    band.SetNoDataValue(NoData_value)
    band.FlushCache()
    if field:
        # 调用栅格化函数。RasterizeLayer函数有四个参数，分别有栅格对象，波段，矢量对象，options
        # options可以有多个属性，其中ATTRIBUTE属性将矢量图层的某字段属性值作为转换后的栅格值
        gdal.RasterizeLayer(targetDataset, bands, layer, burn_values=burn_values,
                            options=["ALL_TOUCHED=" + all_touch, "ATTRIBUTE=" + field])
    else:
        gdal.RasterizeLayer(targetDataset, bands, layer, burn_values=burn_values, options=["ALL_TOUCHED=" + all_touch])


def GetExtent(in_fn):
    """
    获取影像的左上角和右下角坐标
    Args:
        in_fn: 影像
    Returns: x_min, y_max, x_max, y_min
    """
    dataset = gdal.Open(in_fn)
    geotrans = list(dataset.GetGeoTransform())
    xsize = dataset.RasterXSize
    ysize = dataset.RasterYSize
    x_min = geotrans[0]
    y_max = geotrans[3]
    x_max = geotrans[0] + xsize * geotrans[1]
    y_min = geotrans[3] + ysize * geotrans[5]
    dataset = None
    del dataset
    return x_min, y_max, x_max, y_min


def splicing(files, outfile, NoDataValue=0):
    """
    影像镶嵌拼接（无匀色）
    Args:
        files: 待镶嵌的文件列表
        outfile: 输出影像名
        NoDataValue: 指定无数据值，镶嵌忽略值
    Returns: None
    """
    # 获取镶嵌后影像的左上角和右下角坐标
    min_x = max_y = max_x = min_y = 0
    for in_fn in files:
        if in_fn == files[0]:
            min_x, max_y, max_x, min_y = GetExtent(files[0])
        else:
            x_min, y_max, x_max, y_min = GetExtent(in_fn)
            min_x = min(min_x, x_min)
            min_y = min(min_y, y_min)
            max_x = max(max_x, x_max)
            max_y = max(max_y, y_max)

    # 获取镶嵌后影像行列数
    in_ds_1 = gdal.Open(files[0])
    geotrans = list(in_ds_1.GetGeoTransform())
    width = geotrans[1]
    height = geotrans[5]
    cols = int((max_x - min_x) / width) + 1
    rows = int((max_y - min_y) / (-height)) + 1
    bands = in_ds_1.RasterCount

    driver = gdal.GetDriverByName('GTiff')
    out_ds = driver.Create(outfile, cols, rows, bands,
                           gdal_array.flip_code(in_ds_1.GetRasterBand(1).ReadAsArray().dtype))
    out_ds.SetProjection(in_ds_1.GetProjection())
    geotrans[0] = min_x
    geotrans[3] = max_y
    out_ds.SetGeoTransform(geotrans)

    # 定义仿射逆变换  InvGeoTransform函数，使用此函数将现实世界坐标转换为图像坐标
    inv_geotrans = gdal.InvGeoTransform(geotrans)

    for in_fn in files:
        in_ds = gdal.Open(in_fn)
        in_gt = in_ds.GetGeoTransform()
        # 仿射逆变换
        offset = gdal.ApplyGeoTransform(inv_geotrans, in_gt[0], in_gt[3])
        x, y = map(int, offset)

        # gdal.Transformer，可计算相同srs下的坐标偏移；不能用于不同srs投影转换
        # 作用：现实世界坐标（投影坐标或地理坐标）与图像坐标（行列号）之间的转换、两个栅格图像之间像素坐标偏移（行列号），如镶嵌
        # 原理就是在相同srs情况下，计算图1的像素坐标到现实世界坐标的偏移，再从现实世界坐标偏移到图2的像素坐标。其实就是两次仿射变换（正、逆），从而把图1的像素坐标偏移到图2的像素坐标。
        # 所以不能用于不同srs情况，因为该函数没有内置不同srs的投影转换公式。只能用于相同srs下，两个栅格数据集坐标的偏移。
        # 这里in_ds和out_ds具有相同srs。转换目的是为了把不同栅格数据的图像坐标（行列号）进行偏移，方便镶嵌
        #
        # trans = gdal.Transformer(in_ds, out_ds, [])  # in_ds是源栅格，out_ds是目标栅格
        # success, xyz = trans.TransformPoint(False, 0, 0)  # 计算in_ds中左上角像元对应out_ds中的行列号
        # x, y, z = map(int, xyz)
        # print(x, y, z)

        for i in range(bands):
            out_band = out_ds.GetRasterBand(i+1)
            out_band.SetNoDataValue(NoDataValue)
            data = in_ds.GetRasterBand(i+1).ReadAsArray()
            # out_band.WriteArray(data, x, y)  # x，y是开始写入时左上角像元行列号

            # 去除无值黑边
            data_up = out_band.ReadAsArray()
            data_low = np.zeros(data_up.shape)
            data_low[y:(y + data.shape[0]), x:(x + data.shape[1])] = data
            data_up = data_low * np.where(data_up == NoDataValue, 1, 0) + data_up
            out_band.WriteArray(data_up)  # x，y是开始写入时左上角像元行列号
            data_up = data_low = None
            del data_up, data_low, out_band

    del in_ds, out_ds, in_ds_1


if __name__ == '__main__':
    gdal.UseExceptions()

    # 打开数据
    try:
        ds = gdal.Open('./data/merge.tif')
    except RuntimeError as e:
        print(e)

    # 获取元数据
    # metadata = raster_metadata(ds)
    # print('元数据：' + str(metadata))

    # 波段融合
    # Err = band_fusion(['./data/image/band1.tif', './data/image/band2.tif', './data/image/band3.tif'],
    #                   'band_fusion.tif', NoDataValue=None)
    # print(Err)

    # 数据矩阵转为TIFF
    # array2gtiff('./data/Array2Tiff.tif', ds.GetGeoTransform(), ds.GetProjection(),
    #             ds.ReadAsArray(), NAN=1)

    # 栅格转为矢量
    # polygonize_raster("./data/merge.tif", "./data/test.shp")

    # 矢量转为栅格
    # vector2raster('./data/test.shp', './data/test.tif', field="DN", all_touch="False")

    # 影像镶嵌拼接（无匀色）
    in_files = ['D:/Data/LC81230322020296LGN00/LC08_L1TP_123032_20201022_20201022_01_RT_B4.TIF',
                'D:/Data/LC81230332021250LGN00/LC08_L1TP_123033_20210907_20210916_01_T1_B4.TIF',
                'D:/Data/LC81240322019300LGN00/LC08_L1TP_124032_20191027_20191030_01_T1_B4.TIF']
    splicing(in_files, 'splicing.tif', NoDataValue=0)

    # 测试区域
    # print(src_ds.ReadAsArray().shape)  # 获取数组维数 [bands, Y, X]
    # print(src_ds.GetDataType())
    # print(src_ds.GetRasterBand(1).GetNoDataValue())
    # src_ds.GetRasterBand(1).SetNoDataValue(0)
    # print(src_ds.ReadAsArray()[10, 10])
    # src_ds.FlushCache()

    # print(src_ds.GetGeoTransform())
    # print(src_ds.ReadAsArray().shape)
    # print(gdal_array.flip_code(src_ds.ReadAsArray().dtype))
    # print(gdal.GDT_Float32)

    # a = src_ds.ReadAsArray()
    # print(type(a))
    # print(a[500, 500])

    # 关闭数据
    ds = None
    del ds
