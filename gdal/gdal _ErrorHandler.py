# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 20:29:55 2022

@author: Angelo
"""

from osgeo import ogr, osr, gdal


# GDAL错误处理方法
def gdal_error_handler(err_class, err_num, err_msg):
    errtype = {
        gdal.CE_None: 'None',
        gdal.CE_Debug: 'Debug',
        gdal.CE_Warning: 'Warning',
        gdal.CE_Failure: 'Failure',
        gdal.CE_Fatal: 'Fatal'
    }
    err_msg = err_msg.replace('\n', ' ')
    err_class = errtype.get(err_class, 'None')
    print('Error Number: %s' % (err_num))
    print('Error Type: %s' % (err_class))
    print('Error Message: %s' % (err_msg))


if __name__ == '__main__':
    # 安装错误处理
    gdal.PushErrorHandler(gdal_error_handler)

    # 抛出一个假的错误
    # gdal.Error(1, 2, 'test error')
    ds = gdal.Open('./data/test.tif')

    # 卸载错误处理
    gdal.PopErrorHandler()
