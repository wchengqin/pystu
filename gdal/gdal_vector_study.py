from osgeo import gdal, osr, ogr
import os


if __name__ == '__main__':
    gdal.UseExceptions()

    # # 获取OGR驱动列表
    # cnt = ogr.GetDriverCount()
    # formatsList = []  # Empty List
    #
    # for i in range(cnt):
    #     driver = ogr.GetDriver(i)
    #     driverName = driver.GetName()
    #     if not driverName in formatsList:
    #         formatsList.append(driverName)
    #
    # formatsList.sort()  # 排序
    # print(formatsList)

    daShapefile = './data/shp_test.shp'
    driver = ogr.GetDriverByName('ESRI Shapefile')
    gdal.SetConfigOption("GDAL_FILENAME_IS_UTF8", "YES")
    gdal.SetConfigOption("SHAPE_ENCODING", "GBK")
    dataSource = driver.Open(daShapefile, 0)  # 0 只读. 1 读写.

    # 检查数据源是否有效.
    if dataSource is None:
        print('不能打开 %s' % daShapefile)
        exit(0)

    print('打开 %s' % daShapefile)
    layer = dataSource.GetLayer()
    featureCount = layer.GetFeatureCount()   # 获取该数据源中的图层个数，一般shp数据图层只有一个，如果是mdb、dxf等图层就会有多个
    print("%s 要素个数: %d" % (os.path.basename(daShapefile), featureCount))

    # 遍历要素
    # for feature in layer:
    #     print(feature.GetField("area"))
    # layer.ResetReading()

    # 获取要素字段
    layerDefinition = layer.GetLayerDefn()
    for i in range(layerDefinition.GetFieldCount()):
        fieldName = layerDefinition.GetFieldDefn(i).GetName()
        fieldTypeCode = layerDefinition.GetFieldDefn(i).GetType()
        fieldType = layerDefinition.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
        fieldWidth = layerDefinition.GetFieldDefn(i).GetWidth()
        GetPrecision = layerDefinition.GetFieldDefn(i).GetPrecision()
        print(fieldName + " - " + str(fieldTypeCode) + " " + fieldType + " "
              + str(fieldWidth) + " " + str(GetPrecision))
        # 打印属性表
        # for feature in layer:
        #     print(feature.GetField(fieldName), end=' ')
        # print('\n')

    # 过滤属性
    layer.SetAttributeFilter("ID = 1")
    print('条件 ID = 1 要素个数： %d' % layer.GetFeatureCount())
    for feature in layer:
        print(feature.GetField("ecclass"))
    # 取消过滤属性
    layer.SetAttributeFilter(None)
    print('去除要素过滤 要素个数： %d' % layer.GetFeatureCount())

    # 空间过滤
    # layer.SetSpatialFilter()

    # 遍历要素几何
    # for feature in layer:
    #     geom = feature.GetGeometryRef()
    #     # 获取质心
    #     print(geom.Centroid().ExportToWkt())

    # 获取图层能力
    capabilities = [
        ogr.OLCRandomRead,
        ogr.OLCSequentialWrite,
        ogr.OLCRandomWrite,
        ogr.OLCFastSpatialFilter,
        ogr.OLCFastFeatureCount,
        ogr.OLCFastGetExtent,
        ogr.OLCCreateField,
        ogr.OLCDeleteField,
        ogr.OLCReorderFields,
        ogr.OLCAlterFieldDefn,
        ogr.OLCTransactions,
        ogr.OLCDeleteFeature,
        ogr.OLCFastSetNextByIndex,
        ogr.OLCStringsAsUTF8,
        ogr.OLCIgnoreFields
    ]
    # print("图层能力:")
    # for cap in capabilities:
    #     print("  %s = %s" % (cap, layer.TestCapability(cap)))

    # 计算范围/读出上下左右边界
    extent = layer.GetExtent()
    print('UpLeft:[%f, %f]' % (extent[0], extent[3]))
    print('LowerRight:[%f, %f]' % (extent[1], extent[2]))

    # 创建多边形
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(extent[0], extent[2])
    ring.AddPoint(extent[1], extent[2])
    ring.AddPoint(extent[1], extent[3])
    ring.AddPoint(extent[0], extent[3])
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    # 几何集合
    geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
    for feature in layer:
        geomcol.AddGeometry(feature.GetGeometryRef())

    # 计算凸包
    convexhull = geomcol.ConvexHull()



    # 创建矢量文件
    outShapefile = "new.shp"
    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    # 如果存在，先删除
    if os.path.exists(outShapefile):
        outDriver.DeleteDataSource(outShapefile)
    # 创建数据源
    outDataSource = outDriver.CreateDataSource(outShapefile)
    outLayer = outDataSource.CreateLayer("newlayer", geom_type=ogr.wkbPolygon, srs=layer.GetSpatialRef())
    # 添加ID字段
    idField = ogr.FieldDefn("id", ogr.OFTInteger)
    outLayer.CreateField(idField)
    # 创建要素
    featureDefn = outLayer.GetLayerDefn()
    feature = ogr.Feature(featureDefn)
    feature.SetGeometry(convexhull)    # 设置要素几何
    feature.SetField("id", 1)
    outLayer.CreateFeature(feature)
    feature = None
    # 保存并关闭
    dataSource = None
    outDataSource = None

