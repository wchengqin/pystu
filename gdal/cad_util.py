import os
import ezdxf
from pyproj import Proj, transform
from osgeo import osr, ogr, gdal


def dxf_to_geojson(dxf):
    basename = os.path.split(dxf)[0]
    filename = os.path.basename(dxf).split('.')[0]
    outfile = "{}/{}.json".format(basename, filename)

    ogr.RegisterAll()
    gdal.SetConfigOption("GDAL_FILENAME_IS_UTF8", "YES")
    gdal.SetConfigOption("SHAPE_ENCODING", "")
    gdal.SetConfigOption("DXF_ENCODING", "ASCII")  # 设置DXF缺省编码
    poDS = ogr.Open(dxf, False)
    poDriver = ogr.GetDriverByName("GeoJson")
    # 保存文件
    res = poDriver.CopyDataSource(poDS, outfile)


def read_dxf_file(file_path):
    doc = ezdxf.readfile(file_path)
    hdrvars = doc.header.hdrvars
    extmin = hdrvars.get('$EXTMIN')  # 最小坐标
    extmax = hdrvars.get('$EXTMAX')  # 最大坐标
    # 定义CAD坐标系的投影
    # cad_proj = Proj(init='EPSG:32633')  # UTM (通用横轴墨卡托) 投影，例如 UTM 33N
    # cad_proj = Proj(init='EPSG:4326')  # WGS 84 (经纬度) 坐标系
    # cad_proj = Proj(init='EPSG:3857')  #百度坐标
    cad_proj = Proj(init='EPSG:4547')  # CGCS2000/3-dgree Gauss-Kruger CM 114E
    # 定义WGS84坐标系的投影
    wgs84_proj = Proj(proj='latlong', datum='WGS84', ellps='WGS84')
    # 获取模型空间中的所有实体
    modelspace = doc.modelspace()
    data = []
    for entity in modelspace.query():
        # print("Entity Type:", entity.dxftype())#'LWPOLYLINE'
        # print("Layer:", entity.dxf.layer)
        # print("Color:", entity.dxf.color)
        if "污水井管标注" in entity.dxf.layer:
            print("Layer:", entity.dxf.layer)
        if not hasattr(entity, 'lwpoints'):
            continue
        lwpoints = entity.lwpoints
        for point in lwpoints:
            # print(point)
            #print("%s:%s" % (entity.dxf.layer, point))
            cad_x = point[0]
            cad_y = point[1]
            cad_z = point[2]

            # gps_lon, gps_lat = transform(cad_proj, wgs84_proj, cad_x, cad_y)  # 坐标转换
            # data.append([gps_lon, gps_lat])
            # joined_list = [f"{item[0]}, {item[1]}" for item in data]
            # result = ';'.join(joined_list)
            # print(data)
            # print('\n\n\n\n')


if __name__ == "__main__":
    dwg_file_path = "../data/sample.dxf"
    dwg_file_path = "C:/Users/wang/Desktop/20181231铅山县给水管网现状图.dxf"
    dwg_file_path = "C:/Users/wang/Desktop/06~08平纵面图、井表、量表.dxf"
    dwg_file_path = "D:/mj.dxf"
    read_dxf_file(dwg_file_path)
