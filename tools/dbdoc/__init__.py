class Table:
    name = ''
    comment = ''
    columns = []

    def __init__(self, name, comment):
        self.name = name
        self.comment = comment

    def __str__(self):
        return self.name + "\t" + self.comment


class Column:
    name = ''
    type = ''
    allowNull = ''
    defaultValue = ''
    comment = ''

    def __init__(self, name, type, allowNull, defaultValue, comment):
        self.name = name
        self.type = type
        self.allowNull = allowNull
        self.defaultValue = defaultValue
        self.comment = comment

    def __str__(self):
        return


from docxtpl import DocxTemplate


def create_doc(context: dict, templateFilePath: str, targetFilePatg: str) -> None:
    """
    根据模板生成word文件
    :param context: 模板数据
    :param templateFilePath: 模板路径
    :param targetFilePatg: 目标文件路径
    """
    doc = DocxTemplate(templateFilePath)  # 模板文档
    doc.render(context)  # 执行替换
    doc.save(targetFilePatg)  # 保存新的文档
