import pandas as pda

file_path = 'C:/Users/wang/Desktop/2023.01-07考勤表.xlsx'


def get_kq_data(sheet):
    df = pda.read_excel(file_path, sheet_name=sheet, header=2, skiprows=[1])
    df = df.fillna(0)
    selected_columns = [0, 2, 6]  # 姓名，加班时长
    qj_sum = df.iloc[:, 14:23].sum(axis=1)  # 请假时长
    result = df.iloc[:, selected_columns]
    result["qj_sum"] = qj_sum
    result.columns = ["姓名", "部门", "加班时长", "请假时长"]
    # print(result)
    return result


def get_kq_sum(start, stop):
    data = []
    for i in range(start, stop):
        temp = get_kq_data(i)
        data.append(temp)
    df = pda.concat(data, ignore_index=True)
    grouped = df.groupby("姓名")
    user_jb_sum = grouped['加班时长'].sum().to_frame()  # 加班时长汇总
    user_qj_sum = grouped['请假时长'].sum().to_frame()  # 请假时长汇总
    user_jb_qj = pda.concat([user_jb_sum, user_qj_sum], axis=1)  # axis=1 表示按列合并
    # user_jb_qj = user_jb_qj[~user_jb_qj['Name'].str.contains('a', case=False)]
    user_jb_qj = user_jb_qj[~user_jb_qj.index.str.contains('离职')]

    print(user_jb_qj)


if __name__ == "__main__":
    get_kq_sum(0, 7)
