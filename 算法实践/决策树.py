import pandas as pda
from sklearn.tree import DecisionTreeClassifier as DTC
from sklearn.tree import export_graphviz
from six import StringIO
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
fname = config.get("data_path", "base_path") + "lesson.csv"
dataf = pda.read_csv(fname, encoding="utf-8")

x = dataf.iloc[:, 1:5].values
y = dataf.iloc[:, 5].values

for i in range(0, len(x)):
    for j in range(0, len(x[i])):
        thisdata = x[i][j]
        if (thisdata == "是" or thisdata == "多" or thisdata == "高"):
            x[i][j] = int(1)
        else:
            x[i][j] = int(-1)
for i in range(0, len(y)):
    thisdata = y[i]
    if (thisdata == "高"):
        y[i] = 1
    else:
        y[i] = -1
xf = pda.DataFrame(x)
yf = pda.DataFrame(y)
x2 = xf.values.astype(int)
y2 = yf.values.astype(int)
dtc = DTC(criterion="entropy")
dtc.fit(x2, y2)

# 可视化决策树
with open(config.get("data_path", "temp_path") + "tree.dot", "w") as file:
    file = export_graphviz(dtc, feature_names=["shizhan", "keshishu", "chu3333xiao", "ziliao"], out_file=file)
