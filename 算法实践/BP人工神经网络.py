# BP人工神经网络的实现

# 使用人工神经网络预测课程销量
# 数据的读取与整理
import pandas as pda
import numpy as npy
from keras.models import Sequential
from keras.layers.core import Dense, Activation
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
fname = config.get("data_path", "base_path") + "lesson.csv"

# 1、读取、构造数据
dataf = pda.read_csv(fname, encoding="utf-8")
x = dataf.iloc[:, 1:5].values
y = dataf.iloc[:, 5].values
for i in range(0, len(x)):
    for j in range(0, len(x[i])):
        thisdata = x[i][j]
        if (thisdata == "是" or thisdata == "多" or thisdata == "高"):
            x[i][j] = int(1)
        else:
            x[i][j] = 0
for i in range(0, len(y)):
    thisdata = y[i]
    if (thisdata == "高"):
        y[i] = 1
    else:
        y[i] = 0
xf = pda.DataFrame(x)
yf = pda.DataFrame(y)
x2 = xf.values.astype(int)
y2 = yf.values.astype(int)
# 使用人工神经网络模型
# 2、keras.models Sequential   /keras.layers.core Dense Activation
model = Sequential()
# 3、Sequential建立模型
# 输入层
model.add(Dense(10, input_dim=len(x2[0])))
model.add(Activation("relu"))
# 4、Dense建立层
# 输出层
model.add(Dense(1, input_dim=1))
# 5、Activation激活函数
model.add(Activation("sigmoid"))
# 模型的编译
# 6、compile模型编译
model.compile(loss="binary_crossentropy", optimizer="adam")
# 训练
# 7、fit训练（学习）
model.fit(x2, y2, epochs=1000, batch_size=100)
# 预测分类
# 8、验证（测试，分类预测）
rst = model.predict_classes(x.astype(int)).reshape(len(x))


# y_pred_prob = model.predict(x.astype(int))  # 获取每个类别的预测概率
# y_pred = npy.argmax(y_pred_prob, axis=-1)  # 获取概率最高的类别的索引
# rst1=npy.argmax(model.predict(x.astype(int)), axis=-1).reshape(len(x))
print(rst)
print(y)
ct = 0
for i in range(0, len(x2)):
    print(rst[i], y[i])
    if (rst[i] != y[i]):
        ct += 1
print("错误次数:", ct, "预测准确率:", 1 - ct / len(x2))

x3 = npy.array([[1, -1, -1, 1], [1, 1, 1, 1], [-1, 1, -1, 1]])
rst = model.predict_classes(x3).reshape(len(x3))
print("预测结果为:", str(rst))
