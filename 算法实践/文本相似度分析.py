# coding:utf-8
import jieba
import jieba.posseg
import jieba.analyse
from collections import defaultdict
from gensim import corpora, models, similarities
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
file_path = config.get("data_path", "txt_path")
# 1、读取文档
doc1 = open(file_path + "德州扑克小绿皮书.txt", encoding="utf-8").read()
doc2 = open(file_path + "超级系统2.txt", encoding="utf-8").read()
# 2、对要计算的多篇文档进行分词
data1 = jieba.cut(doc1)
data2 = jieba.cut(doc2)
# 3、对文档进行整理成指定格式，多词语间空格分隔（"词语1 词语2 词语3 … 词语n"），方便后续进行计算
word1 = ""
for item in data1:
    word1 += item + " "
word2 = ""
for item in data2:
    word2 += item + " "
documents = [word1, word2]
texts = [[word for word in document.split()]
         for document in documents]

# print(len(texts[0]),len(texts[1]))
# 4、计算出词语的频率
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1
# print(frequency)
# 5【可选】、对频率低的词语进行过滤
texts = [[word for word in text if frequency[token] > 100]
         for text in texts]
# print(len(texts[0]),len(texts[1]))
# 6、通过语料库建立词典
dictionary = corpora.Dictionary(texts)
dictionary.save(file_path + "temp.dict")
# 7、加载要对比的文档
doc3 = open(file_path + "底牌.txt", encoding="utf-8").read()
data3 = jieba.cut(doc3)
word3 = ""
for item in data3:
    word3 += item + " "
# 8、将要对比的文档通过doc2bow转化为稀疏向量
vec = dictionary.doc2bow(word3.split())
# 9、对稀疏向量进行进一步处理，得到新语料库
corpus = [dictionary.doc2bow(text) for text in texts]
corpora.MmCorpus.serialize(file_path + "底牌.mm", corpus)
# print(corpus)
# 10、将新语料库通过tfidfmodel进行处理，得到tfidf
tfidf = models.TfidfModel(corpus)
print(tfidf)
# 11、通过token2id得到特征数
featureNum = len(dictionary.token2id.keys())
# 12、稀疏矩阵相似度，从而建立索引
index = similarities.SparseMatrixSimilarity(tfidf[corpus], num_features=featureNum)
# 13、得到最终相似度结果
sim = index[tfidf[vec]]

print(sim)
