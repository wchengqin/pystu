# 以sklearn的形式对simple linear regression 算法进行封装
import numpy as np
import sklearn.datasets as datasets
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, mean_absolute_error

np.random.seed(123)


class SimpleLinearRegression():
    def __init__(self):
        """
        initialize model parameters
        """
        self.a_ = None
        self.b_ = None

    def fit(self, x_train, y_train):
        """
        training model parameters

        Parameters
        ----------
            x_train:train x ,shape:data [N,]
            y_train:train y ,shape:data [N,]
        """
        assert (x_train.ndim == 1 and y_train.ndim == 1), \
            """Simple Linear Regression model can only solve single feature training data"""
        assert len(x_train) == len(y_train), \
            """the size of x_train must be equal to y_train"""

        x_mean = np.mean(x_train)
        y_mean = np.mean(y_train)
        self.a_ = np.vdot((x_train - x_mean), (y_train - y_mean)) / np.vdot((x_train - x_mean), (x_train - x_mean))
        self.b_ = y_mean - self.a_ * x_mean

    def predict(self, input_x):
        """
        make predictions based on a batch of data

        Parameters
        ----------
            input_x:shape->[N,]
        """
        assert input_x.ndim == 1, \
            """Simple Linear Regression model can only solve single feature data"""

        return np.array([self.pred_(x) for x in input_x])

    def pred_(self, x):
        """
        give a prediction based on single input x
        """
        return self.a_ * x + self.b_

    def __repr__(self):
        return "SimpleLinearRegressionModel"


if __name__ == '__main__':
    boston_data = datasets.load_boston()
    print(boston_data['DESCR'])
    x = boston_data['data'][:, 5]  # total x data (506,)
    y = boston_data['target']  # total y data (506,)
    # keep data with target value less than 50.
    x = x[y < 50]  # total x data (490,)
    y = y[y < 50]  # total x data (490,)

    plt.scatter(x, y)
    plt.show()

    # train size:(343,) test size:(147,)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

    regs = SimpleLinearRegression()
    regs.fit(x_train, y_train)
    y_hat = regs.predict(x_test)

    rmse = np.sqrt(np.sum((y_hat - y_test) ** 2) / len(x_test))
    mse = mean_squared_error(y_test, y_hat)
    mae = mean_absolute_error(y_test, y_hat)
    # notice
    R_squared_Error = 1 - mse / np.var(y_test)

    print('mean squared error:%.2f' % (mse))
    print('root mean squared error:%.2f' % (rmse))
    print('mean absolute error:%.2f' % (mae))
    print('R squared Error:%.2f' % (R_squared_Error))

    a = regs.a_
    b = regs.b_
    x_plot = np.linspace(4, 8, 50)
    y_plot = x_plot * a + b
    plt.scatter(x, y)
    plt.plot(x_plot, y_plot, color='red')
    plt.show()
