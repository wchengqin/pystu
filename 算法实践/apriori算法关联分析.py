# 计算学员购买课程的关联性
import pandas as pda
import numpy as npy
import configparser
import sys

from lib_ext.apriori import find_rule

sys.path.append(r'../lib_ext')

config = configparser.ConfigParser()
config.read("../config.ini")
filename = config.get("data_path", "base_path") + "lesson_buy.xls"
dataframe = pda.read_excel(filename, header=None)
# 转化一下数据
change = lambda x: pda.Series(1, index=x[pda.notnull(x)])
mapok = map(change, dataframe.values)
data = pda.DataFrame(list(mapok)).fillna(0)
print(data)

spt = 0.2  # 临界支持度
cfd = 0.6  # 置信度设置

# 使用apriori算法计算关联结果
rst = find_rule(data, spt, cfd, "-->")
print(type(rst))
print(u'结果为：\n\n', rst)
