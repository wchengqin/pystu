# coding:utf-8
import numpy
from numpy import *
import operator
import os
from os import listdir
from PIL import Image
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
file_path = config.get("data_path", "base_path")


# KNN算法识别
class MyKNN:
    def test(self, k, testdata, traindata, labels):
        '''
        KNN算法，又称邻近算法，或者说K最近邻(kNN，k-NearestNeighbor)分类算法是数据挖掘分类技术中最简单的方法之一。
        所谓K最近邻，就是k个最近的邻居的意思，说的是每个样本都可以用它最接近的k个邻居来代表。
        :param k: 获取数据个数
        :param testdata: 测试数据集
        :param traindata: 训练数据集
        :param labels: 类别集
        :return:
        '''
        traindatasize = traindata.shape[0]  # 训练数据列数
        # 从行方向扩展 tile(a,(size,1))
        dif = tile(testdata, (traindatasize, 1)) - traindata  # 测试集与训练集的向量差值
        sqdif = dif ** 2  # 差值平方
        sumsqdif = sqdif.sum(axis=1)  # 平方和，axis=1按列相加
        distance = sumsqdif ** 0.5  # 开方，获取数据距离
        sortdistance = distance.argsort()  # 升序排序
        count = {}
        for i in range(0, k):  # 获取距离最小的K个数据
            vote = labels[sortdistance[i]]
            count[vote] = count.get(vote, 0) + 1
        sortcount = sorted(count.items(), key=operator.itemgetter(1), reverse=True)  # 降序排序后，出现最多次的类别
        return sortcount[0][0]


# 先将所有图片转为固定宽高，然后再转为文本
# 图片大小不一至识别准确率非常差，当前的训练数据都是32*32，所以要测试的图片必须为32*32
def img2txt(img_path, txt_path):
    im = Image.open(img_path)
    fh = open(txt_path, "w")
    fh.truncate()
    # im.save(file_path + "timg.bmp")
    width = im.size[0]
    height = im.size[1]
    for i in range(0, height):
        for j in range(0, width):
            cl = im.getpixel((j, i))  # 根据像素位置获取颜色对象的 RGB 值
            clall = cl[0] + cl[1] + cl[2]
            if (clall == 0):  # 黑色
                fh.write("1")
            else:
                fh.write("0")
        fh.write("\n")
    fh.close()


# 加载数据
def data2array(fname):
    arr = []
    fh = open(fname)
    for i in range(0, 32):
        thisline = fh.readline()
        for j in range(0, 32):
            arr.append(int(thisline[j]))
    return arr


# 建立一个函数取文件名前缀
def seplabel(fname):
    filestr = fname.split(".")[0]
    label = int(filestr.split("_")[0])
    return label


# 建立训练数据
def traindata():
    labels = []
    trainfile = listdir(file_path + "traindata")
    num = len(trainfile)
    # 长度1024（列），每一行存储一个文件
    # 用一个数组存储所有训练数据，行：文件总数，列：1024
    trainarr = zeros((num, 1024))
    for i in range(0, num):
        thisfname = trainfile[i]
        thislabel = seplabel(thisfname)
        labels.append(thislabel)
        trainarr[i, :] = data2array(file_path + "traindata/" + thisfname)
    return trainarr, labels


# 用测试数据调用两种算法去测试，看是否能够准确识别
def testtraindata():
    knn = MyKNN()
    trainarr, labels = traindata()
    labelsSet = set(labels)
    test_data_path = file_path + "testtraindata/"
    testfileall = listdir(test_data_path)
    num = len(testfileall)
    bys_error = 0
    for i in range(0, num):
        thisfilename = testfileall[i]
        thislabel = seplabel(thisfilename)
        # print(thislabel)
        thisdataarray = data2array(test_data_path + thisfilename)
        label = knn.test(3, thisdataarray, trainarr, labels)
        print("该数字是:" + str(thislabel) + ",识别出来的数字是：" + str(label))
        knn_error=0
        if (label != thislabel):
            knn_error += 1
    print("错误次数:", knn_error, "错误率是：", str(float(knn_error) / float(num)))


def datatest():
    knn = MyKNN()
    trainarr, labels = traindata()
    test_data_path = file_path + "testdata/"
    testlist = listdir(test_data_path)
    tnum = len(testlist)
    for i in range(0, tnum):
        thistestfile = testlist[i]
        fileext = os.path.splitext(thistestfile)[1]
        if fileext != ".png":
            continue
        img_path = test_data_path + thistestfile
        txt_path = test_data_path + thistestfile + ".txt"
        img2txt(img_path, txt_path)
        testarr = data2array(txt_path)

        # KNN算法测试
        rknn = knn.test(3, testarr, trainarr, labels)
        print("KNN: ", img_path, rknn)


datatest()

testtraindata()
