import pandas as pda
import configparser
from sklearn.linear_model import LogisticRegression as LR
from stability_selection.randomized_lasso import RandomizedLogisticRegression as RLR

config = configparser.ConfigParser()
config.read("../config.ini")
fname = config.get("data_path", "base_path") + "luqu.csv"
dataf = pda.read_csv(fname)
x = dataf.iloc[:, 1:4].values
y = dataf.iloc[:, 0:1].values
print(x)
print(y)

r1 = RLR()
r1.fit(x, y)

r1.get_support()#特征筛选
#print(dataf.columns[r1.get_support()])
t=dataf[dataf.columns[r1.get_support()]].as_matrix()
r2=LR()
r2.fit(t,y)
print("训练结束")
print("模型正确率为:"+str(r2.score(x,y)))

