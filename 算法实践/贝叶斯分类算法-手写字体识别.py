# coding:utf-8
import numpy
from numpy import *
import operator
import os
from os import listdir
from PIL import Image
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
file_path = config.get("data_path", "base_path")


# 贝叶斯分类算法
class MyBayes:
    def __init__(self):
        self.length = -1
        self.labelcount = dict()
        self.vectorcount = dict()

    def fit(self, dataSet: list, labels: list):
        if (len(dataSet) != len(labels)):
            raise ValueError("您输入的测试数组跟类别数组长度不一致")
        self.length = len(dataSet[0])  # 测试数据特征值的长度
        labelsnum = len(labels)  # 类别所有的数量
        norlabels = set(labels)  # 不重复类别的数量
        for item in norlabels:
            thislabel = item
            self.labelcount[thislabel] = labels.count(thislabel) / labelsnum  # 求的当前类别占类别总数的比例
        for vector, label in zip(dataSet, labels):
            if (label not in self.vectorcount):
                self.vectorcount[label] = []
            self.vectorcount[label].append(vector)
        print("训练结束")
        return self

    def test(self, TestData, labelsSet):
        if (self.length == -1):
            raise ValueError("您还没有进行训练，请先训练")
        # 计算testdata分别为各个类别的概率
        lbDict = dict()
        for thislb in labelsSet:
            p = 1
            alllabel = self.labelcount[thislb]
            allvector = self.vectorcount[thislb]
            vnum = len(allvector)
            allvector = numpy.array(allvector).T
            for index in range(0, len(TestData)):
                vector = list(allvector[index])
                p *= vector.count(TestData[index]) / vnum
            lbDict[thislb] = p * alllabel
        thislabel = sorted(lbDict, key=lambda x: lbDict[x], reverse=True)[0]
        # print(lbDict)
        return thislabel


# 先将所有图片转为固定宽高，然后再转为文本
# 图片大小不一至识别准确率非常差，当前的训练数据都是32*32，所以要测试的图片必须为32*32
def img2txt(img_path, txt_path):
    im = Image.open(img_path)
    fh = open(txt_path, "w")
    fh.truncate()
    # im.save(file_path + "timg.bmp")
    width = im.size[0]
    height = im.size[1]
    for i in range(0, height):
        for j in range(0, width):
            cl = im.getpixel((j, i))  # 根据像素位置获取颜色对象的 RGB 值
            clall = cl[0] + cl[1] + cl[2]
            if (clall == 0):  # 黑色
                fh.write("1")
            else:
                fh.write("0")
        fh.write("\n")
    fh.close()


# 加载数据
def data2array(fname):
    arr = []
    fh = open(fname)
    for i in range(0, 32):
        thisline = fh.readline()
        for j in range(0, 32):
            arr.append(int(thisline[j]))
    return arr


# 建立一个函数取文件名前缀
def seplabel(fname):
    filestr = fname.split(".")[0]
    label = int(filestr.split("_")[0])
    return label


# 建立训练数据
def traindata():
    labels = []
    trainfile = listdir(file_path + "traindata")
    num = len(trainfile)
    # 长度1024（列），每一行存储一个文件
    # 用一个数组存储所有训练数据，行：文件总数，列：1024
    trainarr = zeros((num, 1024))
    for i in range(0, num):
        thisfname = trainfile[i]
        thislabel = seplabel(thisfname)
        labels.append(thislabel)
        trainarr[i, :] = data2array(file_path + "traindata/" + thisfname)
    return trainarr, labels


# 用测试数据调用两种算法去测试，看是否能够准确识别
def testtraindata():
    bayes = MyBayes()
    trainarr, labels = traindata()
    labelsSet = set(labels)
    bayes.fit(trainarr, labels)
    test_data_path = file_path + "testtraindata/"
    testfileall = listdir(test_data_path)
    num = len(testfileall)
    bys_error = 0
    for i in range(0, num):
        thisfilename = testfileall[i]
        thislabel = seplabel(thisfilename)
        # print(thislabel)
        thisdataarray = data2array(test_data_path + thisfilename)
        label = bayes.test(thisdataarray, labelsSet)
        print("该数字是:" + str(thislabel) + ",识别出来的数字是：" + str(label))
        if (label != thislabel):
            bys_error += 1
    print("错误次数:", bys_error, "错误率是：", str(float(bys_error) / float(num)))


def datatest():
    bayes = MyBayes()
    trainarr, labels = traindata()
    bayes.fit(trainarr, labels)
    labelsSet = set(labels)
    # print(labelsSet)
    test_data_path = file_path + "testdata/"
    testlist = listdir(test_data_path)
    tnum = len(testlist)
    for i in range(0, tnum):
        thistestfile = testlist[i]
        fileext = os.path.splitext(thistestfile)[1]
        if fileext != ".png":
            continue
        img_path = test_data_path + thistestfile
        txt_path = test_data_path + thistestfile + ".txt"
        img2txt(img_path, txt_path)
        testarr = data2array(txt_path)

        # 贝叶斯算法测试
        rbayes = bayes.test(testarr, labelsSet)
        print("Bayes: ", img_path, rbayes)


datatest()

testtraindata()
