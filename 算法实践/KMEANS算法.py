# kmeans算法
# 通过程序实现录取学生的聚类
import sys
import random
import pandas as pda
import numpy as npy
import matplotlib.pylab as pyl
from sklearn.cluster import KMeans
import configparser
from lib_ext import KMeans_ext

config = configparser.ConfigParser()
config.read("../config.ini")
fname = config.get("data_path", "base_path") + "luqu.csv"
dataf = pda.read_csv(fname)
data = npy.array(dataf.iloc[:, 1:4])


def sklearn_kmeans(data, clusters):
    kms = KMeans(n_clusters=clusters)
    y = kms.fit_predict(data)
    return y


def my_kmeans(data, clusters):
    myCentroids, clusterAssment = KMeans_ext.biKmeans(data, clusters)
    #print(clusterAssment)
    return clusterAssment


clusters = 3
# rst = sklearn_kmeans(data, clusters)
rst = my_kmeans(data, clusters)
# print(rst)
# x代表学生序号，y代表学生类别
s = npy.arange(0, len(rst))
pyl.plot(s, rst, "o")
pyl.show()
