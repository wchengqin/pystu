import sqlite3
import pandas as pda
import numpy as npy
import matplotlib.pyplot as plt
import matplotlib.pylab as pyl
from sklearn.cluster import KMeans
from sklearn import metrics


def get_conn():
    dbfile = r'D:\Workspace\Python\yiihua\data\cn.db'
    conn = sqlite3.connect(dbfile)
    return conn


def user_labels(data, clusters=10):
    kms = KMeans(n_clusters=clusters)
    y_pred = kms.fit_predict(data)
    # print(metrics.calinski_harabaz_score(data, y_pred)) # Calinski-Harabasz Index评估的聚类分数
    # print(kms.cluster_centers_)#中心
    # print(kms.labels_)  # 每个样本所属的簇
    # print(kms.inertia_)  # 用来评估簇的个数是否合适，距离越小说明簇分的越好，选取临界点的簇个数
    temp = npy.column_stack((data, y_pred))
    labes = {}
    for i in range(0, clusters):
        lab = temp[npy.where(temp[:, 1] == i)]
        labes[i] = lab[:, 0]
    return labes


def user_kmeans():
    sql = 'select * from user_profile_tag'
    conn = get_conn()
    df = pda.read_sql(sql, conn)
    conn.close()
    for key in df.columns:
        if key == 'uid' or key == 'user_level':
            continue
        data = df[key].values.reshape(-1, 1)
        labels = user_labels(data)
        print('-------------%s-------------' % key)
        for k, v in labels.items():
            if len(v) == 0:
                continue
            print('     %s          count : %s   max : %s   min : %s   mean : %s' % (
                k, len(v), v.max(), v.min(), v.mean()))


if __name__ == '__main__':
    user_kmeans()
