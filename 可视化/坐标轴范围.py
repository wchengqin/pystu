# coding:utf-8
import matplotlib.pyplot as plt
import numpy as np

x = np.random.rand(5)

fig = plt.figure()

ax1 = fig.add_subplot(111)

ax1.plot(x, x * x)
'''
调整坐标轴
ax1.axis([xmin,xmax,ymin,ymax])
ax1.set_xlim(xmin,xmax)
ax1.set_ylim(ymin,ymax)
'''
plt.show()
