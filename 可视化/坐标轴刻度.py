# coding:utf-8
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
import datetime

# x=np.arange(1,11,1)
# plt.plot(x,x)
# ax=plt.gca()
# '''
# locator_params
# 若不指定x,y轴  则同时修改x,y轴刻度
# 参数一为‘y’ 例如ax.locator_params('y',nbins=20) 意为指定y轴刻度为20
#
# '''
# ax.locator_params('y',nbins=20)
# plt.show()
fig = plt.figure()

start = datetime.datetime(2015, 1, 1)
stop = datetime.datetime(2016, 1, 1)
delta = datetime.timedelta(days=1)

dates = mpl.dates.drange(start, stop, delta)
y = np.random.rand(len(dates))
ax = plt.gca()
ax.plot_date(dates, y, linestyle='-', marker='')
# autofmt_xdate 坐标轴刻度自动伸缩
fig.autofmt_xdate()

plt.show()
