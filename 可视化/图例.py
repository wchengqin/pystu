# coding:utf-8
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(1, 11, 1)
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(x, x * 2, label="西瓜", c='g')
ax1.plot(x, x * 3, label="苹果", c='k')
ax1.plot(x, x * 5, label="香蕉", c='r')

plt.title("XXX统计报告")
plt.xlabel("月份")
plt.ylabel("数量")

plt.xlim(1,12)
plt.ylim(0,100)

'''
label可以写在legend里面ax1.legend（"","",""）
legend 显示图例
loc 位置参数 0best(自适应最好的位置) 1右上 2左上 3左下 4右下......--10
ncol    有几列
'''
ax1.legend(loc=1, ncol=3)
plt.show()
