# coding:utf-8
import matplotlib.pyplot as plt
import numpy as np

x = np.random.randint(1, 1000, 20)
y = np.random.randint(1000, 100000, 20)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
'''
无需加True---显示网格(交互式需要)
color='r',  颜色为红色
linestyle='-', 线型为实线
linewidth=2 线宽为2
'''
ax1.grid(color='r', linestyle='-', linewidth=2)
ax1.scatter(x, y)
plt.show()
