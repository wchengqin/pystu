# coding:utf-8
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(1, 100)
fig = plt.figure()
'''
add_subplot
参数一：子图总行数
参数二：子图总列数
参数三：子图位置
'''
ax1 = fig.add_subplot(221)
ax1.plot(x, x)

ax2 = fig.add_subplot(222)
ax2.plot(x, x)

z = np.random.normal(50, 1, 100000)  # 正态分布随机数
ax3 = fig.add_subplot(212)
ax3.hist(z)

plt.show()
