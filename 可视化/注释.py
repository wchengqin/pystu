import matplotlib.pyplot as plt
import numpy as np

x = np.arange(-11, 10, 1)

y = x * x

plt.plot(x, y)
plt.axis([-11, 11, 0, 200])
# annotate注释函数，xy代表箭头位置，xytext代表注释第一个字母所在的坐标
# facecolor箭头颜色，frac箭头所占比例，headwidth箭头宽度,width箭身的宽度
plt.annotate("this is bottom", xy=(0, 1), xytext=(0, 25),
             arrowprops=dict(facecolor='r', frac=0.2, headwidth=10, width=10)
             )
plt.show()
