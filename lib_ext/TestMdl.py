import calendar  # 引入日历模块
import os
import logging

logging.basicConfig(level=logging.INFO)

___auth___ = "wangchengqin"

_private_var = "测试私有变量"  # _、__开头，不允许直接调用


def get_private_var():
    return _private_var


def set_private_var(s):
    global _private_var
    _private_var = s


class Room(object):

    def __init__(self, students):
        self.students = students

    def getStudents(self):
        return self.students


class Student(object):
    stu_say_times = 0  # 类变量

    def __init__(self, name, score):
        self.name = name
        self.score = score
        set_private_var("当前学生姓名：" + name)

    def get_grade(self):
        if self.score >= 90:
            return 'A'
        elif self.score >= 60:
            return 'B'
        else:
            return 'C'
        # 位置参数、可变位置参数、关键词参数、可变关键词参数

    def say(self, a, *args, b="B", c="C", **kw):
        Student.stu_say_times += 1
        print("a=", a)
        print("args=", args)
        print("b=", b)
        print("c=", c)
        print("name=", self.name, "kws=", kw, "host=", kw.get("host", "127.0.0.1"))

    @property
    def grade(self):
        return self.score / 2

    @classmethod
    def cls_mtd(cls, a, b):
        print(cls)
        return a + b

    @staticmethod
    def sta_mtd(a, b):
        return a + b

class MathTest:
    # 乘法口诀
    @staticmethod  # 静态方法注解
    def cfkj():
        for i in range(1, 10):
            for j in range(1, i + 1):
                print('{}x{}={}\t'.format(i, j, i * j), end='')
            print()

    @staticmethod
    def pow(i, n=2):
        return i ** n

    @staticmethod
    def chu(*args):
        a = None
        try:
            if args[1] < 0:
                raise Exception("除数必须大于0", args[1])
            a = args[0] / args[1]
        except Exception as ex:
            print("异常类型：", type(ex), ex.args)
        else:
            print("计算成功")
        finally:
            print("计算结束")
        return a


# 输入指定年月
def cal_test():
    yy = int(input("输入年份: "))
    mm = int(input("输入月份: "))
    # 显示日历
    print(calendar.month(yy, mm))


def digui(n):
    if n < 1:
        return 0
    elif n == 1:
        return 1
    else:
        return n * digui(n - 1)


def test_except(val):
    try:
        return int(val)
    except Exception as e:
        print("error:", e)
        logging.exception(e)
    finally:
        print("end int")
    return -1


def main():
    print("is test main")
    c = Student("超人", 100)
    print("classmethod  ", Student.cls_mtd(1, 5), "   ", c.cls_mtd(1, 5))
    print("staticmethod  ", Student.sta_mtd(4, 6), "   ", c.sta_mtd(4, 6))
    print("property  ", Student.grade, "   ", c.grade)

print("__name__ = ", __name__)
if __name__ == "__main__":
    main()
