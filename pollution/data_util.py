import json

import pandas as pda

base_data_path = "D:/YesEP/08工程项目/天瑞环境/农村面源污染防治监测/日照市农村生活污水治理监管调查表.xlsx"
base_data_column = ["序号", "省（区、市）", "地级行政区", "县级行政区", "乡级行政区", "行政村名称", "行政村行政区划代码",
                    "位于或部分属于集中式饮用水水源保护区", "乡镇政府驻地", "中心村", "自然村数（个）", "常住人口（人）",
                    "常住户数（户）", "开展了水冲厕改造的户数（户）", "该行政村是否完成农村生活污水治理",
                    "已完成污水治理的自然村数（个）", "纳入城镇市政污水管网的自然村数（个）",
                    "建设污水处理设施收集处理后排放的自然村数（个）", "连片收集处理后资源化利用的自然村数（个）",
                    "单户处理后就地消纳利用的自然村数（个）", "其他", "集中式污水处理设施数（个）", "正常运行的设施数（个）",
                    "总设计处理规模（吨/日）", "2020年及以后完工设施数（个）", "其中正常运行设施数（个）",
                    "分散式处理设施数（20吨0日以下）（个）", "该行政村是否完成农村环境整治", "完成农村环境整治时间", "备注",
                    "请核实在该乡级行政区内，乡镇政府驻地村庄是否唯一", "常住人口/常住户数",
                    "开展了水冲厕改造的户数/常住户数", "已完成污水治理的自然村数/该行政村的自然村总数*100",
                    "集中式处理设施正常运行率（%）", "2020年及以后完工集中式处理设施正常运行率（%）",
                    "扣除非正常运行设施后、已完成污水治理的自然村数/该行政村的自然村总数*100",
                    "单个处理设施的平均设计处理规模（吨/日）"]
black_water_path = "D:/YesEP/08工程项目/天瑞环境/农村面源污染防治监测/日照黑臭水体清单（25个）.xlsx"
black_water_column = ["序号", "市", "县（市、区）", "乡镇或街道社区", "名称", "人口", "面积(km2)", "原水体编号", "空",
                      "水体类型", "涉及的自然村", "河湖长姓名", "所在单位", "职务", "联系电话", "水域面积（m2）", "长（m）",
                      "宽（m）", "黑臭段起点", "起点经度", "起点纬度", "黑臭段终点", "终点经度", "终点纬度", "透明度（cm）",
                      "溶解氧（mg/L）", "氨氮（mg/L）", "主要污染问题", "是否连续型污染", "污染物进入量", "是否开展治理",
                      "治理进展", "备注", "透明度（cm）", "溶解氧（mg/L）", "氨氮（mg/L）"]

test_data_path = "D:/YesEP/08工程项目/天瑞环境/农村面源污染防治监测/测试数据.xlsx"
test_monitor_village_column = ["序号", "县(市、区)", "乡(镇)", "村庄", "经度(°)", "纬度(°)", "监管类型"]  # 0监控村庄基础信息
test_surface_water_column = ["序号", "县(市、区)", "断面名称", "经度(°)", "纬度(°)", "断面类型"]  # 1农村地表水水质监测点位信息表
test_livestock_farm_column = ["序号", "区县名称", "养殖场户名称", "详细地址", "经度(°)", "纬度(°)",
                              "养殖种类"]  # 2规模化畜禽养殖场名单
test_aquaculture_farm_column = ["序号", "区县名称", "养殖场户名称", "详细地址", "经度(°)", "纬度(°)",
                                "养殖种类"]  # 3规模化陆地水产养殖场名单
test_irrigation_water_column = ["序号", "农田灌区名称", "经度(°)", "纬度(°)", "灌区规模(万亩）", "一年几熟",
                                "灌溉季节(月份)", "灌溉水类型", "主要1", "主要2", "主要3"]  # 4农田灌溉水基本信息表


# 加载基础数据表
def loadBaseData():
    data = pda.read_excel(base_data_path, sheet_name=0, header=7, names=base_data_column)
    data = data.fillna("")
    # print(data)
    # for index, row in data.iterrows():
    #     print(row)
    return data


# 所有的省
def getProvince():
    df = loadBaseData()
    data = df.iloc[:, 1]  # 第二列
    data = data.drop_duplicates().tolist()  # 去重
    return data


# 省辖市
def getCity(province):
    df = loadBaseData()
    df = df.drop(df[df.iloc[:, 1] != province].index)
    data = df.iloc[:, 2]  # 第三列
    data = data.drop_duplicates().tolist()  # 去重
    return data


#  市辖区县
def getCounty(city):
    print(city)
    df = loadBaseData()
    df = df.drop(df[df.iloc[:, 2] != city].index)
    data = df.iloc[:, 3]  # 第四列
    data = data.drop_duplicates().tolist()  # 去重
    return data


# 县辖乡镇
def getTown(county):
    print(county)
    df = loadBaseData()
    df = df.drop(df[df.iloc[:, 3] != county].index)
    data = df.iloc[:, 4]  # 第五列
    data = data.drop_duplicates().tolist()  # 去重
    return data


# 辖乡镇辖村
def getVillage(town):
    print(town)
    df = loadBaseData()
    df = df.drop(df[df.iloc[:, 4] != town].index)
    data = df.iloc[:, 5]  # 第六列
    data = data.drop_duplicates().tolist()  # 去重
    return data


# 根据ID获取基础数据详情
def getBaseDetail(id):
    df = loadBaseData()
    df = df[df.iloc[:, 0] == id]
    data = {} if df.empty else df.iloc[0].to_dict()
    return data


# 加载黑臭水体清单
def loadBlackWater():
    data = pda.read_excel(black_water_path, sheet_name=0, header=3, names=black_water_column)
    data = data.fillna("")
    # print(data)
    # for index, row in data.iterrows():
    #     print(row.to_dict())
    return data


# 根据ID获取黑臭水体详情
def loadBlackWaterDetail(id):
    df = loadBlackWater()
    df = df[df.iloc[:, 0] == id]
    data = {} if df.empty else df.iloc[0].to_dict()
    return data


# 加载监控村庄基础信息
def loadMonitorVillage():
    data = pda.read_excel(test_data_path, sheet_name=0, header=1, names=test_monitor_village_column)
    data = data.fillna("")
    return data


# 加载农村地表水水质监测点位信息表
def loadSurfaceWater():
    data = pda.read_excel(test_data_path, sheet_name=1, header=1, names=test_surface_water_column)
    data = data.fillna("")
    return data


# 规模化畜禽养殖场名单
def loadLivestockFarm():
    data = pda.read_excel(test_data_path, sheet_name=2, header=1, names=test_livestock_farm_column)
    data = data.fillna("")
    return data


# 规模化陆地水产养殖场名单
def loadAquacultureFarm():
    data = pda.read_excel(test_data_path, sheet_name=3, header=1, names=test_aquaculture_farm_column)
    data = data.fillna("")
    return data


# 农田灌溉水基本信息表
def loadIrrigationWater():
    data = pda.read_excel(test_data_path, sheet_name=4, header=1, names=test_irrigation_water_column)
    data = data.fillna("")
    return data


if __name__ == "__main__":
    print("")
    # getCity("山东省")
    print(getCounty("日照市"))
    print(getTown("莒县"))
    print(getVillage("洛河镇"))
    # print(getBaseDetail(41530))
    # print(loadBlackWaterDetail(25))
    with open("./data/base_data.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadBaseData().to_dict(orient="records"), ensure_ascii=False))
    with open("./data/black_water.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadBlackWater().to_dict(orient="records"), ensure_ascii=False))
    with open("./data/monitor_village.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadMonitorVillage().to_dict(orient="records"), ensure_ascii=False))
    with open("./data/surface_water.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadSurfaceWater().to_dict(orient="records"), ensure_ascii=False))
    with open("./data/livestock_farm.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadLivestockFarm().to_dict(orient="records"), ensure_ascii=False))
    with open("./data/aquaculture_farm.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadAquacultureFarm().to_dict(orient="records"), ensure_ascii=False))
    with open("./data/irrigation_water.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(loadIrrigationWater().to_dict(orient="records"), ensure_ascii=False))
